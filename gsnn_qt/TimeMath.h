﻿#ifndef TIMEMATH_H
#define TIMEMATH_H

/** Based on
\file    time_conversion.h
\brief   GNSS core 'c' function library: converting time information.
\author  Glenn D. MacGougan (GDM)
\date    2007-11-29
\since   2005-07-30
**/

/*
Класс для работы с датами и временем

Предназначен для проведения математических опереаций с датами
и временем через преобразование в формат астрономического времени
- Юлианской даты.

Формат Юлианской даты используется для астрономических наблюдений,
поскольку начало отсчета времени в этом формате берется с полудня
4713 года до нашей эры, что позволяет производить и астрономические
наблючения ночью без смены даты.

Дата преобразуется в количество дней с полудня 4713 до н.э.
Время переобразуется в дробную часть, считая что в одном дне 24 часа
Полученное число с плавающей точкой является Полной Юлианской Датой

В данном классе используется формат двойной точности long double

Примечание: Не путайте Юлианскую Дату с Юлианским Календарем! Это разные вещи!
*/

#include <time.h>
#include <math.h>
#ifdef _WIN32
    #include <windows.h>
    #include <winbase.h>
#else

//Для совместимости с Windows
struct TSystemTimeWindows
{
    long wYear;
    long wMonth;
    long wDayOfWeek;
    long wDay;
    long wHour;
    long wMinute;
    long wSecond;
    long wMilliseconds;
};

typedef struct TSystemTimeWindows SYSTEMTIME;

#endif

//include/time_tools.h
#include "time_tools.h"

// Начало отсчета годов в ОС
#define UNIX_YEAR       1970
#define TIME_H_YEAR     1900
#define WINAPI_YEAR     1601

// Конец отсчета годов в ОС
#define UNIX_FIN_YEAR       2038
#define TIME_H_FIN_YEAR     2038
#define WINAPI_FIN_YEAR     4621

// Год начала отсчета Юлианской даты (до н.э.)
#define JULIAN_YEAR     4713

#define JULIAN_DATE_START_OF_UNIX_TIME  (2440587.5)  //! [days]

#define SECONDS_IN_WEEK (604800.0) //! [s] секунд в неделе

#define MAX_INT32       2147483647

// дней в месяцах
#define DAYS_IN_JAN 31
#define DAYS_IN_MAR 31
#define DAYS_IN_APR 30
#define DAYS_IN_MAY 31
#define DAYS_IN_JUN 30
#define DAYS_IN_JUL 31
#define DAYS_IN_AUG 31
#define DAYS_IN_SEP 30
#define DAYS_IN_OCT 31
#define DAYS_IN_NOV 30
#define DAYS_IN_DEC 31

/*
Описание алгоритмов преобразования форматов времени, используемые
в спутниковых навигационных системах (GPS в частности):
http://www.gmat.unsw.edu.au/snap/gps/gps_survey/chap2/214.htm

Примечание: системы спутниковой навигации не учитывает релятивисткие эффекты
связанные с теорией относительности
*/

//! Преобразования исправно работают по 2100 год!
//! После этого не должна работать GPS/GLONASS в существующем виде

    // Проверка правильности заполнения полей структуры времени/даты
    bool IsUTCTimeValid(TTDateTime utc);

    // Вспомогательные функции
    int GetNumberOfDaysInMonth(int month, int year); // число дней в месяце
    int GetDayOfYear(int day, int month, int year);  // номер дня в году
    bool IsLeapYear(int year); // проверка: високосный год

    // Преобразование структуры времени/дыты в Юлианскую Дату и обратно
    bool GetJulianDateFromUTC(TTDateTime utc, long double *julian_date);
    bool GetUTCfromJulianDate (long double julian_date, TTDateTime *utc);

    // Преобразование системного времени ОС Виндовс в Юлианскую Дату и обратно
    bool GetJulianDateFromSystemTime(SYSTEMTIME systime, long double *julian_date);
    bool GetSystemTimefromJulianDate(long double julian_date, SYSTEMTIME* systime);

    // Преобразование UTC времени UNIX/WIN из библиотеки time.h в Юлианскую Дату и обратно
    bool GetJulianDateFromTM(tm datatime, long double *julian_date);
    bool GetTMfromJulianDate(long double julian_date, tm* datatime);

    // Установка системного времени в ОС Виндовс
    bool SetSystemTimeFromJulianDate(long double julian_date);
    bool SetSystemTimeFromUTC(TTDateTime utc);

    // Преобразование структуры TTDateTime к виду SYSTEMTIME
    bool ConvertUTCToSystemTime(TTDateTime utc, SYSTEMTIME *t);
    // Обратное преобразование
    bool ConvertSystemTimeToUTC(SYSTEMTIME t, TTDateTime *utc);

    // Преобразование инфо о часовом поясе и переходе на сезонное время в Юл.дату
    bool GetJulianDateFromTimezoneInfo(long double *julian_date);

    bool GetJulianDateFromCurentSystemTime(long double *julian_date);

#endif // TIMEMATH_H
