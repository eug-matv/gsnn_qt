﻿#ifndef 	__IEC_H__
#define 	__IEC_H__



#ifdef __cplusplus
extern "C" 
{
#endif	

void * iecMakeDataByStr( const char *str_data, 
					     const void *data);



int iecRazborIEC(const char *str_data,
		   const struct TTDateTime *dt,
		    void *data);


int iecGetDataIEC(
		int type,						//Тип данного
		const void *data,   			//Указатель на данные
		int indx,						   //Индекс	
		unsigned char *out_bytes,		   //Выходной байт	
		int max_sz_out_bytes,			   //размер массива	
		int *sz_out_bytes,				   //Размер данных в байтах
		struct TTDateTime *dt			   //Время получения пакета	 
				);				



#ifdef __cplusplus
}
#endif



#endif
