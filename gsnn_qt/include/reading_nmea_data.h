﻿#ifndef  __READING_NMEA_DATA_H__
#define __READING_NMEA_DATA_H__

#include "iec61161_1tools.h"



class TFIFO_NMEA_DATA
{
    unsigned char ucData[2][MAX_NMEA_SIZE];		//Два массива данных для получения кадров
    
    unsigned char *ucReceivedData;				//Полученная информация включая заголовок
    int szReceivedData;						//Размер полученной информации в байтах
    
    
    unsigned char *ucNewData;					//Получаемая информация на данный момент вкл. заголовок 
    int szNewData;						//Размер получаемой информации в байтах
    unsigned char ucCtrlSum;            //Контрольная сумма
    int iState;                               //Текущее состояние
    
public:
    TFIFO_NMEA_DATA()
    {
	ucReceivedData=ucData[0];
	ucNewData=ucData[1];
	szReceivedData=szNewData=0;
	iState=0;
    };
    
    
//Получение 1 байта данных. Возвращает 1, если байт добавлен. 0 - если байт не добавлен и пакет сброшен 
//2 - если байт добавлен и получен пакет с данными --- который правильный по размеру и его можно читать

    int addByte(unsigned char b);

    int getReceievedData(
                   int &navig_sys,  //Тип навигационной системы 1 - GPS, 2 - ГЛОНАСС, 3 - ГЛОНАСС+GPS,0 - не надо
                   int &mes_type,   //Типы сообщений. Смотреть определенные выще макросы 	
                   char *data      //Данные, включая заголовочные символы и последние два символа
			  );
    
    unsigned char *getReceivedDataUnsafe()
    {       
            if(szReceivedData>0)return ucReceivedData; 
            return (unsigned char*)0;
    };
    int getSizeOfReceivedDataUnsafe()
    {
            if(szReceivedData>0)return szReceivedData; 
            return 0;                                  
    };
    	    
	    
};

#endif
