﻿#ifndef SYNC_PROC_EVENT_H
#define SYNC_PROC_EVENT_H

#ifdef _WIN32

#include <windows.h>

struct TSPEvent
{
    HANDLE handle;
    int isCreate;
};
#else
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
struct TSPEvent
{
    sem_t *sem;
    int isCreate;
    char *name;
};

#endif



struct TSPEvent* spOpenEvent(const char *name,
                             int isCreate,
                             int state);

int spCloseEvent(struct TSPEvent* evnt);

int spSetEvent(struct TSPEvent* evnt);
int spResetEvent(struct TSPEvent* evnt);

int spWaitForEvent(struct TSPEvent* evnt, long timeout_ms);


#endif // SYNC_PROC_EVENT_H
