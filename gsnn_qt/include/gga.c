﻿#include <stdio.h>
#include <string.h>

#include "gga.h"
#include "iec61161_1tools.h"


int ggaRazborGGA(
		   const char *str_data,
		   const struct TTDateTime *dt,
		   struct GGA_DATA *gga_data)
{
   int i=1,k=0,j=0;
    int iRet;	  //
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
    int gr;
	double m;
	
    if(str_data[0]!='$')return 0;
    while(str_data[i])
    {
		summ^=(unsigned char)(str_data[i]);
		if(str_data[i]==','||str_data[i]=='*')
		{
			if(j==0)
			{
				if(k!=5)
				{
					return 0;
				}  
				if(str_data[i-k]!='G'||str_data[i-k+2]!='G'||str_data[i-k+3]!='G'||str_data[i-k+4]!='A')
				{
					return 0;
				}  
				if(str_data[i-k+1]=='P')gga_data->ns=0;
				else if(str_data[i-k+1]=='L')gga_data->ns=1;
				else if(str_data[i-k+1]=='N')gga_data->ns=2;
				else return 0;
			}
			else if(j==1)
			{ 
				//Считаем время UTC
				if(k!=0)
				{  
					int hh,mm;
					double ssss;
					iRet=iecToolsGetTime(str_data+(i-k),&hh,&mm,&ssss);
					if(iRet>0)
					{
						gga_data->sHHMMSS_SS.h=hh;
						gga_data->sHHMMSS_SS.m=mm;
						gga_data->sHHMMSS_SS.s=ssss;
						memcpy(gga_data->dt+j,dt,sizeof(struct TTDateTime));
					}
				}
			}
			else if(j==2)
			{
	  //Считаем широту 
		  
				if(k>0)
				{
					iRet=iecToolsGetLatitudeGrMin(str_data+(i-k),&gr,&m);
					if(iRet<=0)
					{
						m=-1.0;
						gr=-1;
					}	  
				}else{
					m=-1.0;
					gr=-1;
				}  
			}
			else if(j==3)
			{
		  //Счмиаем север это или юг
				if(k!=0&&gr>=0)
				{  
					int ii;
					for(ii=i-k;ii<i;ii++)
					{
						if(str_data[ii]=='N'||str_data[ii]=='S')
						{
							gga_data->sBBBB_BBBB_a.a=str_data[ii];
							gga_data->sBBBB_BBBB_a.gr=gr;
							gga_data->sBBBB_BBBB_a.m=m;
							memcpy(gga_data->dt+(j-1),dt,sizeof(struct TTDateTime));
							break;
						}
					}
				}
			}else if(j==4)
			{
		      //Считаем долготу  
				if(k>0)
				{
					iRet=iecToolsGetLatitudeGrMin(str_data+(i-k),&gr,&m);
					if(iRet<=0)
					{
							m=-1.0;
							gr=-1;
					}	   
				}else{
					m=-1.0;
					gr=-1;
				} 
			}
			else if(j==5)
			{
		 //Считаем запад это или восток
				if(k!=0&&gr>=0)
				{  
					int ii;
					for(ii=i-k;ii<i;ii++)
					{
						if(str_data[ii]=='E'||str_data[ii]=='W')
						{
							gga_data->sLLLL_LLLL_a.a=str_data[ii];
							gga_data->sLLLL_LLLL_a.gr=gr;
							gga_data->sLLLL_LLLL_a.m=m;
							memcpy(gga_data->dt+(j-2),dt,sizeof(struct TTDateTime));
							break;
						}
					}
		   
				}    
			}else if(j==6)
			{	
	//Считаем показатель качества обсервации
				int b;	
				if(k>0)
				{
					iRet=sscanf(str_data+(i-k), "%d", &b);
					if(iRet==1)
					{
						gga_data->b=b;
						memcpy(gga_data->dt+(j-2),dt,sizeof(struct TTDateTime));
					}
				}
			}else if(j==7)
			{
					int cc;
	//Считаем число НКА в решении
					if(k>0)
					{
						iRet=sscanf(str_data+(i-k), "%d", &cc);
						if(iRet==1)
						{	   
							gga_data->cc=cc;
							memcpy(gga_data->dt+(j-2),dt,sizeof(struct TTDateTime));
						}
					}  
			}else if(j==8)
			{
					double d_d;
//Величина горизонтального геометрического фактора
					if(k>0)
					{
						iRet=sscanf(str_data+(i-k),"%lf",&d_d);
						if(iRet==1)
						{
							gga_data->d_d=d_d;
							memcpy(gga_data->dt+(j-2),dt,sizeof(struct TTDateTime));
						}	
					}
			}else if(j==9)
			{
					double e_e;
	//Высота над средним уровнем моря 	
					if(k>0)
					{
						iRet=sscanf(str_data+(i-k),"%lf",&e_e);
						if(iRet==1)
						{	
							gga_data->e_e=e_e;
							memcpy(gga_data->dt+(j-2),dt,sizeof(struct TTDateTime));
						}
					}
			}else if(j==10)
			{
//Величина изменерения - метры --- не будем считывать лишний раз		  
			}else if(j==11)
			{
					double f_f;
      //превышение геода  над эллипсоидом WGS-84
					if(k>0)
					{
						iRet=sscanf(str_data+(i-k),"%lf",&f_f);
						if(iRet==1)
						{	
							gga_data->f_f=f_f;
							memcpy(gga_data->dt+(j-3),dt,sizeof(struct TTDateTime));
						}
					}
			}else if(j==12)
			{
//Величина изменерения - метры --- не будем считывать лишний раз		  
			}else if(j==13)
			{
					double g_g;
 //возраст дифференциальных поправок
					if(k>0)
					{
						iRet=sscanf(str_data+(i-k),"%lf",&g_g);
						if(iRet==1)
						{		
							gga_data->g_g=g_g;
							memcpy(gga_data->dt+(j-4),dt,sizeof(struct TTDateTime));
						}
					}

			}else if(j==14)
			{
					int jjjj;
					if(k>0)
					{  
						iRet=sscanf(str_data+(i-k), "%d", &jjjj);
						if(iRet==1)
						{	
							gga_data->jjjj=jjjj;
							memcpy(gga_data->dt+(j-4),dt,sizeof(struct TTDateTime));
						}
					}
				} 	  
				j++;
				k=0;
		}else{  
				k++;
		}
	
		if(str_data[i]=='*')
		{
				iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
				if(iRet!=1)return 0;
				if(((unsigned int)summ)!=recv_summ)
				{
					return 0;
				}
				break;
		}  
	
		summ^=(unsigned char)(str_data[i]);
		i++;
	};  
	memcpy(gga_data->dt+j, dt,sizeof(struct TTDateTime));	
    return 1;  
}




int ggaMakeGGA(
			   const struct GGA_DATA *gga_data,
			   char *str_data,
			   int size_str_data
			  )
{
	
    int nc=0;
    int i;
    unsigned char ctr_summ=0;
    if(nc+2>size_str_data)return (-1);
    str_data[nc]='$';
    nc+=1;
//Вывести GGA 
    if(nc+6>size_str_data)return (-1);
	if(gga_data->ns==0)
	{	
		memcpy(str_data+nc,"GPGGA,", 6);
	}
	else if(gga_data->ns==1)
	{
		memcpy(str_data+nc,"GLGGA,", 6);
	}else{
		memcpy(str_data+nc,"GNGGA,", 6);
	}	
    nc+=6;
    
//Вывести время
    if(gga_data->sHHMMSS_SS.h<0||gga_data->sHHMMSS_SS.h>23||
	gga_data->sHHMMSS_SS.m<0||gga_data->sHHMMSS_SS.m>=60||
	gga_data->sHHMMSS_SS.s<0||gga_data->sHHMMSS_SS.s>59.99)
    {
	   if(nc+1>size_str_data)return (-1);
	   str_data[nc]=',';
	   nc++;
    }else{
	   if(nc+10>size_str_data)return (-1);
	   sprintf(str_data+nc, "%02d%02d%05.02lf", 
	   gga_data->sHHMMSS_SS.h,gga_data->sHHMMSS_SS.m,gga_data->sHHMMSS_SS.s);
	   str_data[nc+9]=',';
	   nc+=10;
    }  
 
 
 //Вывести широту
    if(gga_data->sBBBB_BBBB_a.gr<0||gga_data->sBBBB_BBBB_a.gr>90||
	   gga_data->sBBBB_BBBB_a.m<0||gga_data->sBBBB_BBBB_a.m>59.99999)
    {
	   if(nc+1>size_str_data)return (-1);
	   str_data[nc]=',';
	   nc++;
    }else{
	   if(nc+10>size_str_data)return (-1);
	   sprintf(str_data+nc, "%02d%07.02lf", 
	   gga_data->sBBBB_BBBB_a.gr,gga_data->sBBBB_BBBB_a.m);
	   str_data[nc+9]=',';
	   nc+=10;
    }  
	
//Выведем север или юг	
    if(gga_data->sBBBB_BBBB_a.a=='N'||gga_data->sBBBB_BBBB_a.a=='S')
    {
    	 if(nc+2>size_str_data)return (-1);
	     sprintf(str_data+nc, "%c", gga_data->sBBBB_BBBB_a.a);
	     str_data[nc+1]=',';
    	 nc+=2;
    }else{
    	 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
    }  

  //Вывести долготу
    if(gga_data->sLLLL_LLLL_a.gr<0||gga_data->sLLLL_LLLL_a.gr>=180||
	   gga_data->sLLLL_LLLL_a.m<0||gga_data->sLLLL_LLLL_a.m>59.99999)
    {
	   if(nc+1>size_str_data)return (-1);
	   str_data[nc]=',';
	   nc++;
    }else{
	   if(nc+11>size_str_data)return (-1);
	   sprintf(str_data+nc, "%03d%07.02lf", 
	   gga_data->sLLLL_LLLL_a.gr,gga_data->sLLLL_LLLL_a.m);
	   str_data[nc+10]=',';
	   nc+=11;
    }  
	
//Выведем Запад или восток	
    if(gga_data->sLLLL_LLLL_a.a=='W'||gga_data->sLLLL_LLLL_a.a=='E')
    {
    	 if(nc+2>size_str_data)return (-1);
	     sprintf(str_data+nc, "%c", gga_data->sLLLL_LLLL_a.a);
	     str_data[nc+1]=',';
    	 nc+=2;
    }else{
    	 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
    }  
 
//Выведем качество обсервации
	if(gga_data->b<0||gga_data->b>2)
	{
		if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
	}else{
		if(nc+2>size_str_data)return (-1);
	     sprintf(str_data+nc, "%d", gga_data->b);
	     str_data[nc+1]=',';
    	 nc+=2;
	}	
		
//Выведем число НКА
	if(gga_data->cc<0||gga_data->cc>=100)
	{
		 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
	}else{
		 if(nc+3>size_str_data)return (-1);
	     sprintf(str_data+nc, "%02d", gga_data->cc);
	     str_data[nc+2]=',';
    	 nc+=3;
	}	
	
 //Вывести геометрический фактор
    if(gga_data->d_d<0)
	{
		if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
	}else{
		 if(nc+10>size_str_data)return (-1);
	     sprintf(str_data+nc, "%1.3lf,", gga_data->d_d);
    	 nc=strlen(str_data);
	}	


 //Вывести высоту над уровнем моря
    if(gga_data->e_e<-12000)
	{
		if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
		 str_data[nc]=',';
	     nc++;
	}else{
		 if(nc+10>size_str_data)return (-1);
	     sprintf(str_data+nc, "%1.1lf,M,", gga_data->e_e);
    	 nc=strlen(str_data);
	}	


//Возвышение геода
    if(gga_data->f_f<-12000)
	{
		if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
		 str_data[nc]=',';
	     nc++;
	}else{
		 if(nc+10>size_str_data)return (-1);
	     sprintf(str_data+nc, "%1.1lf,M,", gga_data->f_f);
    	 nc=strlen(str_data);
	}	


//Возраст диф.поправок
    if(gga_data->g_g<0)
	{
		if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
	}else{
		 if(nc+8>size_str_data)return (-1);
	     sprintf(str_data+nc, "%1.1lf,", gga_data->g_g);
    	 nc=strlen(str_data);
	}	


//Выведем идентификатор диффер станции
    if(gga_data->jjjj<0||gga_data->jjjj>1023)
	{
		if(nc+1>size_str_data)return (-1);
		str_data[nc]='*';
	}else{
		if(nc+4>size_str_data)return (-1);
	     sprintf(str_data+nc, "%04d,", gga_data->jjjj);
    	 nc+=4;
		 str_data[nc]='*';
	}

    
    
 //Теперь расчитаем контрольную сумму   
    for(i=1;i<nc;i++)
    {
	  ctr_summ^=(unsigned char)(str_data[i]);
    }
    nc++;
    sprintf(str_data+nc, "%02X",ctr_summ);
    nc+=2;
    str_data[nc]=0x0D;
    str_data[nc+1]=0x0A;
    nc+=2;
    return nc;
	
}			  



int ggaGetDataGGA(
		const struct GGA_DATA *gga_data,   //Указатель 
		int indx,						   //Индекс	
		unsigned char *out_bytes,		   //Выходной байт	
		int max_sz_out_bytes,			   //размер массива	
		int *sz_out_bytes,				   //Размер данных в байтах
		struct TTDateTime *dt			   //Время получения пакета	 
				)				
{
	if(gga_data==0||max_sz_out_bytes<4)return 0;
	if(indx<1||indx>10)return -1;
	
	switch(indx)
	{
		case 1:
			if(sizeof(gga_data->sHHMMSS_SS)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->sHHMMSS_SS),sizeof(gga_data->sHHMMSS_SS));
			*sz_out_bytes=sizeof(gga_data->sHHMMSS_SS);
		break;
		
		case 2:
		    if(sizeof(gga_data->sBBBB_BBBB_a)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->sBBBB_BBBB_a),sizeof(gga_data->sBBBB_BBBB_a));
			*sz_out_bytes=sizeof(gga_data->sBBBB_BBBB_a);
		break;
		
		case 3:
	        if(sizeof(gga_data->sLLLL_LLLL_a)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->sLLLL_LLLL_a),sizeof(gga_data->sLLLL_LLLL_a));
			*sz_out_bytes=sizeof(gga_data->sLLLL_LLLL_a);		
		break;
		
		case 4:
			if(sizeof(gga_data->b)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->b),sizeof(gga_data->b));
			*sz_out_bytes=sizeof(gga_data->b);		
		break;
		
		case 5:
			if(sizeof(gga_data->cc)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->cc),sizeof(gga_data->cc));
			*sz_out_bytes=sizeof(gga_data->cc);				
		break;	
		
		case 6:
			if(sizeof(gga_data->d_d)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->d_d),sizeof(gga_data->d_d));
			*sz_out_bytes=sizeof(gga_data->d_d);				
		break;	
			
		case 7:
			if(sizeof(gga_data->e_e)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->e_e),sizeof(gga_data->e_e));
			*sz_out_bytes=sizeof(gga_data->e_e);						
		break;
		
		case 8:
			if(sizeof(gga_data->f_f)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->f_f),sizeof(gga_data->f_f));
			*sz_out_bytes=sizeof(gga_data->f_f);	
		break;
		
		case 9:
			if(sizeof(gga_data->g_g)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->g_g),sizeof(gga_data->g_g));
			*sz_out_bytes=sizeof(gga_data->g_g);			
		break;
		
		case 10:
			if(sizeof(gga_data->jjjj)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(gga_data->jjjj),sizeof(gga_data->jjjj));
			*sz_out_bytes=sizeof(gga_data->jjjj);			
		break;


		default:
			return (-3);
		
	};
		
	memcpy(dt,gga_data+indx,sizeof(struct TTDateTime));
	return 1;
}		
