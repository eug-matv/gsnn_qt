﻿#include <stdlib.h>
#include <string.h>
#include "gga.h"
#include "gll.h"
#include "gsa.h"
#include "gsv.h"
#include "rmc.h"
#include "vtg.h"
#include "zda.h"
#include "pirea.h"
#include "pirfv.h"
#include "pirgk.h"
#include "pirra.h"
#include "iec.h"
#include "iec61161_1tools.h"



void* iecMakeDataByStr( const char *str_data, 
					     const void *data)
{
	int type;
	void *data1;
	if(data)return ((void*)data);
    type=iecToolsTypeOfPackage((unsigned char*)str_data);
	switch(type)
	{
           case NMEA_GPGGA:
           case NMEA_GLGGA:
           case NMEA_GNGGA:
			   data1=malloc(sizeof(struct GGA_DATA));
			   if(data1)memset(data1,0,sizeof(struct GGA_DATA));
               return data1; 
           break;          
           
           case NMEA_GPGLL:
           case NMEA_GLGLL:
           case NMEA_GNGLL:
               data1=malloc(sizeof(struct GLL_DATA));
			   if(data1)memset(data1,0,sizeof(struct GLL_DATA));
               return data1; 
           break;          
           
           case NMEA_GPGSA:
           case NMEA_GLGSA:
           case NMEA_GNGSA:
               data1=malloc(sizeof(struct GSA_DATA));
			   if(data1)memset(data1,0,sizeof(struct GSA_DATA));
               return data1; 
           break;          
           
           case NMEA_GPGSV:
           case NMEA_GLGSV:
           case NMEA_GNGSV:
               data1=malloc(sizeof(struct GSV_DATA));
			   if(data1)memset(data1,0,sizeof(struct GSV_DATA));
               return data1; 
           break;          

           case NMEA_GPRMC:
           case NMEA_GLRMC:
           case NMEA_GNRMC:
               data1=malloc(sizeof(struct RMC_DATA));
			   if(data1)memset(data1,0,sizeof(struct RMC_DATA));
               return data1; 
           break;     
           
           case NMEA_GPVTG:
           case NMEA_GLVTG:
           case NMEA_GNVTG:
               data1=malloc(sizeof(struct VTG_DATA));
			   if(data1)memset(data1,0,sizeof(struct VTG_DATA));
               return data1; 
           break;     

           case NMEA_GPZDA:
           case NMEA_GLZDA:
           case NMEA_GNZDA:
               data1=malloc(sizeof(struct ZDA_DATA));
			   if(data1)memset(data1,0,sizeof(struct ZDA_DATA));
               return data1; 
           break;     
           
           case IEC_PIREA:
               data1=malloc(sizeof(struct PIREA_DATA));
			   if(data1)memset(data1,0,sizeof(struct PIREA_DATA));
               return data1; 
           break;         

           case IEC_PIRFV:
               data1=malloc(sizeof(struct PIRFV_DATA));
			   if(data1)memset(data1,0,sizeof(struct PIRFV_DATA));
               return data1; 
           break;         

           case IEC_PIRGK:
               data1=malloc(sizeof(struct PIRGK_DATA));
			   if(data1)memset(data1,0,sizeof(struct PIRGK_DATA));
               return data1; 
           break;         

           case IEC_PIRRA:
               data1=malloc(sizeof(struct PIRRA_DATA));
			   if(data1)memset(data1,0,sizeof(struct  PIRRA_DATA));
               return data1; 
           break;         
	};
	return 0;
	
}


int iecRazborIEC(const char *str_data,
		   const struct TTDateTime *dt,
		    void *data
			)
{
		int type;
		int iRet;
        type=iecToolsTypeOfPackage((unsigned char*)str_data);
		
		switch(type)
		{
		   case NMEA_GPGGA:
           case NMEA_GLGGA:
           case NMEA_GNGGA:
		     
               iRet=ggaRazborGGA(str_data,dt,(struct GGA_DATA*)data);	
		   break;
		   
		   case NMEA_GPRMC:
           case NMEA_GLRMC:
           case NMEA_GNRMC:
               iRet=rmcRazborRMC(str_data,dt,(struct RMC_DATA*)data);	
		   break;
		   
		   default:
				return (-1);
			   
		};
		
		return iRet;
}			


int iecGetDataIEC(
		int type,						//Тип данного
		const void *data,   			//Указатель на данные
		int indx,						   //Индекс	
		unsigned char *out_bytes,		   //Выходной байт	
		int max_sz_out_bytes,			   //размер массива	
		int *sz_out_bytes,				   //Размер данных в байтах
		struct TTDateTime *dt			   //Время получения пакета	 
				)
{
	

		int iRet;
		if(!data)return 0;
		switch(type)
		{
		   case NMEA_GPGGA:
           case NMEA_GLGGA:
           case NMEA_GNGGA:
		     
               iRet=ggaGetDataGGA((struct GGA_DATA*)data,
			       indx,out_bytes, max_sz_out_bytes, sz_out_bytes, dt);	
		   break;
		   
		   case NMEA_GPRMC:
           case NMEA_GLRMC:
           case NMEA_GNRMC:
               iRet=rmcGetDataRMC((struct RMC_DATA*)data,
			       indx,out_bytes, max_sz_out_bytes, sz_out_bytes, dt);	
 		   break;
		   
		   default:
				return (-1);
			   
		};
		
		return iRet;
	
}

