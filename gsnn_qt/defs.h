﻿#ifndef DEFS_H
#define DEFS_H

/*
====================================================
Заголовок для объявления назвния и версии приложения
====================================================
*/

#define APP_TITLE "GSNN MNP7 Client"

#define APP_version "0.1.1"

/*
====================================================
Коды ошибок
====================================================
*/

// проблемы соединения
#define PORT0_NMEA_FAIL         0001
#define PORT0_MNP_FAIL          0002
#define PORT1_MNP_FAIL          0003

// отсутствие данных
#define PORT0_NMEA_GP_NODATA    0011
#define PORT0_NMEA_GL_NODATA    0012
#define PORT0_NMEA_GN_NODATA    0013
#define PORT0_MNP_NODATA        0014
#define PORT1_MNP_NODATA        0015

// обрыв линии связи (данные одинаковые)

#define PORT0_NMEA_GP_LINECRUSH 0021
#define PORT0_NMEA_GL_LINECRUSH 0022
#define PORT0_NMEA_GN_LINECRUSH 0023
#define PORT0_MNP_LINECRUSH     0024
#define PORT1_MNP_LINECRUSH     0025

// неверный формат времени
#define TIMEFORMAT_ERROR        0300

// неверный формат файла конфигурации
#define CONFIG_FILE_ERROR       0400

// Проблема 2038 года для 32bit UNIX и С, C++ программ "time.h"
#define YEAR_2038_ERROR         2038

#endif // DEFS_H
