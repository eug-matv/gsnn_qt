<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="26"/>
        <source>GSNN_qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="70"/>
        <source>RMC data</source>
        <translation>RMC данные</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="91"/>
        <source>Type of sys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="98"/>
        <location filename="mainwindow/mainwindow.ui" line="124"/>
        <location filename="mainwindow/mainwindow.ui" line="150"/>
        <location filename="mainwindow/mainwindow.ui" line="176"/>
        <location filename="mainwindow/mainwindow.ui" line="202"/>
        <location filename="mainwindow/mainwindow.ui" line="228"/>
        <location filename="mainwindow/mainwindow.ui" line="254"/>
        <location filename="mainwindow/mainwindow.ui" line="286"/>
        <location filename="mainwindow/mainwindow.ui" line="312"/>
        <location filename="mainwindow/mainwindow.ui" line="353"/>
        <location filename="mainwindow/mainwindow.ui" line="410"/>
        <source>LastTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="117"/>
        <source>UTC Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="143"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="169"/>
        <source>Latitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="195"/>
        <source>Longitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="221"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="247"/>
        <source>Course</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="279"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="305"/>
        <source>MagnOtklon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="346"/>
        <source>RejimMest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.ui" line="392"/>
        <source>Sputniks count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="22"/>
        <location filename="mainwindow/mainwindow.cpp" line="212"/>
        <location filename="mainwindow/mainwindow.cpp" line="234"/>
        <location filename="mainwindow/mainwindow.cpp" line="394"/>
        <source>Satellites: not known</source>
        <translation>Спутников: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="34"/>
        <source>The configuration file is not found. It should in the same folder as the program</source>
        <translation>Не найден файл конфигурации. Он должен быть в той же папке, что и программа</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="38"/>
        <source>Cannot read the configuration file</source>
        <translation>Невозможно прочитать файл конфигурации</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="43"/>
        <source>Error in the configuration file: Stopbits !=0, 1, 2</source>
        <translation>Ошибка в конфигурационном файле: StopBits != 0, 1, 2</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="47"/>
        <source>Error in configuration file: Protocol value not equal to 0, 1</source>
        <translation>Ошибка в конфигурационном файле: Protocol != 0, 1</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="52"/>
        <source>Error in configuration file: (SyncPeriod &lt;= 0) || (SyncPeriod &gt; 600000)</source>
        <translation>Ошибка в конфигурационном файле: (SyncPeriod &lt;= 0) || (SyncPeriod &gt; 600000)</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="57"/>
        <source>Error in configuration file:TimeConstant &lt;-7200000 || &gt; 7200000</source>
        <translation>Ошибка в конфигурационном файле: TimeConstant &lt;-7200000    &gt; 7200000</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="109"/>
        <source>Show Window</source>
        <translation>Показать окно</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="112"/>
        <source>Hide Window</source>
        <translation>Скрыть окно</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="115"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="158"/>
        <location filename="mainwindow/mainwindow.cpp" line="438"/>
        <source>hh:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="159"/>
        <location filename="mainwindow/mainwindow.cpp" line="439"/>
        <source> </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="184"/>
        <source>System type: unknown</source>
        <translation>Тип системы: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="185"/>
        <source>UTC time: unknown</source>
        <translation>Время UTC: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="186"/>
        <location filename="mainwindow/mainwindow.cpp" line="308"/>
        <source>Status: unknown</source>
        <translation>Статус: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="187"/>
        <source>Latitude: unknown</source>
        <translation>Широта: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="188"/>
        <source>Longitude: unknown</source>
        <translation>Долгота: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="189"/>
        <source>Ground speed: unknown</source>
        <translation>Наземн.скор в узлах: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="190"/>
        <source>Ground heading: unknown</source>
        <translation>Наземн.курс в гр.: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="191"/>
        <source>Date: unknown</source>
        <translation>Дата: Нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="192"/>
        <source>Magnetic deflection: unknown</source>
        <translation>Магн.отклонение: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="193"/>
        <source>Place detection mode: unknown</source>
        <translation>Режим местоопределения: нет данных</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="208"/>
        <source>The data is not received within </source>
        <translation>Данные не поступали  в течение </translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="209"/>
        <location filename="mainwindow/mainwindow.cpp" line="231"/>
        <source> seconds</source>
        <translation> сек</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="222"/>
        <source>Data began to flow again</source>
        <translation>Данные стали поступать снова</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="230"/>
        <source>Was gps/GLONASS data for </source>
        <translation>Не было данных gps/глонасс в течение </translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="244"/>
        <source>Data gps/GLONASS began to come again</source>
        <translation>Данные gps/глонасс стали поступать снова</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="275"/>
        <source>System type: GPS</source>
        <translation>Тип системы: GPS</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="279"/>
        <source>System type: GLONASS</source>
        <translation>Тип системы: ГЛОНАСС</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="283"/>
        <source>System type: GPS+GLONASS</source>
        <translation>Тип системы: GPS+ГЛОНАСС</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="285"/>
        <source>System type: error!</source>
        <translation>Тип системы: ошибка!</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="298"/>
        <source>Status: the solution is not valid</source>
        <translation>Статус: решение не годно</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="302"/>
        <source>Status: autonomous mode</source>
        <translation>Статус: автономный режим</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="306"/>
        <source>Status: differential mode</source>
        <translation>Статус: дифферинциальный режим</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="349"/>
        <source>Place detection mode:  autonomous</source>
        <translation>Режим местоопределения: автономный</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="353"/>
        <source>Place detection mode:  differential</source>
        <translation>Режим местоопределения: дифферинциальный</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="357"/>
        <source>Place detection mode:  expected</source>
        <translation>Режим местоопределения: ожидаемый</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="361"/>
        <source>Place detection mode:  manual input</source>
        <translation>Режим местоопределения: ручной ввод</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="365"/>
        <source>Place detection mode:  imitation</source>
        <translation>Режим местоопределения: имитация</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="369"/>
        <source>Place detection mode:   not valid</source>
        <translation>Режим местоопределения:  не годно</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="371"/>
        <source>Place detection mode:  unknown</source>
        <translation>Режим местоопределения: неизвестно</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="388"/>
        <source>Satellites: </source>
        <translation>Спутников: </translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="405"/>
        <source>There is no information about time and date</source>
        <translation>Нет информации о времени и дате</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="414"/>
        <source>Error setting system time</source>
        <translation>Ошибка установки системного времени</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="422"/>
        <source>Time near midnight - do nothing</source>
        <translation>Время возле полуночи - ничего не делаем</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="429"/>
        <source>System time is set successfully</source>
        <translation>Установка системного времени прошла успешно</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="451"/>
        <source>Critical error!</source>
        <translation>Критическая ошибка!</translation>
    </message>
    <message>
        <location filename="mainwindow/mainwindow.cpp" line="452"/>
        <source>Description of the error: </source>
        <translation>Описание ошибки: </translation>
    </message>
</context>
<context>
    <name>globalThread</name>
    <message>
        <location filename="globalthread.cpp" line="86"/>
        <location filename="globalthread.cpp" line="97"/>
        <source>gsnn_qt.log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="87"/>
        <location filename="globalthread.cpp" line="99"/>
        <source>$gsnn_qt$.log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="96"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="101"/>
        <location filename="globalthread.cpp" line="105"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="122"/>
        <source>Getting started program gsnn_qt</source>
        <translation>Начало работы gsnn_qt</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="123"/>
        <source>The end of the program gsnn_qt</source>
        <translation>Конец работы gsnn_qt</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="145"/>
        <source>The working directory is not detected settings file for the program. </source>
        <translation>В рабочем каталоге не обнаружен файл с настройками для работы программы.</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="184"/>
        <source>Setting options for reading GPS data from serial port
</source>
        <translation type="unfinished">Установка опций для чтения GPS-данных с последовательного порта
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="202"/>
        <source>Setting options for reading the GPS data from the serial port is successful
</source>
        <translation type="unfinished">Установка опций для чтения GPS-данных с последовательного порта прошла успешно
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="219"/>
        <location filename="globalthread.cpp" line="381"/>
        <source>The application runs successfully with the settings:
</source>
        <translation type="unfinished">Приложение успешно запущено с настройками:
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="350"/>
        <source>The main thread is stopped 
</source>
        <translation type="unfinished">Главный поток остановлен
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="454"/>
        <source>Launching a secondary thread to read information from the serial port
</source>
        <translation type="unfinished">Запуск вторичного потока для чтения информации с последовательного порта
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="460"/>
        <source>Error starting the secondary thread read the data: 
</source>
        <translation type="unfinished">Ошибка запуска вторичного потока чтения данных: 
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="463"/>
        <source>Secondary thread to read data from the port is already running
</source>
        <translation type="unfinished">Вторичный поток чтения данных с порта уже запущен
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="464"/>
        <source>Secondary thread to read data from the port is already running</source>
        <translation>Вторичный поток чтения данных с порта уже запущен</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="470"/>
        <source>Launching a secondary thread to read information has been completed successfully
</source>
        <translation type="unfinished">Запуск вторичного потока для чтения информации был успешно завершен
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="534"/>
        <source>the input to the function SyncTimeout()</source>
        <translation>Вошли в функцию SyncTimeout(</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="545"/>
        <source>There is no accurate data on time</source>
        <translation>Нет коректных данных о времени</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="578"/>
        <source>Runtime error: GetJulianDateFromUTC(*UTC_rmc, &amp;JD_UTC_rmc)</source>
        <translation>Ошибка выполнения GetJulianDateFromUTC(*UTC_rmc, &amp;JD_UTC_rmc</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="582"/>
        <source>Successful execution of the function GetJulianDateFromUTC(*UTC_rmc, &amp;JD_UTC_rmc)</source>
        <translation>Успешное выполнение GetJulianDateFromUTC(*UTC_rmc, &amp;JD_UTC_rmc)</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="593"/>
        <source>Runtime error: GetJulianDateFromUTC(*LocalFix_rmc, &amp;JD_LocalFix_rmc)</source>
        <translation>Ошибка выполнения GetJulianDateFromUTC(*LocalFix_rmc, &amp;JD_LocalFix_rmc)</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="597"/>
        <source>Successful execution of the function GetJulianDateFromUTC(*LocalFix_rmc, &amp;JD_LocalFix_rmc)</source>
        <translation>Успешное выполнение GetJulianDateFromUTC(*LocalFix_rmc, &amp;JD_LocalFix_rmc)</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="606"/>
        <source>Successful execution of the function GetJulianDateFromTimezoneInfo(&amp;JD_TimeZone)</source>
        <translation>Успешное выполнение GetJulianDateFromTimezoneInfo(&amp;JD_TimeZone)</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="618"/>
        <source>Runtime error: GetJulianDateFromCurentSystemTime(&amp;JD_SystemTime)</source>
        <translation>Ошибка выполнения GetJulianDateFromCurentSystemTime(&amp;JD_SystemTime)</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="622"/>
        <source>Successful execution of the function GetJulianDateFromCurentSystemTime(&amp;JD_SystemTime)</source>
        <translation>Успешное выполнение GetJulianDateFromCurentSystemTime(&amp;JD_SystemTime)</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="637"/>
        <source>Runtime error: GetUTCfromJulianDate (JD_SystemTime, CurentUTC)</source>
        <translation>Ошибка выполнения GetUTCfromJulianDate (JD_SystemTime, CurentUTC)</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="641"/>
        <source>Successful execution of the function GetUTCfromJulianDate (JD_SystemTime, CurentUTC)</source>
        <translation>Успешное выполнение GetUTCfromJulianDate (JD_SystemTime, CurentUTC)</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="659"/>
        <source>Error setting system time
</source>
        <translation type="unfinished">Ошибка установки системного времени
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="667"/>
        <source>Successful execution of the function SetSystemTimeFromUTC</source>
        <translation>Успешное выполнение SetSystemTimeFromUTC</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="731"/>
        <source>There is a problem with reading the file with settings for the program.</source>
        <translation>Возникла проблема с чтением файла с настройками для работы программы.</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="801"/>
        <source>Error in configuration file: StopBits is not 0, 1 and 2.</source>
        <translation>Ошибка в конфигурационном файле: StopBits != 0, 1, 2.</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="810"/>
        <source>Error in configuration file: Protocol is not 0 and 1.</source>
        <translation>Ошибка в конфигурационном файле: Protocol != 0, 1.</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="825"/>
        <source>Error in configuration file: SyncPeriod &lt;=0 or &gt; 600000</source>
        <translation>Ошибка в конфигурационном файле: SyncPeriod &lt;=0  &gt; 600000</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="835"/>
        <source>Error in configuration file: TimeConstant &lt;-7200000 or &gt; 7200000</source>
        <translation>Ошибка в конфигурационном файле: TimeConstant &lt;-7200000  &gt; 7200000</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="848"/>
        <source>The system failed to work with the TimeZone.
 Try to set the TimeZone or set the options ifNoTimeZoneSetThis=1
and Bias_min and DaylightsBias_min in the file gsnn_config.ini</source>
        <translation>В системе ошибка работы с TimeZone.
Попробуйте настроить TimeZone или установить
параметры ifNoTimeZoneSetThis=1, а так же  Bias_min и DaylightsBias_min  в файле gsnn_config.ini</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="919"/>
        <location filename="globalthread.cpp" line="1055"/>
        <location filename="globalthread.cpp" line="1101"/>
        <source>127.0.0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="926"/>
        <source>In the method globalThread::receivedDataSlot error sending data to 127.0.0.1: %d</source>
        <translation>В методе globalThread::receivedDataSlot ошибка отправки данных на 127.0.0.1:%d</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="939"/>
        <source>In the method globalThread::receivedDataSlot connection error 127.0.0.1:%d</source>
        <translation>В методе globalThread::receivedDataSlot ошибка соединения 127.0.0.1:%d</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="999"/>
        <source>The data is not received within %d seconds 
</source>
        <translation>Данные не поступали  в течении %d сек
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1011"/>
        <source>The data come again 
</source>
        <translation>Данные стали поступать снова
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1018"/>
        <source>No gps data was within %d seconds  
</source>
        <translation>Не было данных gps в течении %d сек 
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1031"/>
        <source>Gps data began to come again 
</source>
        <translation>Данные gps стали поступать снова 
</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1062"/>
        <source>In the method globalThread::noDataEventForOtherSlot error sending data to 127.0.0.1:%d</source>
        <translation>В методе globalThread::noDataEventForOtherSlot ошибка отправки данных на 127.0.0.1:%d</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1074"/>
        <source>In the method globalThread::noDataEventForOtherSlot error connection 127.0.0.1:%d</source>
        <translation>В методе globalThread::noDataEventForOtherSlot ошибка соединения 127.0.0.1:%d</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1108"/>
        <source>In the method globalThread::noPackageEventForOtherSlot error sending data to 127.0.0.1:%d</source>
        <translation>В методе globalThread::noPackageEventForOtherSlot ошибка отправки данных на 127.0.0.1:%d</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1120"/>
        <source>In the method globalThread::noPackageEventForOtherSlot error connection 127.0.0.1::%d</source>
        <translation>В методе globalThread::noPackageEventForOtherSlot ошибка соединения 127.0.0.1:%d</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1139"/>
        <location filename="globalthread.cpp" line="1166"/>
        <location filename="globalthread.cpp" line="1193"/>
        <source>Error allocating memory for device  </source>
        <translation>Ошибка выделения памяти для устройства</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1143"/>
        <source>Error opening  </source>
        <translation>Ошибка открытия порта</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1144"/>
        <source> port to receive data from GPS/GLONASS receiver</source>
        <translation> для получения данных с GPS/ГЛОНАСС-приемника</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1148"/>
        <location filename="globalthread.cpp" line="1175"/>
        <location filename="globalthread.cpp" line="1202"/>
        <source>Error setting port settings </source>
        <translation>Ошибка установки параметров порта </translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1149"/>
        <location filename="globalthread.cpp" line="1176"/>
        <location filename="globalthread.cpp" line="1203"/>
        <source> port: Baudrate: </source>
        <translation> Скорость: </translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1151"/>
        <source> to receive data from GPS/GLONASS receiver</source>
        <translation> для получения данных с GPS/ГЛОНАСС-приемника</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1154"/>
        <location filename="globalthread.cpp" line="1181"/>
        <location filename="globalthread.cpp" line="1208"/>
        <source>Error associated with the serial port emulation via file </source>
        <translation>Ошибка, связанные с эмуляцией последовательного порта через файл </translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1170"/>
        <location filename="globalthread.cpp" line="1197"/>
        <source>Error opening </source>
        <translation>Ошибка открытия порта</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1171"/>
        <source> port to transfer data to remote host 1</source>
        <translation> для передачи данных на удаленный узел 1</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1177"/>
        <location filename="globalthread.cpp" line="1204"/>
        <source> stop bit: </source>
        <translation> Стоп-бит: </translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1178"/>
        <source> to transfer data to remote host 1</source>
        <translation> для передачи данных на удаленный узел 1</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1198"/>
        <source> port to transfer data to remote host 2</source>
        <translation> для передачи данных на удаленный узел 2</translation>
    </message>
    <message>
        <location filename="globalthread.cpp" line="1205"/>
        <source> to transfer data to remote host 2</source>
        <translation> для передачи данных на удаленный узел 2</translation>
    </message>
</context>
</TS>
