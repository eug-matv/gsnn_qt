﻿//---------------------------------------------------------------------------
//
// Функции для работы с протоколами
//
// НТЦ ОКБ ЧРЗ "Полет"
// Инженер-конструктор I категории
// Рацебуржинский С.Л.
// октябрь 2007
//
//---------------------------------------------------------------------------



#include <windowsx.h>
#include <windows.h>
#include <conio.h>


#include <stdio.h>

#include <stdlib.h>
#include "protocol.h"

#include <string>
#include <iostream>

using namespace std;

#define NameProtokolFileDirectory "%s\\%02d"
#define NameProtokolFile "%s\\%02d\\%s"
#define ERR001 "Probably at this point, the file was opened by another application, see the whole file in the folder of another day."
//---------------------------------------------------------------------------
// ведение протокола.
// производится потоянная запись в служебный файл,
// а по таймеру производиться копирование в нужный файл
// nm номер протокола
// dirname каталог содержащий папки с именами "1"-"31"
TProtocol::TProtocol(	/*prtcl_char* dirname, 
						prtcl_char* tempfile, 
						int nm, 
						prtcl_char* frotokolfile, 
						prtcl_char* tpmFileName,
						prtcl_char* _StartMes,
						prtcl_char* _StopMes*/)
{
	noflush = false;
	stop = false;
	filebusy = false;
	fp_tmp = NULL;
	//Init(dirname, tempfile, nm, frotokolfile, tpmFileName, _StartMes, _StopMes);

    //HND = CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)(&(TProtocol::PrtThread)), 
	//	this, NULL, &thID);
}


void TProtocol::Init(	prtcl_char* dirname, 
						prtcl_char* tempfile, 
						int nm, 
						prtcl_char* frotokolfile, 
                        prtcl_char* tpmFileName,
						prtcl_char* _StartMes,
						prtcl_char* _StopMes)
{
	NUM = nm;
	strcpy(StartMes, _StartMes);
	strcpy(StopMes, _StopMes);
	strcpy(ProtokolFile, frotokolfile);
	strcpy(DirName, dirname);
	// временный файл
    prtcl_char ch[2256];
    if (tempfile==NULL)
    {
#ifdef _WIN32
        GetWindowsDirectory(ch,sizeof(ch));
#else
        getcwd(ch,sizeof(ch)-1);
#endif
    }
    else
    {
            strcpy(ch,tempfile);
    }
#ifdef _WIN32
        sprintf(tpmFile, "%s\\%s", ch, tpmFileName);
        SYSTEMTIME stUTC, fileTime;
#else
        sprintf(tpmFile, "%s//%s", ch, tpmFileName);
//        TTDateTime st
#endif
    // текущее время и дата
    // преобразовать время модификации в локальное время.
    GetSystemTime(&stUTC);
    SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &Start);
    // время последнего открытия файла
    // преобразовать время модификации в локальное время.
    HANDLE hFindFile;
    WIN32_FIND_DATA FindFileData;
    hFindFile = FindFirstFile(tpmFile, &FindFileData);
    FindClose(hFindFile);
    FileTimeToSystemTime(&FindFileData.ftLastWriteTime, &stUTC);
    SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &fileTime);
    // проверка, когда открыт файл?
    // если файл открывался в текушем часу то продолжаем запись в файл,
    // если нет то создается новый файл
    SetFileAttributes(tpmFile, FILE_ATTRIBUTE_NORMAL);
    Sleep(10);    
    if ((Start.wYear==fileTime.wYear)&&(Start.wMonth==fileTime.wMonth)&&
        (Start.wDay==fileTime.wDay))//&&(Start.wHour==fileTime.wHour))
        fp_tmp = fopen(tpmFile,"a+");
    else
	{
		fp_tmp = fopen(tpmFile,"wt");
	}
    if (fp_tmp==NULL)
    {
        prtcl_char tmp[256];
        sprintf(tmp,"Can't open the log file.\n%s",tpmFile);
		//MessageBox(NULL,tmp,"Ошибка протокола", MB_OK);
        return;
    }
    SetFileAttributes(tpmFile, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM);
    Sleep(10);
    fprintf(fp_tmp, "\n\n===========================================================================================");
    fprintf(fp_tmp, "\n<<< %02d:%02d.%02d %d.%02d.%02d %s.",
    Start.wHour, Start.wMinute, Start.wSecond, Start.wYear, Start.wMonth, Start.wDay, StartMes);
    fprintf(fp_tmp, "\n  < %02d:%02d.%02d %d.%02d.%02d Start Protocol № %d.",
    Start.wHour, Start.wMinute, Start.wSecond, Start.wYear, Start.wMonth, Start.wDay,  NUM);
    // если каталог DirName не существует, создаем его
	if (CreateDirectory(DirName, NULL)) 
	{ 
        fprintf(fp_tmp, "\n  < %02d:%02d.%02d %d.%02d.%02d Created the directory: %s.",
		Start.wHour, Start.wMinute, Start.wSecond, Start.wYear, Start.wMonth, Start.wDay,  DirName);
	} 
    sprintf(SaveFile, NameProtokolFileDirectory, DirName ,Start.wDay);
    // если каталог SaveFile не существует, создаем его
	if (CreateDirectory(SaveFile, NULL)) 
	{ 
        fprintf(fp_tmp, "\n  < %02d:%02d.%02d %d.%02d.%02d Created the directory: %s.",
		Start.wHour, Start.wMinute, Start.wSecond, Start.wYear, Start.wMonth, Start.wDay,  SaveFile);
	} 
	sprintf(SaveFile, NameProtokolFile, DirName ,Start.wDay, ProtokolFile);
    LastSave = Start;
    Local = Start;
	fclose(fp_tmp);
	fp_tmp=NULL;
	newdata = true;
	filebusy = false;
	fp_tmp = fopen(tpmFile,"a+");

	HND = CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)(&(TProtocol::PrtThread)), 
		this, NULL, &thID);
}

TProtocol::~TProtocol()
{
	Free();
}

void TProtocol::Free()
{
    // завершение работы потока
	int k;
	stop = true;
	for (k=0; k<500; k++) if ((!PrtThreadWork)&&(!filebusy)) break; else Sleep(100);

	//DWORD dwExitCode = 0;
	//if (HND!=NULL)
	//TerminateThread(HND, dwExitCode);
	CloseHandle(HND);
	
	// текущее время и дата
	SYSTEMTIME stUTC;
    // преобразовать время модификации в локальное время.
    GetSystemTime(&stUTC);
    SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &Local);
	// закрытие программы, последняя запись в протокол
	fp_tmp = fopen(tpmFile,"a+");
    fprintf(fp_tmp, "\n  > %02d:%02d.%02d %d.%02d.%02d Stop Protocol № %d",
    Local.wHour, Local.wMinute,Local.wSecond, Local.wYear, Local.wMonth, Local.wDay,  NUM);
    fprintf(fp_tmp, "\n>>> %02d:%02d.%02d %d.%02d.%02d %s.",
    Local.wHour, Local.wMinute,Local.wSecond, Local.wYear, Local.wMonth, Local.wDay, StopMes);
    fprintf(fp_tmp, "\n===========================================================================================");
    fclose(fp_tmp);
	fp_tmp=NULL;
	if (!CopyFile (tpmFile,SaveFile,false))
	{
		// если скопировать не удалось, переписываем в новый файл
		prtcl_char tmp[1024];
		sprintf(tmp, "%s%s", SaveFile, "_$");
		CopyFile (tpmFile,tmp,false);
		SetFileAttributes(tmp, FILE_ATTRIBUTE_NORMAL);
		// если так не копируется, значит не судьба  :(
	};
	SetFileAttributes(SaveFile, FILE_ATTRIBUTE_NORMAL);
}

void TProtocol::ProtocolRoutine()
{
    // текущее время и дата
	SYSTEMTIME stUTC;
    // преобразовать время модификации в локальное время.
    GetSystemTime(&stUTC);
    SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &Local);
	// проверяем сменился или нет день
    int NewLocation = 0;
    if ((Local.wYear!=LastSave.wYear)||(Local.wMonth!=LastSave.wMonth)||
        (Local.wDay!=LastSave.wDay))//||(Local.wHour!=LastSave.wHour))
        NewLocation = 1;

    // новый день
	if (NewLocation)
    {
		// последняя запись в файл
		fclose(fp_tmp);
        fp_tmp = fopen(tpmFile,"a+");
        fprintf(fp_tmp, "\n\n    %02d:%02d.%02d %d.%02d.%02d Change the file Protocol № %d.",
        Local.wHour, Local.wMinute,Local.wSecond, Local.wYear, Local.wMonth, Local.wDay,  NUM);
        fclose(fp_tmp);
		fp_tmp=NULL;
        // копирование
        int FCPY=CopyFile (tpmFile,SaveFile,false);
        SetFileAttributes(SaveFile, FILE_ATTRIBUTE_NORMAL);
        Sleep(10);
		// новый файл, если копирование прошло успешно
        if (FCPY)
        {
            DeleteFile(tpmFile);
			fp_tmp = fopen(tpmFile,"wt");
        }
		// если копирование не произвелось, дописываем существующий файл
        else
        {
            fp_tmp = fopen(tpmFile,"a+");
            fprintf(fp_tmp, "\n    %02d:%02d.%02d %d.%02d.%02d It is impossible to copy the file.\n    %s",
            Local.wHour, Local.wMinute,Local.wSecond, Local.wYear, Local.wMonth, Local.wDay,
            ERR001);
        }
        SetFileAttributes(tpmFile, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM);
        Sleep(10);        
        fprintf(fp_tmp, "\n===========================================================================================");
        fprintf(fp_tmp, "\n    %02d:%02d.%02d %d.%02d.%02d The continuation of the Protocol № %d. Начало работы: %02d:%02d.%02d %d.%02d.%02d",
        Local.wHour, Local.wMinute,Local.wSecond, Local.wYear, Local.wMonth, Local.wDay,  NUM,
        Start.wHour, Start.wMinute, Start.wSecond, Start.wYear, Start.wMonth, Start.wDay);
		//  новое имя файла и копируем
		sprintf(SaveFile, NameProtokolFileDirectory, DirName ,Local.wDay);
		// если каталог SaveFile не существует, создаем его
		if (CreateDirectory(SaveFile, NULL)) 
		{ 
            fprintf(fp_tmp, "\n  < %02d:%02d.%02d %d.%02d.%02d Created the directory %s.",
			Local.wHour, Local.wMinute,Local.wSecond, Local.wYear, Local.wMonth, Local.wDay,  SaveFile);
		}
		fclose(fp_tmp);
		fp_tmp=NULL;
		sprintf(SaveFile, NameProtokolFile, DirName ,Local.wDay, ProtokolFile);
		newdata = true;
    }
	if (newdata)
	{
	    Sleep(10);// пауза на случай выключения, чтобы хоть один файл остался
		CopyFile (tpmFile,SaveFile,false);
		SetFileAttributes(SaveFile, FILE_ATTRIBUTE_NORMAL);
	    LastSave = Local;
		newdata = false;
		if (fp_tmp==NULL)
		fp_tmp = fopen(tpmFile,"a+");
	}
}

void TProtocol::sWriteInfo(prtcl_char * dt, ...)
{
	if (dt==NULL) return;

	prtcl_char s_tmp[1000];
	if (strlen(dt)>sizeof(s_tmp))
	{
        WriteInfo((prtcl_char *)"The length of the string more than 1000 characters, the string will not be displayed.");
		return;
	}
	va_list args;
	va_start(args, dt);
	wvsprintf(s_tmp,dt,args);
	va_end(args);
	WriteInfo(s_tmp);

}

void TProtocol::WriteInfo(prtcl_char * dt)
{
	if (dt==NULL) return;
	// текущее время и дата
	SYSTEMTIME stUTC;    
	// преобразовать время модификации в локальное время.
    GetSystemTime(&stUTC);
    SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &Local);
	try
	{
		int k;
		prtcl_char s_tmp[1000];
		string dt_tmp = dt;

		for (k=0; k<100; k++)
		{
			if (stop) {k=100; break;};
			if (filebusy) Sleep(100); else break;
		}

		if (k==100) return;
		filebusy = true;

		{ 
			if (strlen(dt)<sizeof(s_tmp))
				sprintf(s_tmp, "\n    %02d:%02d.%02d - %s", Local.wHour, Local.wMinute,Local.wSecond, dt);
			else
                sprintf(s_tmp, "\n    %02d:%02d.%02d - %s", Local.wHour, Local.wMinute,Local.wSecond, "The length of the string more than 1000 characters, the string will not be displayed.");

			fprintf(fp_tmp, "%s", s_tmp);
			fflush(fp_tmp);
		}

		filebusy = false;
		newdata = true;

		//// текстовый вывод на экран
        CharToOemA(s_tmp, s_tmp);
		printf("%s", s_tmp);
	}
	catch (...)
	{
		return;
	}
}


void TProtocol::WriteInfoWithoutData(prtcl_char * dt)
{
	if (dt==NULL) return;

	try
	{
		int k;
		prtcl_char s_tmp[1000];
		string dt_tmp = dt;

		for (k=0; k<100; k++)
		{
			if (stop) {k=100; break;};
			if (filebusy) Sleep(100); else break;
		}

		if (k==100) return;
		filebusy = true;

		{ 
			if (strlen(dt)<sizeof(s_tmp))
				sprintf(s_tmp, "%s", dt);
			else
                sprintf(s_tmp, "%s", "The length of the string more than 1000 characters, the string will not be displayed.");

			fprintf(fp_tmp, "%s", s_tmp);
			fflush(fp_tmp);
		}

		filebusy = false;
		newdata = true;

		//// текстовый вывод на экран
		CharToOem(s_tmp, s_tmp);
		printf("%s", s_tmp);
	}
	catch (...)
	{
		return;
	}
}


DWORD WINAPI TProtocol::PrtThread(LPVOID PClass)
{
	try
	{
		TProtocol* Protocol = (TProtocol*)PClass;
		Protocol->PrtThreadWork = true;

		while(!Protocol->stop)
		{
			if (!Protocol->filebusy)
			{
				Protocol->filebusy = true;
				Protocol->ProtocolRoutine();
				Protocol->filebusy = false;
			}
			if (Protocol->stop)	break;
			Sleep(500);
		}
		Protocol->PrtThreadWork = false;			
		return 0;
	}
	catch (...)
	{
		TProtocol* Protocol = (TProtocol*)PClass;
		Protocol->PrtThreadWork = false;
		return 1;
	}
}


_Message::_Message()
{
	count = 0;
	p_wr = &msg[0];
	p_rd = &msg[0];
	Protocol = NULL;
}

void _Message::Write(prtcl_char* MSG, va_list args)
{
	strcpy(p_wr->Text,MSG);
	p_wr++;
	if(p_wr >= ((sizeof(msg)/sizeof(_msg))+&msg[0])) p_wr = &msg[0];
	if (Protocol!=NULL)
		Protocol->sWriteInfo(MSG, args);
}

void _Message::Read(prtcl_char* MSG)
{
	if (p_rd==p_wr)	return;
	strcpy(p_rd->Text,MSG);
	p_rd++;
	if(p_rd >= ((sizeof(msg)/sizeof(_msg))+&msg[0])) p_rd = &msg[0];
}
