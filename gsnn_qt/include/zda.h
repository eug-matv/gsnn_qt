﻿/*—ообщение ZDA -  врем¤, дата и временна¤ зона*/

#ifndef __ZDA_H__
#define __ZDA_H__

#include "time_tools.h"

struct ZDA_TIME_UTC
{
    int h;
    int m;
    double s;
};


struct ZDA_DATA
{
      int ns;		//“ип навигационной системы. 0 - GPS, 1 - √ЋќЌј—, 2 GPS+√лонас
      struct ZDA_TIME_UTC sHHMMSS_SS;	//¬рем¤ обсервации UTC
      int dd;		//ƒень UTC - от 1 до 31
      int bb;		//мес¤ц UTC от 1 до 12
      int yyyy;		//год UTC 
      int xx;		//„асы локальной временной зоны от 0 до +-13
      int mm;		//ћинуты локальной временной зоны
};

#ifdef __cplusplus
extern "C"
{
#endif	
int zdaRazborZDA( const char *str_data,
		    struct ZDA_DATA *zda_data);
#ifdef __cplusplus
}
#endif

#endif
