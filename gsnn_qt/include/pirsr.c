#include <stdio.h>
#include <string.h>
#include "pirsr.h"



/*���� �������, �� ���������� ������ ����� ���� ������� ��������� � ��� �������� <CR>  � <LF>.
0 - ���� ������ � ������ � ����� �� �������,
� -1 -- ���� �� ������� �� ����� size_data<, ��� �������� ����� �������
*/
int pirsrMakePIRSR(const struct PIRSR_DATA *pirsr_data, 
		   char *str_data, int size_str_data)
{
        int nc=0;
    int i;
    unsigned int u_mask=0;
    unsigned char ctr_summ=0;
    if(nc+2>size_str_data)return (-1);
    str_data[nc]='$';
    nc+=1;
//������� PIRSR 
    if(nc+6>size_str_data)return (-1);
    memcpy(str_data+nc,"PIRSR,", 6);
    nc+=6;
    
//������� ����� ��� GPS
    if(pirsr_data->pppppppp.isNoEmpty)
    {
	for(i=0;i<32;i++)
	{
	      if(pirsr_data->pppppppp.m[i])
	      {	
		    u_mask|=1<<i;
	      }
	}
	if(nc+10>size_str_data)return (-1);
	sprintf(str_data+nc,"%08X,",u_mask);
	nc+=9;
    }else{
	if(nc+1>size_str_data)return (-1);
	str_data[nc]=',';
	nc++;
    }
    
//������� ����� ��� �������
    u_mask=0;
    if(pirsr_data->xxxxxx.isNoEmpty)
    {
	for(i=0;i<24;i++)
	{
	      if(pirsr_data->xxxxxx.m[i])
	      {	
		    u_mask|=1<<i;
	      }
	}
	if(nc+8>size_str_data)return (-1);
	sprintf(str_data+nc,"%06X,",u_mask);
	nc+=7;
    }else{
	if(nc+1>size_str_data)return (-1);
	str_data[nc]=',';
	nc++;
    }
    
    if(nc+5>size_str_data)return (-1);
    str_data[nc]='*';
    
 //������ ��������� ����������� �����   
    for(i=1;i<nc;i++)
    {
	ctr_summ^=(unsigned char)(str_data[i]);
    }
    nc++;
    sprintf(str_data+nc, "%02X",ctr_summ);
    nc+=2;
    str_data[nc]=0x0D;
    str_data[nc+1]=0x0A;
    nc+=2;
    return nc;
}
