﻿
#include <stdlib.h>
#include <string.h>
#include "sync_proc_event.h"


#ifdef _WIN32

/*Windows реализация*/
struct TSPEvent* spOpenEvent(const char *name,
                                int isCreate,
                             int state) //Состояние. 0 - не сигнальное, 1 сигнальное
{
    struct TSPEvent *evnt;
    evnt=(struct TSPEvent*)malloc(sizeof(struct TSPEvent));
    if(!evnt)
    {
        return (struct TSPEvent*)0;
    }

    if(isCreate)
    {
		WCHAR temp_wchar[1024];
		MultiByteToWideChar(CP_UTF8,0,name,-1,temp_wchar,1023);
        evnt->handle=OpenEventW(EVENT_ALL_ACCESS, FALSE,   temp_wchar );
        if(evnt->handle)
        {
            CloseHandle(evnt->handle);
            free(evnt);
            return (struct TSPEvent*)0;
        }
        evnt->handle=CreateEventW(NULL,FALSE,(state!=0), temp_wchar);
        if(!(evnt->handle))
        {
            free(evnt);
            return (struct TSPEvent*)0;
        }
        evnt->isCreate=1;
    }else{
		WCHAR temp_wchar[1024];
		MultiByteToWideChar(CP_UTF8,0,name,-1,temp_wchar,1023);
        evnt->handle=OpenEventW(EVENT_ALL_ACCESS, FALSE, temp_wchar );
        if(!(evnt->handle))
        {
            free(evnt);
            return (struct TSPEvent*)0;
        }
        evnt->isCreate=0;
    }

    return evnt;

}

/*Windows реализация*/
int spCloseEvent(struct TSPEvent* evnt)
{
    if(!evnt)
    {
        return 0;
    }
    if(evnt->handle)
    {
        CloseHandle(evnt->handle);
        free(evnt);
        return 1;
    }
    free(evnt);
    return 0;
}





/*Windows реализация
 * Функция возвращает 1, если успешно. 0 - если время timeout_ms вышло, -1 -- прочие ошибки*/
int spWaitForEvent(struct TSPEvent* evnt, long timeout_ms)
{

    DWORD waitResult;

    if(!evnt||!(evnt->handle))
    {
        return (-1);
    }

    waitResult=WaitForSingleObject(evnt->handle,timeout_ms);
    switch(waitResult)
    {
        case WAIT_OBJECT_0:
            return 1;
        break;

        case WAIT_TIMEOUT:
            return 0;
        break;
    };
    return (-1);
}



int spSetEvent(struct TSPEvent* evnt)
{
	
	if(evnt&&evnt->handle)
	{
		return (int)SetEvent(evnt->handle);
	}
	return 0;
}

int spResetEvent(struct TSPEvent* evnt)
{
	if(evnt&&evnt->handle)
	{
		return (int)ResetEvent(evnt->handle);
	}
	return 0;

}

#else

    #include <time.h>
    #include <errno.h>
/*Unix реализация открытия или создания события*/
struct TSPEvent* spOpenEvent(const char *name,
                             int isCreate,
                             int state)
{
    struct TSPEvent *evnt;
    evnt=(struct TSPEvent*)malloc(sizeof(struct TSPEvent));
    if(!evnt)
    {
        return (struct TSPEvent*)0;
    }
    if(state!=0)state=1;
    evnt->name=(char*)malloc(sizeof(char)*(strlen(name)+2));
    if(!(evnt->name))
    {
                free(evnt);
                return (struct TSPEvent*)0;
    }
    evnt->name[0]='/';
    strcpy(evnt->name+1,name);

    if(isCreate)
    {
        evnt->sem=sem_open(evnt->name,O_CREAT|O_EXCL,S_IRUSR|S_IWUSR,state);
        if(evnt->sem==SEM_FAILED)
        {
            free(evnt->name);
            free(evnt);
            return (struct TSPEvent*)0;
        }
        evnt->isCreate=1;
    }else{
        evnt->sem=sem_open(evnt->name,0);
        if(evnt->sem==SEM_FAILED)
        {
            free(evnt);
            return (struct TSPEvent*)0;
        }
        evnt->isCreate=0;
    }
    return evnt;
}

/*Unix реализация закрытия события*/
int spCloseEvent(struct TSPEvent* evnt)
{
    if(!evnt)
    {
        return 0;
    }
    if(evnt->sem)
    {
        sem_close(evnt->sem);
        if(evnt->isCreate)
        {
            if(evnt->name)
            {
                sem_unlink(evnt->name);
                free(evnt->name);
            }
        }else{
            if(evnt->name)
            {
                free(evnt->name);
            }
        }
        free(evnt);
        return 1;

    }
    free(evnt);
    return 0;
}



int spSetEvent(struct TSPEvent* evnt)
{
    int r,s;
    if(!evnt)return(-10);
    while((r=sem_getvalue(evnt->sem, &s))==0&&s<=0)
    {
        sem_post(evnt->sem);
    }
    if(r!=0)
    {
        return (-1);
    }
    return 1;
}

/*Пока не знаю, насчет этой процедуры*/
int spResetEvent(struct TSPEvent* evnt)
{
    if(!evnt)return (-10);
    while(sem_trywait(evnt->sem)==0);
    if(errno==EAGAIN)return 1;
    return (-1);
}



/*Unix реализация. Входа в защищенную часть
 *
 * Функция возвращает 1, если успешно. 0 - если время timeout_ms вышло, -1 -- прочие ошибки*/
int spWaitForEvent(struct TSPEvent* evnt, long timeout_ms)
{
    struct timespec ts;
    int val_sem,s,r;
    if(!evnt||!(evnt->sem))
    {
        return (-1);
    }

    if(clock_gettime(CLOCK_REALTIME, &ts)<0)
    {
        return (-1);
    }

//Укажем новое время
    ts.tv_sec+=timeout_ms/1000;
    ts.tv_nsec+=(1000000*(timeout_ms-(timeout_ms/1000)*1000));

    if(ts.tv_nsec>=1000000000)
    {
        ts.tv_sec+=ts.tv_nsec/1000000000;
        ts.tv_nsec%=1000000000;
    }



    r=0;
    while(((s = sem_timedwait(evnt->sem, &ts)) == -1 && errno == EINTR)||
        (s==0&& (r = sem_getvalue(evnt->sem, &val_sem))==0&&val_sem>0))
    {

            continue;
    }

    if(s<0)
    {
        if(errno == ETIMEDOUT)
        {
            return 0;
        }
        return (-1);
    }
    if(r<0)
    {
        return (-1);
    }
    return 1;
}











#endif





