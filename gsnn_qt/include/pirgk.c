﻿#include <stdio.h>
#include "pirgk.h"
#include "iec61161_1tools.h"


int pirgkRazborPIRGK( const char *str_data,
		    struct PIRGK_DATA *pirgk_data)
{
   int i=1,k=0,j=0;
    int iRet;
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
    
    if(str_data[0]!='$')return 0;
    while(str_data[i])
    {
	summ^=(unsigned char)(str_data[i]);
	if(str_data[i]==','||str_data[i]=='*')
	{
	    if(j==0)
	    {
		if(k!=5)
		{
		      return 0;
		}  
		if(str_data[i-k]!='P'||str_data[i-k+1]!='I'||str_data[i-k+2]!='R'||str_data[i-k+3]!='G'||str_data[i-k+3]!='K')
		{
		      return 0;
		}  
	    }
	    else if(j==1)
	    { 
	      //Считаем время UTC
		if(k!=0)
		{  
		  int hh,mm;
		  double ssss;
		  iRet=iecToolsGetTime(str_data+(i-k),&hh,&mm,&ssss);
		  if(iRet<=0)return 0;
		  pirgk_data->sHHMMSS_SS.h=hh;
		  pirgk_data->sHHMMSS_SS.m=mm;
		  pirgk_data->sHHMMSS_SS.s=ssss;
		  
		}else{
		    return 0;
		}  
	    }
	    else if(j==2)
	    {
	      //Считаем индикатор качества GNSS
		if(k!=0)
		{  
		   iRet=sscanf(str_data+(i-k),"%d",&(pirgk_data->A));
		   if(iRet!=1)return 0;
		   
		}else{
		    return 0;
		}  
	    }
	    else if(j==3)
	    {
	      //Считаем координату X 
		  
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%lf",&(pirgk_data->x_x));
		      if(iRet!=1)return 0;
		  }else{
		      return 0;
		  }  
	    }else if(j==4)
	    {
	    //Считаем модифицированную Y
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%lf",&(pirgk_data->y_y));
		      if(iRet!=1)return 0;
		  }else{
		      return 0;
		  }	  
	    }else if(j==5)
	    {
	    //Считаем высоту
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%lf",&(pirgk_data->z_z));
		      if(iRet!=1)return 0;
		  }else{
		      return 0;
		  }	  
	    }else if(j==6)
	    {
	    //Считаем скорость
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%lf",&(pirgk_data->v_v));
		      if(iRet!=1)return 0;
		  }else{
		      return 0;
		  }	  
	    }else if(j==7)
	    {
	    //Считаем курс
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%lf",&(pirgk_data->k_k));
		      if(iRet!=1)return 0;
		  }else{
		      return 0;
		  }	  
	    }else if(j==8)
	    {
	//Считаем дату      
		if(k>0)
		{
		    iRet=iecToolsGetDate(str_data+(i-k),&(pirgk_data->sDDMMYY.dd),&(pirgk_data->sDDMMYY.mm),&(pirgk_data->sDDMMYY.yy));
		    if(iRet<=0)return 0;
		}else{
		    return 0;
		}  
	    }else if(j==9)
	    {
	//Считаем геометрический фактор ухудшения точности в плане (HDOP)
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%lf",&(pirgk_data->f_f));
		      if(iRet!=1)return 0;
		  }else{
		      return 0;
		  }	
	    }else if(j==10)
	    {
	//Считаем геометрический фактор ухудшения точности по высоте (VDOP)
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%lf",&(pirgk_data->g_g));
		      if(iRet!=1)return 0;
		  }else{
		      return 0;
		  }	
	    }else if(j==11)
	    {
	//Считаем количество НКА	
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%d", &(pirgk_data->n));
		      if(iRet!=1)return 0;
		  }else{
		      return 0;
		  }     
	    }
	    j++;
	    k=0;
	}else{  
	    k++;
	}
	
	if(str_data[i]=='*')
	{
	    iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
	    if(iRet!=1)return 0;
	    if(((unsigned int)summ)!=recv_summ)
	    {
		return 0;
	    }
	    break;
	}  
	
	summ^=(unsigned char)(str_data[i]);
	i++;
    };  
    return 1;  
}
