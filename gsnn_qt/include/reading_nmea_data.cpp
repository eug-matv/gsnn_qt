﻿#include <string.h>
#include "reading_nmea_data.h"

#define RND_GET_DATA_FROM_SYMB10(x) ((x)-0x30)
#define RND_GET_DATA_FROM_SYMB_h(x) (((x)>='a'&&(x)<='f')?((x)-'a'+10):RND_GET_DATA_FROM_SYMB10(x)) 
#define RND_GET_DATA_FROM_SYMB(x) (((x)>='A'&&(x)<='F')?((x)-'A'+10):RND_GET_DATA_FROM_SYMB_h(x))

int TFIFO_NMEA_DATA::addByte(unsigned char b)
{
    if(szNewData==MAX_NMEA_SIZE)
    {
                 szNewData=0;
                 iState=0;               
                 return 0;                                               
    }
    if(iState==0)
    {
                 if(b==(unsigned char)'$')
                 {
                      ucNewData[0]=b;
                      szNewData=1;
                      iState=1;
                      ucCtrlSum=0;
                      return 3;   //Означает, что возможное начало нового пакета            
                 }
                 return 0;
    }
     
    if(iState==1)
    {
                 ucNewData[szNewData]=b;            
                 szNewData++;
                 if(b==(unsigned char)('*'))
                 {
                      iState=2;
                      return 1;
                 }
                 if(b==(unsigned char)'$')
                 {
                      iState=1;
                      szNewData=1;
                      ucNewData[0]='$';
                      return 1;           
                 }
                 ucNewData[szNewData]=b;
				 return 1;
    }
    
    
    if(iState==2||iState==3)
    {            
                 if((b>=(unsigned char)'0'&&b<=(unsigned char)'9')||
                    (b>=(unsigned char)'a'&&b<=(unsigned char)'f')||
                    (b>=(unsigned char)'A'&&b<=(unsigned char)'F'))
                 {
                        ucNewData[szNewData]=b;
                        szNewData++;         
                        if(iState==3)
                        {
                              unsigned char sm;
                              sm=16*RND_GET_DATA_FROM_SYMB(ucNewData[szNewData-2])+
                                 RND_GET_DATA_FROM_SYMB(ucNewData[szNewData-1]);                                                                                        
                           //   if(sm==ucCtrlSum)
                              {
                                  iState=4;
                                  return 1;             
                              }    
                              szNewData=0;
                              iState=0;
                              return 0;               
                        }
                        iState=3;
                        return 1;
                 }
                 szNewData=0;
                 iState=0;
                 return 0;                                     
    }
    if(iState==4)
    {
         if(b==0x0D||b==0x0A)
         {
            unsigned char *ucTemp;
            ucTemp=ucNewData;
            szReceivedData=szNewData+2;      
            ucNewData=ucReceivedData;
            ucReceivedData=ucTemp;
			ucReceivedData[szReceivedData-2]=0x0D;
            ucReceivedData[szReceivedData-1]=0x0A;
			szNewData=0;
            iState=0;
            return 2;
         }        
         szNewData=0;
         iState=0;
         return 0;                                             
    }
    szNewData=0;
    iState=0;
    return 0;                                                 
}


int TFIFO_NMEA_DATA::
              getReceievedData(
                   int &navig_sys,  //Тип навигационной системы 1 - GPS, 2 - ГЛОНАСС, 3 - ГЛОНАСС+GPS,0 - не надо
                   int &mes_type,   //Типы сообщений. Смотреть определенные выще макросы 	
                   char *data      //Данные, включая заголовочные символы и последние два символа
			  )
{
     if(szReceivedData>=6)
     {
           memcpy(data,ucReceivedData,szReceivedData);
           if(data[1]=='G')
           {
               if(data[2]=='P')navig_sys=1;
               else if(data[2]=='L')navig_sys=2;
               else if(data[2]=='N')navig_sys=3;
               else navig_sys=0;            
           }else{
               navig_sys=0;  
           }
     }     
     return 1;
}			  

