﻿#ifndef __IEC_LAT_LONG_H__
#define __IEC_LAT_LONG_H__

struct TIecLatLong
{
	 int gr;		//градусы 
	 double m;		//минуты
     char a;		//север/юг (N/S)  или запад/восток (W/E)
};
	
	
#ifdef __cplusplus
extern "C" 
{
#endif	

int iecLLMakeStringFromLatOrLong(
                    void *d,    //Данные типа TIecLatLong
					char *str,	//Строка
					int sz_str	//Размер строки
                       );


#ifdef __cplusplus
}
#endif

	
	
#endif