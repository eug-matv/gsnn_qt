﻿/*Автор: Матвеенко Е.А.
Реализация вторичного потока считывания данных с последовательного порта протоколов
NMEA и MNP-binary.

*/

#ifndef READTHREAD_H
#define READTHREAD_H



//include\time_tools.h
#include "time_tools.h"

//include\reading_nmea_data.h
#include "reading_nmea_data.h"

//include\reading_mnp_data.h
#include "reading_mnp_data.h"


//include\tools_win_unix.h
#include "tools_win_unix.h"


//include\glob_data.h
#include "glob_data.h"

#include <QMutex>
#include <QThread>
#include <QTimer>

#include <time.h>

#ifdef linux
#include <sys/times.h>
#else
#include <windows.h>
#endif

/*! Класс потока чтения данных в потоке с последовательного порта/или файла
    для протоколов NMEA и MNP-binary, используемых в навигационных приемниках */



class readThread : public QThread
{
    Q_OBJECT
public:

    readThread();
    ~readThread();

    void destroy();     // Убить поток

    void setOptions(
        int protocol,           // Тип протокола 0 - nmea (iec), 1 - mnp - бинарный
        const char *comport,    // Символьное наиемнование последовательного порта
        int baudrate,           // Скорость чтения в битах
        int stop_bits,          // Число стоповых бит. 0  - 1, 1  - 1.5, 2 - 2
        int period_test_ret_sec, //Период в течении которого допустимо отсутствие данных на вх. последовательном порте
        int _is_remote1,            //Посылаь данные на первый удаленный узел
        char *_cPortName_remote1,    //Имя последовательного порта, куда данные будут пересылаться как удаленные
                                            //если значение нулевое или none - то не посылаются данные
        int _iBaudeRate_remote1,  // скорость обмена по порту, бод для обмена с удаленным узлом
        int _iStopBits_remote1,   // 0 - один стоп бит

        int _is_remote2,            //Посылаь данные на второй удаленный узел
        char * _cPortName_remote2,    //Имя последовательного порта, куда данные будут пересылаться как удаленные
                                            //если значение нулевое или none - то не посылаются данные
        int _iBaudeRate_remote2,  // скорость обмена по порту, бод для обмена с удаленным узлом
        int _iStopBits_remote2   // 0 - один стоп бит
                   );

    int start();    // Запустить поток
    int stop();     // Остнановить поток


    bool was_data;      //  были получены какие-то данные при обращении к порту
                        // в течении period_test_ret секунд

    bool was_package;   //  был получен пакет с данными в течении
                        //period_test_ret секунд

    int error_recive_data;      //Ошибка получения данных
    int error_recive_package;


    int error_sending_data_remote1;

    int error_sending_data_remote2;


//Получение рекомендованного минимума данных
             int getRMC_DATA(int ns, struct RMC_DATA &rmc_data);
             int getGGA_DATA(int ns, struct GGA_DATA &gga_data);



/*Метод добавлденный 26.05.2015*/
    int sendDataToRemote();


signals:

    void noDataEvent();             // Не было получено данных в течении 5 секунд

    void againDataEvent();          // Данные появились снова

    void noPackageEvent();          //не было пакетов с данными

    void againPackageEvent();       //Снова данные с пакетами

    void receivedData(int);            //распознан и получен пакет данных
                                       //В качестве параметра передается.
                                       //Если тип пакета IEC, то значение
                                       //из файла iec61161_1tools.h
                                       //Если кадр 3000, то число 3000


    void errorReopenPortEvent();    // Попытка заново открыть порт привела к ошибке открытия

    void noPackageEventForOther();  //Не было пакетов - вызывает не один раз, а пока из нет

    void noDataEventForOther();     //Не было байт

    void errorOpenComPortEvent(int);        //Ошибка открытия входного  порта, в качестве параметров
                                        //передается ошибка: -1 - ошибка открытия  порта
                                        //-2 - ошибка установки свойств порта

    void errorOpenComPortForRemote1Event(int);        //Ошибка открытия 1 выходного  порта, в качестве параметров
                                        //передается ошибка: -1 - ошибка открытия  порта
                                        //-2 - ошибка установки свойств порта

    void errorOpenComPortForRemote2Event(int);        //Ошибка открытия 2 выходного  порта, в качестве параметров
                                        //передается ошибка: -1 - ошибка открытия  порта
                                        //-2 - ошибка установки свойств порта

    void criticalError(int);

protected:

    void run();             // Функция выполняемая во вторичном потоке


    // контроль наличия данных по времени
    int testTimeOut();

//Проверка открытия порта, и если  он  не открыт, то попытка его открыть
//Параметр type указывает какой порт надо открыть 0 -основной входной, 1 - порт длл связи
//с remote 1, 2 - порт для связи с Remote2.
    int testAndOpenComPort(int type);

//Закрытие и повторное открытие порта
    int closeAndOpenComPort(int type);

private:

    TFIFO_MNP_DATA   fifo_mnp_data;
    TFIFO_NMEA_DATA  fifo_nmea_data;
    TDATA_OF_ALL_PACKAGES glob_data;	//Данные всех пакетов

    int type_of_protocol;       // Тип протокола 0 - nmea (iec), 1 - mnp - бинарный
    char portname[200];         // Имя порта
    int  brate;                 // Скорость обмена по порту
    int stopbits;               // '0' - 1 стоповый бит
    TWUFileDescr *twuFile;      // Дескрипто открываемого устройства
    int isWork;                 // Работает ли поток
    TTDateTime dt;              // Структура времени

    int period_test_ret;    // Период проверки отсутствия данных или информации на входе



    QMutex readMutex;
    QMutex timeoutMutex;


//Дополнительно от матвеенко е.а.
//параметры поссылки данных по портам
    int isSendDataToRemote1;        //Посылать данные на первый удаленный узел
    char cPortName_remote1[200];    //Имя последовательного порта, куда данные будут пересылаться как удаленные
                                    //если значение нулевое или "none" - то не посылаются данные
    int iBaudeRate_remote1;  // скорость обмена по порту, бод для обмена с удаленным узлом
    int iStopBits_remote1;   // 0 - один стоп бит
    TWUFileDescr *twuFile_remote1;      // Дескрипто открываемого устройства


    int isSendDataToRemote2;        //Посылать данные на первый удаленный узел
    char cPortName_remote2[200];    //Имя последовательного порта, куда данные будут пересылаться как удаленные
                                    //если значение нулевое или "none" - то не посылаются данные
    int iBaudeRate_remote2;  // скорость обмена по порту, бод для обмена с удаленным узлом
    int iStopBits_remote2;   // 0 - один стоп бит
    TWUFileDescr *twuFile_remote2;      // Дескриптор открываемого устройства



//Время последней посылки сигнала  errorOpenComPortEvent
    long lTimeOfLastSendEOCPE;


//Время последней посылки сигнала  errorOpenComPortForRemote1Event
    long lTimeOfLastSendEOCPFE1E;

//Время последней посылки сигнала  errorOpenComPortForRemote2Event
    long lTimeOfLastSendEOCPFE2E;



//Дополнительная процедура для проверки отправлять на удаленные узлы rmc или gga.
    int whyOtpravlyat_sendDataToRemote;

};

#endif // READTHREAD_H
