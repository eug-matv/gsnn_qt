﻿#ifndef 	__READING_MNP_DATA_H__
#define     __READING_MNP_DATA_H__

//#include "mnp-kadr.h"
#include "kadr3000cls.h"


class TFIFO_MNP_DATA
{
  
    unsigned char ucData[2][MNP_KADR_MAXSIZE*2+12];		//Два массива данных для получения кадров
    
    unsigned char *ucReceivedData;				//Полученная информация включая заголовок
    int szReceivedData;						//Размер полученной информации в байтах
    
    
    unsigned char *ucNewData;					//Получаемая информация на данный момент вкл. заголовок 
    int szNewData;						//Размер получаемой информации в байтах
    int szWaittingDataWords;					//Ожидаемый ращмеп данных в словах	
    int iGotDataWords;
    int iState;
    
public:
    TFIFO_MNP_DATA()
    {
	ucReceivedData=ucData[0];
	ucNewData=ucData[1];
	szReceivedData=szNewData=0;
	iState=0;
    };
    
    
//Получение 1 байта данных. Возвращает 1, если байт добавлен. 0 - если байт не добавлен и пакет сброшен 
//2 - если байт добавлен и получен пакет с данными --- который правильный по размеру и его можно читать

    int addByte(unsigned char b);

    int getReceievedData(int &type_of_kadr,  //Тип кадра
			 int &size_of_data,  //Размер данных в словах (по 2 байта)
			 unsigned short &mask2200, //Маска для кадра 2200,
			 unsigned short *data	  //Данные, которые были получены	
			  );
    
    unsigned char *getReceivedDataUnsafe()
    {       
            if(szReceivedData>0)return ucReceivedData; 
            return (unsigned char*)0;
    };
    int getSizeOfReceivedDataUnsafe()
    {
            if(szReceivedData>0)return szReceivedData; 
            return 0;                                  
    };
			  

};



#endif
