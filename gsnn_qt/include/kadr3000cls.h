﻿#ifndef __KADR3000_CLASS_H__
#define __KADR3000_CLASS_H__

#include <math.h>
#include "time_tools.h"
#include "mnp_kadr.h"
#include "rmc.h"
#include "gga.h"
#ifdef _MSC_VER
#define M_PI 3.14159265358979323846
#endif


class TKadr3000cls:public TMNPKadr
{

  public:
    TKadr3000cls():TMNPKadr()
    {
    };

    int setData(const void *d, const struct TTDateTime &dt1)
    {
		uint16_t *ui16_d;
		ui16_d=(uint16_t*)d;
		if(ui16_d[1]!=3000)return (-1);
		
	    return TMNPKadr::setData(ui16_d+5,80,dt1);
    };
    
//Широта в радианах (0 4) (в 2-х байтных словах)
    double getLatitude()
    {
	  return getDFData(0);
    };  
    
//Долгота в радианах   (4 4) 
    double getLongitude()
    {
	  return getDFData(4);
    };
    
//Высота в метрах (8 4)
    double getAltitude()
    {
	  return getDFData(8);
    };
    
//Модуль наземной скорости в метрах (12 4)
    double getGroundSpeed()
    {
	  return getDFData(12);
    };  
    
//Азимут в рад (16 4)
    double getAzimuth()
    {
	  return getDFData(16);
    };  
    
//Скорость подъема (20 4)
    double getRisingSpeed()
    {
	  return getDFData(20);
    };
    
//Маска каналы в решении (24 2)
    int getSolvingChanels(int n)
    {	
	  return (int)(getBits(24+n/16,n%16,1));  
    };
    
//Число каналов в решении не равных 0
    int getSolvingChanelsCount()
    {
        int n=0;
        int i;
        for(i=0;i<24;i++)
        {
            if(getSolvingChanels(i))
            {
                n++;
            }
        }
        return n;
    };


//Маска: наличие диф. поправок (26 2)
    int getDiffCorrection(int n)
    {
	  return (int)(getBits(26+n/16,n%16,1));
    };  
 
//Получение года (28 2)
    int getYear()
    {
	  return (int)getDIData(28);
    };
    
//Получение месяца (30 2)
    int getMonth()
    {
	  return (int)getDIData(30);
    };
    
//Получение дня (32 2)
    int getDay()
    {
	  return (int)getDIData(32);  
    };  
    
//Получение часа (34 2)
    int getHour()
    {
	  return (int)getDIData(34);
    };  
      
    
//Получение минут (36 2)
    int getMinute()
    {
	  return (int)getDIData(36);
    };
    
 //Полечение секунды(38 2)
    int getSecond()
    {
	  return (int)getDIData(38);
    };  
    
//Внутренее время приемника (40 2) в 0.5 мс
    unsigned int getInnerTime()
    {
	  return (unsigned int)getUDIData(40);
    };  

//Отсройка генератора в Гц (42 2)
    float getTuningOutOfGenetor()
    {
	  return getFData(42);
    };
    
//GDOP  (44 2)
    float getGDOP()
    {
	  return getFData(44);
    };
    
    
//PDOP (46 2)
    float getPDOP()
    {
	  return getFData(46);
    };
    
//Фильтрационная широта в рад (48 4)
    double getFiltLatitude()
    {
	  return getDFData(48);
    };
    
//Фильтрационная долгота в рад (52 4)
    double getFiltLongitude()
    {
	  return getDFData(52);
    };

//Фильтрованная высота в метрах (56 4)
    double getFiltAltitude()
    {
	  return getDFData(56);
    };  
    

//Филтрованная скорость (модуль) в м/с (60 4)
    double getFiltSpeed()
    {
	  return getDFData(60);
    };  
    
 //Филтрованная скорость (азимут) в рад (64 4)
    double getFiltSpeedAzimuth()
    {
	  return getDFData(64);
    };
    
 //Филтрованная скорость (подъем) в м/с (68 4)
    double getFiltRisingSpeed()
    {
	  return getDFData(68);
    };
    
 //Далее идут 2 слова флагов
 //Двумерное решение (72.0 1бит)
    int get2dimSolving()
    {
	return (int)(getBits(72,0,1));    
    };
    
//Фиксированная невязка времени между GPS и ГЛОНАСС    (72.1 1бит
    int getFixDiscTimeGPS_GLONASS()
    {
	return (int)(getBits(72,1,1));    	
    };
    
//Выбранный эллипсоид   0 - WGS-84, 1 - ПЗ-90 2 - эллипсоид Красковского 
//			3 - эллипсоид, определяемый пользователем    (72.2-72,3 2бита)
    int getChosenEllipsoid()
    {
	return (int)(getBits(72,2,2));
    };
    
//Годность решения (72.4 1бит)
    int getSolvingValidity()
    {
	return (int)(getBits(72,4,1));
    };
    
//Годность времени (72.5 1бит)
    int getTimeValidity()
    {
	return (int)(getBits(72,5,1));
    };

//Резерв 72.6 72.9 --- пока не трогаем
    
    
//Выбранная система координат;0 - WGS-84     1 - ПЗ 90     2 - СК-42	3 - СК-95
//				4 - определяется пользователем -- остальное резерв  (72.10-72.12 3 бита)
    int getChosenCoordinateSys()
    {
	return (int)(getBits(72,10,3));      
    };
    
//Дифферинциальный режим (72.13, 1 бит)
    int getDiffRegime()
    {
	return (int)(getBits(72,13,1));
    };
    
//72.14 - 73.11 -- резерв

//Версия флагов 73.12 - 73.15 4бита
    int getFlagsVersion()
    {
	return (int)(getBits(73,12,4));
    };
    
//Маска Наличие эмфермед    (74 2)
    int getEphemerisesAvailible(int n)
    {
	  return (int)(getBits(74+n/16,n%16,1));
    };
    
//Температура в градусах по F (76 2)
    float getTemperature()
    {
	  return getFData(76);
    };
    
//Маска - отбракованные измерения (1 - 	измерения отбракованные алгоритмом RAIM) (78 2)
    int getDropoutMeasuring(int n)
    {
	  return (int)(getBits(78+n/16,n%16,1));
    };
    
    
//Итого размер равен 80
	
	
//Специально для совместимости. Получение минимального рекомендованного минимумума навигационных данных
    int getRMC_DATA(struct RMC_DATA &rmc)
	{
		int i;
		double s_d;  //Широта или долгота
		int i_s_d;
//Заполним ns
		rmc.ns=3;
		
//Получение даты		
		rmc.sHHMMSS_SS.h=getHour();
		rmc.sHHMMSS_SS.m=getMinute();
		rmc.sHHMMSS_SS.s=getSecond();

//Получение дифференциального или автономного режима

		if(getSolvingValidity())
		{
			if(getDiffRegime())
			{
				rmc.A='D';
			}else{
				rmc.A='A';
			}
		}else{
			rmc.A='V';
		}
//Получение широты
		s_d=getLatitude()/M_PI*180.0;
		if(s_d<0)
		{
			rmc.sBBBB_BBBB_a.a='S';
			s_d=-s_d;
		}else{
			rmc.sBBBB_BBBB_a.a='N';
		}
		i_s_d=(int)s_d;  //Отбрасываем дробную часть
		rmc.sBBBB_BBBB_a.gr=i_s_d;
		rmc.sBBBB_BBBB_a.m=(s_d-i_s_d)*60.0;

//Получение долготы
		s_d=getLongitude()/M_PI*180.0;
		if(s_d<0)
		{
			rmc.sLLLL_LLLL_a.a='W';
			s_d=-s_d;
		}else{
			rmc.sLLLL_LLLL_a.a='E';
		}
		i_s_d=(int)s_d;  //Отбрасываем дробную часть
		rmc.sLLLL_LLLL_a.gr=i_s_d;
        rmc.sLLLL_LLLL_a.m=(s_d-i_s_d)*60.0;

//Получение наземной скорости в узлах
		rmc.v_v=getGroundSpeed()/0.514;

//Азимут наверное и есть наземный курс
		rmc.z_z=getAzimuth()/M_PI*180.0;

//Дата. Ввести дату
		rmc.sDDMMYY.dd=getDay();
		rmc.sDDMMYY.mm=getMonth();
		rmc.sDDMMYY.yy=getYear();

//Магнитное склонение в градусах
		rmc.x_x_a.x_x=-12000;
		rmc.x_x_a.a=0;



//Режим местоопределения
		if(rmc.A=='V')
		{
			rmc.b='N';
		}else{
			rmc.b=rmc.A;
		}
                unsigned char *uc1,*uc2;
                int ii;
                uc2=(unsigned char*)(&dt);
		for(i=0;i<10;i++)
		{

                        uc1=(unsigned char*)(rmc.dt+i);
                        for(ii=0;ii<(int)(sizeof(struct TTDateTime));ii++)
                        {
                                uc1[ii]=uc2[ii];
                        }
		}
		//memset(rmc.dt+8,0,sizeof(struct TTDateTime));
                uc1=(unsigned char*)(rmc.dt+8);
                for(ii=0;ii<(int)(sizeof(struct TTDateTime));ii++)
                {
                        uc1[ii]=0;
                }
		return 1;
	};
	
	
    //Специально для совместимости. Получение минимального рекомендованного минимумума навигационных данных
    int getGGA_DATA(struct GGA_DATA &gga)
    {
        int i,ii;

        unsigned char *uc1,*uc2;
        uc2=(unsigned char*)(&dt);

        gga.ns=3;       //Получим типа данных

        if(getSolvingValidity())
        {
            if(getDiffRegime())
            {
                gga.b=2;
            }else{
                gga.b=1;
            }
        }else{
            gga.b=0;
        }

        gga.cc=getSolvingChanelsCount();

        for(i=0;i<11;i++)
        {

                        uc1=(unsigned char*)(gga.dt+i);
                if(i==0||i==4||i==5)
                {
                        for(ii=0;ii<(int)(sizeof(struct TTDateTime));ii++)
                        {
                                uc1[ii]=uc2[ii];
                        }
                }else{
                    for(ii=0;ii<(int)(sizeof(struct TTDateTime));ii++)
                    {
                            uc1[ii]=0;
                    }
                }
        }

        return 1;
    };


	
	
    
};



#endif
