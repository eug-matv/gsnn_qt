﻿#ifndef 	__PIREA_H__
#define __PIREA_H__

#include "time_tools.h"

struct PIREA_DATA
{
  int x;	//–езультат теста, 0 - означает, что все тесты успешно пройдены
};

#ifdef __cplusplus
extern "C"
{
#endif	
int pireaRazborPIREA( const char *str_data,
		    struct PIREA_DATA *pirea_data);
#ifdef __cplusplus
}
#endif


#endif
