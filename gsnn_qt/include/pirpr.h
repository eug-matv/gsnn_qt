﻿/*PIRPR - запрос на изменение установок порта*/
#ifndef 		__PIRPR_H__
#define 	__PIRPR_H__


struct PIRPR_DATA
{
    union
    {
    	int x;			//Номер порта установки которого надо заменить
     	int port_number;	//0 - UART0, 1 - UART1, -1 - текущий порт - в тексте будет пустое поле
    };	    
    
    union
    {
    	int b;			//Скорость обмена от 1200 до 115200, если значение <=0 -- то оставить текущее значение
     	int speed;		//
    };
    
    
    union
    {
    	int n;			//Тип протокола 0 - выключить обмен, 1 -  MNP-binary 2 - R-binary
     	int type_of_protocol;	//3 - RTCM, 4 - IEC 61162-1 (NMEA 0183)
    };
    
    
    union
    {    
    	struct 
     	{
	       int GxGGA;
	       int GxGSA;
	       int GxGSV;
	       int GxRMC;
	       int GxVTG;
	       int GxGLL;
	       int GxZDA;
	       int rezerv1;
	       int PIREA;
	       int PIRFV;
	       int PIRGK;
	       int PIRRA;
	       int rezerv2[4];
        }resolving;
	    int mmmm[16];
    };
	
};

#ifdef __cplusplus
extern "C"
{
#endif	
int pirprMakePIRPR(const struct PIRPR_DATA *pirpr_data, 
		   char *str_data, int size_str_data);
#ifdef __cplusplus
}
#endif



#endif	
