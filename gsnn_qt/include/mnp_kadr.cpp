﻿#include <string.h>
#include "mnp_kadr.h"


int TMNPKadr::setData(const void *d, int size_in_words, const struct TTDateTime &dt1)
{
    if(size_in_words<=0||!d)return 0;
	data_size=size_in_words;
    memcpy(data,d,size_in_words*sizeof(uint16_t));
	memcpy(&dt,&dt1,sizeof(struct TTDateTime));
    return 1;
}
