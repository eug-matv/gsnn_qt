﻿#include <stdio.h>
#include "pirfv.h"

int pirfvRazborPIRFV( const char *str_data,
		    struct PIRFV_DATA *pirfv_data)
{
    int i=1,k=0,j=0;
    int iRet;
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
    
    if(str_data[0]!='$')return 0;
    while(str_data[i])
    {
	summ^=(unsigned char)(str_data[i]);
	if(str_data[i]==','||str_data[i]=='*')
	{
	    if(j==0)
	    {
		if(k!=5)
		{
		      return 0;
		}  
		if(str_data[i-k]!='P'||str_data[i-k+1]!='I'||str_data[i-k+2]!='R'||str_data[i-k+3]!='F'||str_data[i-k+4]!='V')
		{
		      return 0;
		}  
	    }
	    else if(j==1)
	    { 
		int ii;
		int s=0;
	      //Считаем результат теста
		if(k!=0)
		{  
		      pirfv_data->xx_=0;
		      pirfv_data->_xx=0;
		      
		      for(ii=i-k;ii<i;ii++)
		      {
			  if(str_data[ii]=='.')
			  {
				s=1;
				continue;
			  }
			  if(str_data[ii]>='0'&&str_data[ii]<='9')
			  {
				if(s==1)
				{
				      pirfv_data->_xx=10*pirfv_data->_xx+(int)(str_data[ii]-0x30);
				}else{
				      pirfv_data->xx_=10*pirfv_data->xx_+(int)(str_data[ii]-0x30);
				}  
			  }  
			  
		      }
		}else{
		    return 0;
		}  
	    }
	}else{  
	    k++;
	}
	
	if(str_data[i]=='*')
	{
	    iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
	    if(iRet!=1)return 0;
	    if(((unsigned int)summ)!=recv_summ)
	    {
		return 0;
	    }
	    break;
	}  
	
	summ^=(unsigned char)(str_data[i]);
	i++;
    };  
    return 1;  
}
