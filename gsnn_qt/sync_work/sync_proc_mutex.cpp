﻿#include <stdlib.h>
#include <string.h>
#include "sync_proc_mutex.h"


#ifdef _WIN32

/*Windows реализация*/
struct TSPMutex* spOpenMutex(const char *name, int isCreate)
{
	struct TSPMutex *psm;
	psm=(struct TSPMutex*)malloc(sizeof(struct TSPMutex));
	if(!psm)
	{
		return (struct TSPMutex*)0;
	}	
	
	if(isCreate)
	{	
		WCHAR temp_wchar[1024];
		MultiByteToWideChar(CP_UTF8,0,name,-1,temp_wchar,1023);
        psm->handle=OpenMutexW(MUTEX_ALL_ACCESS, FALSE, temp_wchar );
		if(psm->handle)
		{
			CloseHandle(psm->handle);
			free(psm);
			return (struct TSPMutex*)0;
		}	
        psm->handle=CreateMutexW(NULL,FALSE,temp_wchar);
		if(!(psm->handle))
		{
			free(psm);
			return (struct TSPMutex*)0;
		}
		psm->isCreate=1;
	}else{
		WCHAR temp_wchar[1024];
		MultiByteToWideChar(CP_UTF8,0,name,-1,temp_wchar,1023);
        psm->handle=OpenMutexW(MUTEX_ALL_ACCESS, FALSE, temp_wchar );
		if(!(psm->handle))
		{
			free(psm);
			return (struct TSPMutex*)0;
		}
		psm->isCreate=0;
	}	
		
	return psm;
	
}

/*Windows реализация*/
int spCloseMutex(struct TSPMutex* mtx)
{
	if(!mtx)
	{
		return 0;
	}	
	if(mtx->handle)
	{
		CloseHandle(mtx->handle);
		free(mtx);
		return 1;
	}	
	free(mtx);
	return 0;
}



/*Windows реализация
 * Функция возвращает 1, если успешно. 0 - если время timeout_ms вышло, -1 -- прочие ошибки*/
int spWaitForMutex(struct TSPMutex* mtx, long timeout_ms)
{
	
	DWORD waitResult;
	
	if(!mtx||!(mtx->handle))
	{
		return (-1);
	}	
	
	waitResult=WaitForSingleObject(mtx->handle,timeout_ms);
	switch(waitResult)
	{
		case WAIT_OBJECT_0:
			return 1;
		break;
		
		case WAIT_TIMEOUT:
			return 0;
		break;	
	};
	return (-1);
}



/**Windows реализация
 * 
 * Освобождение мьютекса*/
int spReleaseForMutex(struct TSPMutex* mtx)
{
	if(!mtx||!(mtx->handle))
	{
		return (-1);
	}	
	if(!ReleaseMutex(mtx->handle))
	{
		return 0;
	}	
	return 1;
}

#else

	#include <time.h>
	#include <errno.h>
/*Unix реализация открытия мьютекса*/	
struct TSPMutex* spOpenMutex(const char *name, int isCreate)
{
	struct TSPMutex *psm;
	psm=(struct TSPMutex*)malloc(sizeof(struct TSPMutex));
	if(!psm)
	{
		return (struct TSPMutex*)0;
	}

        psm->name=(char*)malloc(sizeof(char)*(strlen(name)+2));
        if(!(psm->name))
        {
                free(psm);
                return (struct TSPMutex*)0;
        }
        psm->name[0]='/';
        strcpy(psm->name+1,name);

	if(isCreate)
	{
		psm->sem=sem_open(psm->name,O_CREAT|O_EXCL,S_IRUSR|S_IWUSR,1);
		if(psm->sem==SEM_FAILED)
		{
			free(psm->name);
			free(psm);
			return (struct TSPMutex*)0;
		}	
		psm->isCreate=1;
	}else{
		psm->sem=sem_open(psm->name,0);
		if(psm->sem==SEM_FAILED)
		{
			free(psm);
			return (struct TSPMutex*)0;
		}
		psm->isCreate=0;
	}	
	return psm;
}

/*Unix реализация закрытия мьютекса*/
int spCloseMutex(struct TSPMutex* mtx)
{
	if(!mtx)
	{
		return 0;
	}	
	if(mtx->sem)
	{
		sem_close(mtx->sem);
		if(mtx->isCreate)
		{
			if(mtx->name)
			{
				sem_unlink(mtx->name);
				free(mtx->name);
			}	
                }else{
                    if(mtx->name)
                    {
                        free(mtx->name);
                    }
               }
               free(mtx);
               return 1;

	}	
	free(mtx);
	return 0;
}




/*Unix реализация. Входа в защищенную часть
 * 
 * Функция возвращает 1, если успешно. 0 - если время timeout_ms вышло, -1 -- прочие ошибки*/
int spWaitForMutex(struct TSPMutex* mtx, long timeout_ms)
{
    struct timespec ts;
	int val_sem,s,r;
	if(!mtx||!(mtx->sem))
	{
		return (-1);
	}	
	
	if(clock_gettime(CLOCK_REALTIME, &ts)<0)
	{
		return (-1);
	}	

//Укажем новое время
	ts.tv_sec+=timeout_ms/1000;
	ts.tv_nsec+=(1000000*(timeout_ms-(timeout_ms/1000)*1000));
	
	if(ts.tv_nsec>=1000000000)
	{
		ts.tv_sec+=ts.tv_nsec/1000000000;
		ts.tv_nsec%=1000000000;
	}	
	
	
	
	r=0;	
	while(((s = sem_timedwait(mtx->sem, &ts)) == -1 && errno == EINTR)||
		(s==0&& (r = sem_getvalue(mtx->sem, &val_sem))==0&&val_sem>0))
	{	
		
			continue;	
	}
	
	if(s<0)
	{
		if(errno == ETIMEDOUT)
		{
			return 0;
		}
		return (-1);
	}	
	if(r<0)
	{
		return (-1);
	}	
	return 1;	
}


/**Unix реализация
 * 
 * Освобождение мьютекса*/
 int spReleaseForMutex(struct TSPMutex* mtx)
 {
	int val_sem, s;
	
	if(!mtx||!(mtx->sem))
	{
		return (-1);
	}
	
	if(sem_getvalue(mtx->sem,&val_sem)<0)
	{
		return (-1);
	}
	if(val_sem==0)
	{
		s=sem_post(mtx->sem);
		if(s==0)return 1;
	}	
	return 0;
 }
#endif	





