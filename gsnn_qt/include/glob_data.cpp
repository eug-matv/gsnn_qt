﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "glob_data.h"
#include "gga.h"
#include "gll.h"
#include "gsa.h"
#include "gsv.h"
#include "rmc.h"
#include "vtg.h"
#include "zda.h"
#include "pirea.h"
#include "pirfv.h"
#include "pirgk.h"
#include "pirra.h"
#include "iec.h"


int TDATA_OF_ALL_PACKAGES::addDataIEC(const TTDateTime &dt,
                                      const  void *ucData,
                                            int szData)
{
     int type;     
     if(szData<10)return 0;
     type=iecToolsTypeOfPackage((unsigned char*)ucData);                                       
     if(type<=0)return 0;
     
#ifdef linux
       pthread_mutex_lock(&mutex);
#else
       EnterCriticalSection(&csCS);
#endif  
     
     if(isDestroying)
      {
#ifdef linux
         pthread_mutex_unlock(&mutex);
#else
         LeaveCriticalSection(&csCS);
#endif                       
      }      
      
     if(type<30)
     {
        nmea_dt[type].dd=dt.dd;
        nmea_dt[type].h=dt.h;
        nmea_dt[type].m=dt.m;
        nmea_dt[type].mm=dt.mm;
        nmea_dt[type].s=dt.s;
        nmea_dt[type].yyyy=dt.yyyy;      
        if(szData>nmea_maxsz[type])
        {
           if(nmea_str[type])
		   {	   
			   delete [] (nmea_str[type]);
		   }
           nmea_str[type]=new char [szData];                  
           nmea_maxsz[type]=szData;

        }
        memcpy((void*)(nmea_str[type]),(void*)ucData,szData);
        nmea_sz[type]=szData;
		last_type_data=type;
		
//Теперь разберем и добавим данное
		nmea_data[type]=iecMakeDataByStr(nmea_str[type],nmea_data[type]);

//Теперь добавим данное
		iecRazborIEC(nmea_str[type],&dt,nmea_data[type]);
		
		
#ifdef linux
       pthread_mutex_unlock(&mutex);
#else
       LeaveCriticalSection(&csCS);
#endif  
        return last_type_data;
     }

     if(type>100&&type<130)
     {
        type-=100;                   
        iec_dt[type].dd=dt.dd;
        iec_dt[type].h=dt.h;
        iec_dt[type].m=dt.m;
        iec_dt[type].mm=dt.mm;
        iec_dt[type].s=dt.s;
        iec_dt[type].yyyy=dt.yyyy;      
        if(szData>iec_maxsz[type])
        {
           if(iec_str[type])delete [](iec_str[type]);
           iec_str[type]=new char [szData];                  
           iec_maxsz[type]=szData;
        }
        memcpy((void*)(iec_str[type]),(void*)ucData,szData);
        iec_sz[type]=szData;
		last_type_data=type+100;
		
//Теперь разберем и добавим данное
		iec_data[type]=iecMakeDataByStr(iec_str[type],iec_data[type]);

//Теперь добавим данное
		iecRazborIEC(iec_str[type],&dt,iec_data[type]);
		
		
#ifdef linux
        pthread_mutex_unlock(&mutex);
#else
        LeaveCriticalSection(&csCS);
#endif          
        return last_type_data;
     }
#ifdef linux
      pthread_mutex_unlock(&mutex);
#else
      LeaveCriticalSection(&csCS);
#endif       
     return 0;
}      




int TDATA_OF_ALL_PACKAGES::
       addDataMNP(const TTDateTime &dt,
				   const void *ucData, 
				   int szData)
{
	//Пока принимаем только пакеты 3000
		if(!ucData||szData!=172||*(((unsigned short*)ucData)+1)!=3000)
		{
			return 0;
		}	
		
#ifdef linux
       pthread_mutex_lock(&mutex);
#else
       EnterCriticalSection(&csCS);
#endif  
     
     if(isDestroying)
      {
#ifdef linux
         pthread_mutex_unlock(&mutex);
#else
         LeaveCriticalSection(&csCS);
#endif                       
      }      
		
	  memcpy(k3000,ucData,szData);
	  sz_k3000=szData;
	  k3000obj.setData(k3000,dt);
	
#ifdef linux
      pthread_mutex_unlock(&mutex);
#else
      LeaveCriticalSection(&csCS);
#endif                       
    return 3000;
	
}				   



int TDATA_OF_ALL_PACKAGES::
    getDataIEC(int type,
               TTDateTime &dt,
                   void *ucData,
                   int maxszData,
                   int &szData)            
{
   int iec=0; 
   if(type==0)type=last_type_data;
   if(type<0||(type>=30&&type<=100)||type>=130||ucData==0)return (-1);                   
   if(type>100)
   {
      iec=1;         
      type-=100;
   }        
   
#ifdef linux
       pthread_mutex_lock(&mutex);
#else
       EnterCriticalSection(&csCS);
#endif     

     if(isDestroying)
     {
#ifdef linux
         pthread_mutex_unlock(&mutex);
#else
         LeaveCriticalSection(&csCS);
#endif                       
      }      
 
   if(iec)
   {
       if(iec_sz[type]>maxszData)
       {
#ifdef linux
            pthread_mutex_unlock(&mutex);
#else
            LeaveCriticalSection(&csCS);
#endif         
            return (-2);
       }
       if(iec_sz[type]==0)
       {
#ifdef linux
          pthread_mutex_unlock(&mutex);
#else
          LeaveCriticalSection(&csCS);
#endif                                   
          return 0;
       }
       dt.dd=iec_dt[type].dd;
       dt.h=iec_dt[type].dd;
       dt.m=iec_dt[type].m;
       dt.mm=iec_dt[type].mm;
       dt.s=iec_dt[type].s;
       dt.yyyy=iec_dt[type].yyyy;      
       szData=iec_sz[type];
       memcpy(ucData,iec_str[type],szData);
#ifdef linux
       pthread_mutex_unlock(&mutex);
#else
       LeaveCriticalSection(&csCS);
#endif         
       return 1;   
   }
   if(nmea_sz[type]>maxszData)
   {
#ifdef linux
      pthread_mutex_unlock(&mutex);
#else
      LeaveCriticalSection(&csCS);
#endif         
                                 
      return (-2);
   }
   if(nmea_sz[type]==0)
   {
#ifdef linux
      pthread_mutex_unlock(&mutex);
#else
      LeaveCriticalSection(&csCS);
#endif            
      return 0;
   }
   dt.dd=nmea_dt[type].dd;
   dt.h=nmea_dt[type].dd;
   dt.m=nmea_dt[type].m;
   dt.mm=nmea_dt[type].mm;
   dt.s=nmea_dt[type].s;
   dt.yyyy=nmea_dt[type].yyyy;      
   szData=nmea_sz[type];
   memcpy(ucData,nmea_str[type],szData);
#ifdef linux
   pthread_mutex_unlock(&mutex);
#else
   LeaveCriticalSection(&csCS);
#endif         
   
   return 1;       
}

int TDATA_OF_ALL_PACKAGES::getDataStr(
                   int type,
                   char *ucData,
                   int maxszData,
                   int &szData     
                        )
{
   int iec=0;   
   int i;             
   unsigned char summ=0;
   
   
   if(type==0)type=last_type_data;
   if(type<0||(type>=30&&type<=100)||type>=130||ucData==0)return (-1);                   


#ifdef linux
       pthread_mutex_lock(&mutex);
#else
       EnterCriticalSection(&csCS);
#endif     
  
     if(isDestroying)
     {
#ifdef linux
         pthread_mutex_unlock(&mutex);
#else
         LeaveCriticalSection(&csCS);
#endif                       
			return 0;
			
      }      
   
   if(type>100)
   {
      iec=1;         
      type-=100;
   }           
   if(iec)
   {
       if(iec_sz[type]==0)
       {
#ifdef linux
            pthread_mutex_unlock(&mutex);
#else
            LeaveCriticalSection(&csCS);
#endif                    
            return 0;   
       }
       if(iec_sz[type]+29>maxszData)
       {
#ifdef linux
            pthread_mutex_unlock(&mutex);
#else
            LeaveCriticalSection(&csCS);
#endif            
            return (-2);
       }
       szData=iec_sz[type]+29;
       sprintf(ucData,"@%02d,%02d,%04d,%02d,%02d,%06.03lf@",
              iec_dt[type].dd,iec_dt[type].mm, iec_dt[type].yyyy,
              iec_dt[type].h,iec_dt[type].m, iec_dt[type].s);
       
       memcpy(ucData+25,iec_str[type],iec_sz[type]);
#ifdef linux
       pthread_mutex_unlock(&mutex);
#else
       LeaveCriticalSection(&csCS);
#endif     
       
       for(i=0;i<szData-4;i++)
       {
           summ^=ucData[i];                   
       }
       sprintf((char*)(ucData+(szData-4)),"%02X",(unsigned int)summ);
       ucData[szData-2]=0x0D;
       ucData[szData-1]=0x0A;
       return 1;   
   }
   if(nmea_sz[type]==0)
   {
#ifdef linux
       pthread_mutex_unlock(&mutex);
#else
       LeaveCriticalSection(&csCS);
#endif     
      return 0;
   }
   if(nmea_sz[type]+29>maxszData)
   {
       szData=nmea_sz[type]+29;                          
#ifdef linux
       pthread_mutex_unlock(&mutex);
#else
       LeaveCriticalSection(&csCS);
#endif        

       return (-2);
   }
   szData=nmea_sz[type]+29;
   sprintf(ucData,"@%02d,%02d,%04d,%02d,%02d,%06.03lf#",
              nmea_dt[type].dd,nmea_dt[type].mm, nmea_dt[type].yyyy,
              nmea_dt[type].h,nmea_dt[type].m, nmea_dt[type].s);

   memcpy(ucData+25,nmea_str[type],nmea_sz[type]);
   
   #ifdef linux
     pthread_mutex_unlock(&mutex);
   #else
     LeaveCriticalSection(&csCS);
   #endif     
   
   for(i=0;i<szData-4;i++)
   {
           summ^=ucData[i];                   
   }
   sprintf((char*)(ucData+(szData-4)),"%02X",(unsigned int)summ);
   ucData[szData-2]=0x0D;
   ucData[szData-1]=0x0A;

   
   
   return 1;       
                        
}                        


int TDATA_OF_ALL_PACKAGES::
         addDataStr(const char *cData,
                   int szData)
{
     TTDateTime dt;              
     unsigned char summ;
     unsigned int summ1;
     int i;
     char twoB[3]={0,0,0};
     if(szData<=35||!cData)return -1;  //Даже не рассматривать              
     if(cData[0]!='@'||cData[3]!=','||
        cData[6]!=','||cData[11]!=','||cData[14]!=','||
        cData[17]!=','||cData[24]!='@'||cData[25]!='$')return (-1);
     sscanf(cData+1,"%d",&(dt.dd));
     sscanf(cData+4,"%d",&(dt.mm));
     sscanf(cData+7,"%d",&(dt.yyyy));
     sscanf(cData+12,"%d",&(dt.h));
     sscanf(cData+15,"%d",&(dt.m));
     sscanf(cData+18,"%lf",&(dt.s));
     summ=0;
     for(i=0;i<szData-4;i++)
     {
         summ^=*((unsigned char*)(cData+i));                   
     }
     
//Получим сумму
     twoB[0]=cData[szData-4];
     twoB[1]=cData[szData-3];
     sscanf(twoB,"%X",&summ1);
     if(((unsigned int)summ)!=summ1)return (-2);
     
     
     if(cData[szData-2]!=0x0D||cData[szData-2]!=0x0A)return (-2);
     
 //Теперь все нормально можно добавлять             
               
     return addDataIEC(dt,cData+25,szData-29);
     
}                                                                         


int TDATA_OF_ALL_PACKAGES::
    getNotStringData(int type,
               void *notString)
{
      int iec=0;   
      int iRet;     
      int type1;
	  int sz;
      if(type<=0||(type>=30&&type<=100)||type>=130||notString==0)return (-1);                   
      
	  
#ifdef linux
      pthread_mutex_lock(&mutex);
#else
      EnterCriticalSection(&csCS);
#endif     
 
 
      if(isDestroying)
      {
#ifdef linux
         pthread_mutex_unlock(&mutex);
#else
         LeaveCriticalSection(&csCS);
#endif                       
      }      
      if(type>100)
      {
         iec=1;         
         type1=type-100;
      }else{
         type1=type;   
      }        
      
               
      switch(type)
      {
           case NMEA_GPGGA:
           case NMEA_GLGGA:
           case NMEA_GNGGA:
				sz=sizeof(struct GGA_DATA);
           break;          
           
           case NMEA_GPGLL:
           case NMEA_GLGLL:
           case NMEA_GNGLL:
               sz=sizeof(struct GLL_DATA);
           break;          
           
           case NMEA_GPGSA:
           case NMEA_GLGSA:
           case NMEA_GNGSA:
               sz=sizeof(struct GSA_DATA);
           break;          
           
           case NMEA_GPGSV:
           case NMEA_GLGSV:
           case NMEA_GNGSV:
               sz=sizeof(struct GSV_DATA);
           break;          

           case NMEA_GPRMC:
           case NMEA_GLRMC:
           case NMEA_GNRMC:
               sz=sizeof(struct RMC_DATA);
           break;     
           
           case NMEA_GPVTG:
           case NMEA_GLVTG:
           case NMEA_GNVTG:
               sz=sizeof(struct VTG_DATA);
           break;     

           case NMEA_GPZDA:
           case NMEA_GLZDA:
           case NMEA_GNZDA:
               sz=sizeof(struct ZDA_DATA);
           break;     
           
           case IEC_PIREA:
               sz=sizeof(struct PIREA_DATA);
           break;         

           case IEC_PIRFV:
               sz=sizeof(struct PIRFV_DATA);
           break;         

           case IEC_PIRGK:
               sz=sizeof(struct PIRGK_DATA);
           break;         

           case IEC_PIRRA:
               sz=sizeof(struct PIRRA_DATA);
           break; 
		   
		   default:
			return 0;
      };         
      
	  
	  if(iec)
      {
             type1=type-100;
			 if(iec_data[type1])
			 {	 
				memcpy(notString,iec_data[type1],sz);
				return sz;
			 }
			 return 0;
      }else{
             type1=type;
			 if(nmea_data[type1])
			 {	 
				memcpy(notString,nmea_data[type1],sz);
				return sz;
			 }
			 return 0;	
      }
	  
#ifdef linux
       pthread_mutex_unlock(&mutex);
#else
       LeaveCriticalSection(&csCS);
#endif     

      if(iRet<=0)return -2;
      return 1;
}


int TDATA_OF_ALL_PACKAGES::getDataByTypeAndIndex(
                              int type,
	                          int indx,
							  unsigned char *out_bytes,		   //Выходной байт	
							  int max_sz_out_bytes,			   //размер массива	
							  int &sz_out_bytes,				   //Размер данных в байтах
							  struct TTDateTime &dt			   //Время получения пакета	 
							  )
{
	if(type>0&&type<30)
	{
		if(!nmea_data[type])return 0;
		return iecGetDataIEC(type,nmea_data[type],indx,out_bytes,max_sz_out_bytes,&sz_out_bytes,&dt);
	}	
	
	if(type>100&&type<130)
	{
		if(!iec_data[type-100])return 0;
		return iecGetDataIEC(type,iec_data[type-100],indx,out_bytes,max_sz_out_bytes,&sz_out_bytes,&dt);
	}	
	return 0;
}							  



int TDATA_OF_ALL_PACKAGES::reset()
{
	       int i;                   
#ifdef linux
       pthread_mutex_lock(&mutex);
#else
       EnterCriticalSection(&csCS);
#endif           
       for(i=0;i<30;i++)
       {
          if((nmea_maxsz[i])>0)delete[](nmea_str[i]);
          if(iec_maxsz[i]>0)delete[](iec_str[i]);    
		  if(iec_data[i])free(iec_data[i]);
		  if(nmea_data[i])free(nmea_data[i]);
          
       }

        for(i=0;i<30;i++)
        {
           nmea_str[i]=0;
           iec_str[i]=0;
           nmea_maxsz[i]=nmea_sz[i]=0;
           iec_maxsz[i]=iec_sz[i]=0; 
		   iec_data[i]=nmea_data[i]=0;
        }      
		sz_k3000=sz_k3001=sz_k3002=sz_k3003=sz_k3011=0;
		last_type_data=-1;
		
#ifdef linux
       pthread_mutex_unlock(&mutex);
#else
       LeaveCriticalSection(&csCS);
#endif           
    return 1;
}


//! Тут может быть проблема - не учитываются смешанные предложения

int TDATA_OF_ALL_PACKAGES::getRMC_DATA(int type_of_protocol,   //Тип протокола 0 - NMEA, 1 - MNP
			   int ns,				//ns=0 -- GPS, ns==1 -- ГЛОНАСС, ns==2 -- GPS - ГЛОНАСС
			    struct RMC_DATA &rmc_data	//Заполненные данные
				)
{
	int iRet;
	int type;
		 
	if(type_of_protocol)
	{
		if(sz_k3000>0)
		{
			iRet=k3000obj.getRMC_DATA(rmc_data);
			if(iRet>0)
			{
				return 1;
			}	
		
		}	
	}else{
                type=NMEA_GPRMC + ns*10.0; //! надо чтобы все воспринимал
		if(nmea_sz[type]>0)
		{
			memcpy(&rmc_data,nmea_data[type],sizeof(RMC_DATA));
			return 1;
		}	
		
	}	
	return 0;
}

//! Прототип функции определения количества спутников

int TDATA_OF_ALL_PACKAGES::getGSV_DATA( int ns, struct GSV_DATA &gsv_data)
{   
    int type;

    type=NMEA_GPGSV + ns*10.0;
    if(nmea_sz[type]>0)
    {
            memcpy(&gsv_data,nmea_data[type],sizeof(GSV_DATA));
            return 1;
    }
    return 0;
}

//! Прототип функции извлечения предложения GGA с расширенным набором данных данных

int TDATA_OF_ALL_PACKAGES::getGGA_DATA( int type_of_protocol,   //Тип протокола 0 - NMEA, 1 - MNP
                                        int ns,				//ns=0 -- GPS, ns==1 -- ГЛОНАСС, ns==2 -- GPS - ГЛОНАСС
                                         struct GGA_DATA &gga_data	//Заполненные данные
                                        )
{
    int iRet;
    int type;



    if(type_of_protocol)
    {
        if(sz_k3000>0)
        {
            iRet=k3000obj.getGGA_DATA(gga_data);
            if(iRet>0)
            {
                return 1;
            }

        }
    }else{
        type=NMEA_GPGGA + ns*10;
        if(nmea_sz[type]>0)
        {
            memcpy(&gga_data,nmea_data[type],sizeof(GGA_DATA));
            return 1;
        }

    }
    return 0;
}
