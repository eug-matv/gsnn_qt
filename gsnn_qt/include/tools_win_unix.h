﻿#ifndef __TOOLS_WIN_UNIX_H__
#define __TOOLS_WIN_UNIX_H__
#ifdef __BORLANDC__
   #define __WINDA__
#endif
#ifdef _MSC_VER
   #define __WINDA__
#endif


#include <string.h>

#ifdef linux
        #include <sys/times.h>
		#include <unistd.h>
#else
	#include <windows.h>
#endif

/*Создание каталога. MKDIR (имя каталога с путем в виде ANSI). Если успешно, возвращает 0, иначе возвращает 1.*/
#ifdef __WINDA__
        #define twuMKDIR(dir) (!CreateDirectory((dir),NULL))
#else
        #ifdef linux
                #define twuMKDIR(dir) mkdir(dir, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP)
        #else
                #define twuMKDIR(dir)  mkdir(dir)
        #endif
#endif


/*Приостановка работы потока на интервал в мс*/
#ifdef linux
                #define twuSLEEP(ms) usleep(ms*1000)
#else
                #define twuSLEEP(ms)  Sleep(ms)
#endif


/*Время*/
#ifdef linux
	#define twuGET_TICKS() times(NULL)
	#define twuONE_SECOND_TICKS sysconf(_SC_CLK_TCK)
	typedef clock_t twuCLOCK;
#else
	#define twuGET_TICKS() GetTickCount()
	#define twuONE_SECOND_TICKS 1000
	typedef DWORD twuCLOCK;
#endif


#ifdef linux
#else
#endif




/*Структура TWUFileDescr предназначена для того, чтобы изолировать работу с файлами от системо-зависимых функций.
Кроме того, в случае отсутствии или нехватки последовательных портов с эмулировать обмен через именованные каналы*/
struct TWUFileDescr
{
	int isSerial;


        int i_fd1;	//Устройство 1. На обмен и передачу, если
        int i_fd2;

#ifndef linux

	HANDLE h_fd1;
	HANDLE h_fd2;
        HANDLE hControl1;
        HANDLE hControl2;
#endif
	char *file_name1;	//Имя файла для чтения
        char *file_name2;       //Имя файла для записи
        char *file_name;    //Изначальное имя устройства
        int speed;          //Скорость обмена с устройствами
        int stop_bits;      //Стоповые биты: 0 -1 стоповый бит, 1 - 1.5 стоповых бита
                            //2 - 2 стоповых бит

        int isTest;         //Тестовый - то есть со стороны управляющей программы или
        int iError;         //Код ошибки: 0 - нет ошибки, -1 - ошибка открытие файла - неправильное имя
                            //-2

};




/*
Открытие файла-типа устройства:
 TWUFileDescr* twuOpenFileWork(char *file_name,
				   int speed,
				   int stop_bits);

 Если операционная система linux, и если  начинаться с /dev/ - то файл открывается как последовательное устройство, иначе если задается строка  <name_file>, то создаются два канала на прием и передачу <name_file>1 и <name_file>2.
Если операционная система Windows, и  имя начинается с COM или \\.\COM - то открывается как устройство, иначе создается два канала
  */

 TWUFileDescr* twuOpenFileWork(const char *file_name,
				   int speed,
				   int stop_bits);




/*
Открытие файла-типа устройства:
 TWUFileDescr* twuOpenFileTest(char *file_name,
				   int speed,
				   int stop_bits);

 Если операционная система linux, и если  начинаться с /dev/ - то файл открывается как последовательное устройство, иначе если задается строка  <name_file>, то создаются два канала на прием и передачу <name_file>1 и <name_file>2.
Если операционная система Windows, и  имя начинается с COM или \\.\COM - то открывается как устройство, иначе создается два канала
  */

 TWUFileDescr* twuOpenFileTest(const char *file_name,
				   int speed,
				   int stop_bits);



 /*Переоткрыть файл - то есть закрыть и открыть снова.
   Возвращает дискриптор нового файла, если переоткрытие прошло успешно, иначе NULL
*/
  TWUFileDescr* twuReopenFile(TWUFileDescr* twuFile);



/*Чтение данных из файла*/
 int twuReadFile(TWUFileDescr* twuFile,
			void *data,
			int size);





int twuWriteFile(TWUFileDescr* twuFile,
			void *data,
			int size);


int twuPurgeFile(TWUFileDescr* twuFile, unsigned int whats);

int twuCloseFile(TWUFileDescr* twuFile);



/*Проверка существование каталога и возможности записи туда.
Возвращает 1, если каталог сушествует и туда можно писать и читать.
0 - если каталога не существует.
-1 - если писать или читать-писать в/в-из данный каталог нельзя.
-2 - файл не является каталогом
*/
int twuTestDirReadWrite(const char *path);





/*Проверка существование каталога и, если его нет, то создания его.
Возвращает 1, если каталог сушествует и туда можно писать и читать.
0 - если каталога не существует.
-1 - если писать или читать-писать в/в-из данный каталог нельзя.
-2 - файл не является каталогом
*/
int twuMakeDirReadWrite(const  char *path);







#endif


