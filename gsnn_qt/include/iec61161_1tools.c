﻿#include <stdlib.h>
#include <string.h>
#include "iec61161_1tools.h"

int iecToolsGetTime(const char *strka, 
		    int *hh, int *mm,double *ssss)
{
    int iState=0;
    int i=0;
    int iIntParty=-1;
    double dfFloatParty=-1;
    double drob;			                //Дробь
    for(i=0;strka[i]!=','&&strka[i]!='*';i++)
    {
	  if(iState==0)
	  {
		if(strka[i]>='0'&&strka[i]<='9')
		{
		    iIntParty=strka[i]-0x30;
		    iState=1;
		    continue;
		} 
		continue;
	  }  
	  
	  if(iState==1)
	  {
		if(strka[i]>='0'&&strka[i]<='9')
		{
		     iIntParty=iIntParty*10+(strka[i]-0x30);
		     continue;
		}    
		if(strka[i]=='.')
		{
		     dfFloatParty=0.0;
		     iState=2;
		     drob=0.1;
		     continue;
		}  
		return 0;
	  }  
	  if(iState==2)
	  {
		if(strka[i]>='0'&&strka[i]<='9')
		{
		     dfFloatParty+=(strka[i]-0x30)*drob;
		     drob*=0.1;
		     continue;
		}
		if(strka[i]==' ')break;
		  
	  }  
    }
    if(iIntParty<0||dfFloatParty<0)
    {
	  return 0;
    }
    *hh=iIntParty/10000;
    iIntParty-=(*hh)*10000;
    *mm=iIntParty/100;
    iIntParty-=(*mm)*100;
    *ssss=(double)(iIntParty)+dfFloatParty;
    return 1;
  
}



int iecToolsGetLatitudeGrMin(const char *strka, 
		    int *graduses, double *minutes)
{
    int iState=0;
    int i=0;
    int iIntParty=-1;
    double dfFloatParty=-1;
    double drob;			                //Дробь
    for(i=0;strka[i]!=','&&strka[i]!='*';i++)
    {
	  if(iState==0)
	  {
		if(strka[i]>='0'&&strka[i]<='9')
		{
		    iIntParty=strka[i]-0x30;
		    iState=1;
		    continue;
		} 
		continue;
	  }  
	  
	  if(iState==1)
	  {
		if(strka[i]>='0'&&strka[i]<='9')
		{
		     iIntParty=iIntParty*10+(strka[i]-0x30);
		     continue;
		}    
		if(strka[i]=='.')
		{
		     dfFloatParty=0.0;
		     iState=2;
		     drob=0.1;
		     continue;
		}  
		return 0;
	  }  
	  if(iState==2)
	  {
		if(strka[i]>='0'&&strka[i]<='9')
		{
		     dfFloatParty+=(strka[i]-0x30)*drob;
		     drob*=0.1;
		     continue;
		}
		if(strka[i]==' ')break;
		  
	  }  
    }
    if(iIntParty<0||dfFloatParty<0)
    {
	  return 0;
    }
    *graduses=iIntParty/100;
    iIntParty-=(*graduses)*100;
    *minutes=(double)(iIntParty)+dfFloatParty;
    
    return 1;
    
}  


//Получение даты
int iecToolsGetDate(const char *strka, 
		    int *dd, int *mm,int *yy)
{
    int iState=0;
    int i=0;
    int iIntParty=-1;
    
    for(i=0;strka[i]!=','&&strka[i]!='*';i++)
    {
	  if(iState==0)
	  {
		if(strka[i]>='0'&&strka[i]<='9')
		{
		    iIntParty=strka[i]-0x30;
		    iState=1;
		    continue;
		} 
		continue;
	  }  
	  
	  if(iState==1)
	  {
		if(strka[i]>='0'&&strka[i]<='9')
		{
			 *dd=iIntParty*10+(strka[i]-0x30);
		     iIntParty=0;
			 iState=2;
			 continue;
		}
		return 0;
	  }  
	  
	  if(iState==2)
	  {
		  if(strka[i]>='0'&&strka[i]<='9')
		{
		    iIntParty=strka[i]-0x30;
		    iState=3;
		    continue;
		} 
		continue;
	  }	  
	  
	  if(iState==3)
	  {
		  if(strka[i]>='0'&&strka[i]<='9')
		  {
			 *mm=iIntParty*10+(strka[i]-0x30);
		     iState=4;
		     continue;
		  } 
		  continue;
	  }
	  
	  if(iState==4)
	  {
		  if(strka[i]>='0'&&strka[i]<='9')
		  {
			 iIntParty=strka[i]-0x30;
		     iState=5;
		     continue;
		  }
	  }	  
	  if(iState==5)
	  {
		  if(strka[i]>='0'&&strka[i]<='9')
		  {
			 iIntParty=iIntParty*10+(strka[i]-0x30);
		     continue;
		  }
	  }
    }
    if(iIntParty<0)
    {
	  return 0;
    }
    if(iIntParty<100)
	{
		iIntParty+=2000;
	}	
	*yy=iIntParty;
    return 1;
}		    


//! Во здесь надо приравнять тип RMC сообшщений
//! от разных навигационных систем
int iecToolsTypeOfPackage(const unsigned char *data)
{
     int mes_type;    
     if(data[0]!='$')return IEC_FAULT;
     if(data[1]=='G'&&data[3]=='G'&&data[4]=='G'&&data[5]=='A')
     {
           mes_type=NMEA_GPGGA; 
     }else 
     if(data[1]=='G'&&data[3]=='G'&&data[4]=='S'&&data[5]=='A') 
     {
         mes_type=NMEA_GPGSA;
     }else
     if(data[1]=='G'&&data[3]=='G'&&data[4]=='S'&&data[5]=='V')
     {
         mes_type=NMEA_GPGSV;
     }else
     if(data[1]=='G'&&data[3]=='R'&&data[4]=='M'&&data[5]=='C')
     {
         mes_type=NMEA_GPRMC;
     }else
     if(data[1]=='G'&&data[3]=='V'&&data[4]=='T'&&data[5]=='G')
     {
         mes_type=NMEA_GPVTG;                                                        
     }else
     if(data[1]=='G'&&data[3]=='G'&&data[4]=='L'&&data[5]=='L')
     {
         mes_type=NMEA_GPGLL;
     }else
     if(data[1]=='G'&&data[3]=='Z'&&data[4]=='D'&&data[5]=='A')
     {
         mes_type=NMEA_GPZDA;                                                      
     }else
     if(data[1]=='P'&&data[2]=='I'&&data[3]=='R'&&data[4]=='P'&&data[5]=='R')
     {
         mes_type=IEC_PIRPR;                                                                    
     }else
     if(data[1]=='P'&&data[2]=='I'&&data[3]=='R'&&data[4]=='T'&&data[5]=='R')
     {
         mes_type=IEC_PIRTR;                                                                     
     }else
     if(data[1]=='P'&&data[2]=='I'&&data[3]=='R'&&data[4]=='S'&&data[5]=='R')
     {
         mes_type=IEC_PIRSR;                                                                     
     }else
     if(data[1]=='P'&&data[2]=='I'&&data[3]=='R'&&data[4]=='E'&&data[5]=='A')
     {
         mes_type=IEC_PIREA;                                                                    
     }else
     if(data[1]=='P'&&data[2]=='I'&&data[3]=='R'&&data[4]=='F'&&data[5]=='V')
     {
         mes_type=IEC_PIRFV;     
     }else
     if(data[1]=='P'&&data[2]=='I'&&data[3]=='R'&&data[4]=='G'&&data[5]=='K')
     {
        mes_type=IEC_PIRGK;     
     }else
     if(data[1]=='P'&&data[2]=='I'&&data[3]=='R'&&data[4]=='R'&&data[5]=='A')
     {
        mes_type=IEC_PIRRA;  
     }else{
        mes_type=IEC_FAULT;
     }                     
     
     //! Вот оно - нехорошеее место
     //! тут подправили

     //! Было не 0!:

     if(mes_type>0&&mes_type<10)
     {
        if(data[2]=='L')
        {
           mes_type+= 0;

        }else
        if(data[2]=='N')
        {
           mes_type+=0;

        }else
        if(data[2]!='P')
        {
          mes_type=IEC_FAULT;
        }
     }
     return mes_type;

     //! теперь без разницы от какой навигационной системы пришло сообщение
     //! сообщения теперь различаются по типу навигационной иеформации
     //! сообщения с непредусмотренной второй буквой заголовка отбраковываются
/**
     if(mes_type>0&&mes_type<10)
     {
        if(data[2]!='P' || data[2]!='L' || data[2]!='N')
        {           
           mes_type=IEC_FAULT;
        }
        else
        {
            mes_type += 0;
        }
     }  
     return mes_type;

   */
   //! Конец правки
}



 struct TIEC_String_Data* iecInitIEC_String_Data(int type)
 {
	struct TIEC_String_Data* iec; 
	if((type>=1&&type<=7)||
		(type>=11&&type<=17)||
		(type>=21&&type<=27)||
		(type>=101&&type<=107))
	{
	   iec=(struct TIEC_String_Data*)malloc(sizeof(struct TIEC_String_Data));	 		
	   if(!iec)return 0;
	   iec->type=type;
	   if(type<100)
	   {
		   iec->max_sz_str_data=100;
		   iec->str_data=(char*)malloc(iec->max_sz_str_data);
		   switch(type%10)
		   {	
				case 1:	  //Время, местоположение и годность навигационного решения
					strcpy(iec->str_data,"$GXGGA,,,,,,,,,,,,,,*");
				break;
				case 2:
//В GSA - есть некоторые проблемы				
					strcpy(iec->str_data,"$GXGSA,,,,,,,,,,,,,,,,,*");
				break;
				case 3:  //GSV - видимые спутники полное обновление
//В GSA
				break;	
				
		   };   
		   
	   }
	   
	   
	}	
    return iec;
 }
