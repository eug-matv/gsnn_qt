﻿#ifndef __RMC_H__
#define __RMC_H__

#include "time_tools.h"


struct RMC_TIME_UTC
{
    int h;
    int m;
    double s;
};


struct RMC_LATITUDE
{
  int gr;		//градусы 
  double m;		//минуты
  char a;		//север/юг (N/S)
};


struct RMC_LONGITUDE
{
  int gr;		//градусы 
  double m;		//минуты
  char a;		//восток/запал (E/W)
};

struct	 RMC_DATE
{
      int dd;
      int mm;	
      int yy;
};

//magnetic declination
//
struct RMC_MAGN_DECL
{
      double x_x;	//магнитное склонение в градусах
      char a;		//восток/запад (E/W)
};  



struct RMC_DATA
{
      int ns;				//Тип навигационной системы. 0 - GPS, 1 - ГЛОНАС, 2 GPS+Глонас
							//3 - искуственно созданный пакет из других протоколов
		
      struct RMC_TIME_UTC sHHMMSS_SS;  //1 время UTC
      char A;				//2 Статус: V - решение не годно; A - автономный режим, 
                    // D - дифферинциальный режим
      struct RMC_LATITUDE sBBBB_BBBB_a;  //3 Широта
      struct RMC_LONGITUDE sLLLL_LLLL_a; //4 Долгота
      double v_v;			//5 Наземная скорость в узлах
      double z_z;			//6 Наземный курс в градусах
      struct RMC_DATE sDDMMYY; 		//7 Дата
      struct RMC_MAGN_DECL x_x_a;	//8 Магнитное склонение в градусах	
      char b;				//9 режим местоопределения: A -  автономный, D - дифференциальный,
					//E - ожидаемый (сопровождение при  недостаточном количестве спутников),
					//M -  ручной ввод, S -  режим имитации, N - данные не годны
					
      struct TTDateTime dt[10];	 //Время получения значений каждого из объектов	. Индекс 0 соответствует времени
                                //получения пакетов
					
};

#ifdef __cplusplus
extern "C"
{
#endif	
int rmcRazborRMC( const char *str_data,
		    const struct TTDateTime *dt,
			struct RMC_DATA *rmc_data
			);


int rmcMakeRMC(
			   const struct RMC_DATA *rmc_data,
			   char *str_data,
			   int size_str_data
			  );			
			  
int rmcGetDataRMC(
		const struct RMC_DATA *rmc_data,   //Указатель 
		int indx,						   //Индекс	
		unsigned char *out_bytes,		   //Выходной байт	
		int max_sz_out_bytes,			   //размер массива	
		int *sz_out_bytes,				   //Размер данных в байтах
		struct TTDateTime *dt			   //Время получения пакета	 
				);			  
				
				
int rmcPrintfRMC(void *fp, 		//Дескриптор файл
			const struct RMC_DATA *rmc_data);

			  
#ifdef __cplusplus
}
#endif

#endif
