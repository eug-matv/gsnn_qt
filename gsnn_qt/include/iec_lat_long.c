#include <stdio.h>
#include "iec_lat_long.h"


int iecLLMakeStringFromLatOrLong(void *d,    //������ ���� TIecLatLong
					char *str,	//������
					int sz_str	//������ ������
                       )
{
	
	struct TIecLatLong *iecLL;
	int iMin;
	double dfSec;
	
	if(sz_str<15)return 0;
	
	iecLL=(struct TIecLatLong *)d;
	
	iMin=iecLL->m;
	dfSec=(iecLL->m-iMin)*60.0;
	
	if(iecLL->a=='N'||iecLL->a=='S')
	{	
                sprintf(str,"%c%02d�%02d\'%04.01lf\"",iecLL->a,iecLL->gr,iMin,dfSec);
	}else 
	if(iecLL->a=='W'||iecLL->a=='E')
	{
                sprintf(str,"%c%03d�%02d\'%04.01lf\"",iecLL->a,iecLL->gr,iMin,dfSec);
	}else{
		return 0;
	}	
	return 1;
}					   
