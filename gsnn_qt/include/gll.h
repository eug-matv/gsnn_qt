﻿
/*GLL - местоположение, время и годность навигационного решения */

#ifndef 	__GLL_H__
#define __GLL_H__

#include "time_tools.h"

struct GLL_LATITUDE
{
  int gr;		//градусы 
  double m;		//минуты
  char a;		//север/юг (N/S)
};


struct GLL_LONGITUDE
{
  int gr;		//градусы 
  double m;		//минуты
  char a;		//восток/запал (E/W)
};


struct GLL_TIME_UTC
{
    int h;
    int m;
    double s;
};


struct GLL_DATA
{
    int ns;				//Тип навигационной системы. 0 - GPS, 1 - ГЛОНАС, 2 GPS+Глонас
    struct GLL_LATITUDE sBBBB_BBBB_a;	//1 Широта
    struct GLL_LONGITUDE sLLLL_LLLL_a;	//2 Долгота
    struct GLL_TIME_UTC sHHMMSS_SS;	//3 Время обсервации UTC
    char A;				//4 Статус: V - решение не годно, A - автономный режим, 
					//D - дифференциальный режим
    char b;		//5 режим местоопределения: A - автономный, D - дифференциальный, 
			//E - ожидаемый (сопровождение при недостаточном количество спутников),
			//M - ручной ввод, S -  режим иммитации, N - данные не годны					

   	struct TTDateTime dt[6];
};


#ifdef __cplusplus
extern "C"
{
#endif	
int gllRazborGLL( const char *str_data,
		    struct GLL_DATA *gll_data);

#ifdef __cplusplus
}
#endif


#endif
