﻿#include <stdio.h>
#include "gll.h"
#include "iec61161_1tools.h"

int gllRazborGLL( const char *str_data,
		    struct GLL_DATA *gll_data)
{
    int i=1,k=0,j=0;
    int iRet;
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
    
    if(str_data[0]!='$')return 0;
    while(str_data[i])
    {
	summ^=(unsigned char)(str_data[i]);
	if(str_data[i]==','||str_data[i]=='*')
	{
	    if(j==0)
	    {
		if(k!=5)
		{
		      return 0;
		}  
		if(str_data[i-k]!='G'||str_data[i-k+2]!='G'||str_data[i-k+3]!='L'||str_data[i-k+4]!='L')
		{
		      return 0;
		}  
		if(str_data[i-k+1]=='P')gll_data->ns=0;
		else if(str_data[i-k+1]=='L')gll_data->ns=1;
		else if(str_data[i-k+1]=='N')gll_data->ns=2;
		else return 0;
	    }
	    else if(j==1)
	    { 
	      
	//Считаем широту 	  
		if(k>0)
		{
		      iRet=iecToolsGetLatitudeGrMin(str_data+(i-k),&(gll_data->sBBBB_BBBB_a.gr),&(gll_data->sBBBB_BBBB_a.m));
		      if(iRet<=0)return 0;
		}else{
		      return 0;
		}  
	    }
	    else if(j==2)
	    {
	      
		if(k!=0)
		{  
		   int ii;
		   gll_data->sBBBB_BBBB_a.a=0;
		   for(ii=i-k;ii<i;ii++)
		   {
			if(str_data[ii]=='N'||str_data[ii]=='S')
			{
			    gll_data->sBBBB_BBBB_a.a=str_data[ii];
			}
		   }
		   if(gll_data->sBBBB_BBBB_a.a==0)return 0;
		}else{
		    return 0;
		} 	      
	    }
	    else if(j==3)
	    {
	  //Считаем долготу  
		 if(k>0)
		 {
		      iRet=iecToolsGetLatitudeGrMin(str_data+(i-k),&(gll_data->sLLLL_LLLL_a.gr),&(gll_data->sLLLL_LLLL_a.m));
		      if(iRet<=0)return 0;
		  }else{
		      return 0;
		  }   
	    }else if(j==4)
	    {
	      //Считаем запад это или восток
		if(k!=0)
		{  
		   int ii;
		   gll_data->sLLLL_LLLL_a.a=0;
		   for(ii=i-k;ii<i;ii++)
		   {
			if(str_data[ii]=='E'||str_data[ii]=='W')
			{
			    gll_data->sLLLL_LLLL_a.a=str_data[ii];
			}
		   }
		   if(gll_data->sLLLL_LLLL_a.a==0)return 0;
		 }else{
		    return 0;
		 }		
	    }
	    else if(j==5)
	    {
			      //Считаем время UTC
		if(k!=0)
		{  
		  int hh,mm;
		  double ssss;
		  iRet=iecToolsGetTime(str_data+(i-k),&hh,&mm,&ssss);
		  if(iRet<=0)return 0;
		  gll_data->sHHMMSS_SS.h=hh;
		  gll_data->sHHMMSS_SS.m=mm;
		  gll_data->sHHMMSS_SS.s=ssss;
		  
		}else{
		    return 0;
		}  

	    }else if(j==6)
	    {
      //Счмиаем статус
		if(k!=0)
		{  
		   int ii;
		   gll_data->A=0;
		   for(ii=i-k;ii<i;ii++)
		   {
			if(str_data[ii]=='V'||str_data[ii]=='A'||str_data[ii]=='D')
			{
			    gll_data->A=str_data[ii];
			}
		   }
		   if(gll_data->A==0)return 0;
		}else{
		    return 0;
		}  
	    }else if(j==7)
	    {
	//Считаем режим местоопределения
		if(k!=0)
		{  
		   int ii;
		   gll_data->b=0;
		   for(ii=i-k;ii<i;ii++)
		   {
			if(str_data[ii]=='A'||str_data[ii]=='D'||
			   str_data[ii]=='E'||str_data[ii]=='M'||
			   str_data[ii]=='S'||str_data[ii]=='N')
			{
			    gll_data->b=str_data[ii];
			}
		   }
		   if(gll_data->b==0)return 0;
		}else{
		    return 0;
		}
	    }
	}else{  
	    k++;
	}
	
	if(str_data[i]=='*')
	{
	    iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
	    if(iRet!=1)return 0;
	    if(((unsigned int)summ)!=recv_summ)
	    {
		return 0;
	    }
	    break;
	}  
	
	summ^=(unsigned char)(str_data[i]);
	i++;
    };  
    return 1;  
}
