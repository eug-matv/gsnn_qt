﻿/*Данные об отбракованных НКА*/

#ifndef 		__PIRRA_H__
#define 	__PIRRA_H__

#include "time_tools.h"


struct PIRRA_DATA
{
  int x[1000];
  int n_of_x;
};

#ifdef __cplusplus
extern "C"
{
#endif	
int pirraRazborPIRRA( const char *str_data,
		    struct PIRRA_DATA *pirra_data);
#ifdef __cplusplus
}
#endif

#endif
