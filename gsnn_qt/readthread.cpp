﻿#include "readthread.h"


//include\time_tools.h
#include "time_tools.h"

//include\reading_nmea_data.h
#include "reading_nmea_data.h"

//include\reading_mnp_data.h
#include "reading_mnp_data.h"

//include\tools_win_unix.h
#include "tools_win_unix.h"

//include\glob_data.h
#include "glob_data.h"

#include <sys/types.h>
#include <sys/stat.h>

#ifdef linux
#include <sys/times.h>
#include <unistd.h>
#else
#include <windows.h>
#endif

#include <fcntl.h>

#include <time.h>

#include <QMutex>
#include <QThread>
#include <QTimer>
#include "time_count_tools.h"

#ifdef DEBUG_WORK_TH_START
#define DEBUG_WTH_ST_MESSAGE(txt) DEBUG_MESSAGE(txt)
#define DEBUG_WTH_ST_MESSAGE_N(txt, N)   \
{	\
char tmp_str[3000]; \
sprintf(tmp_str,"%s%d",txt,N); \
DEBUG_MESSAGE(tmp_str)	\
}

#else
#define DEBUG_WTH_ST_MESSAGE(txt)
#define DEBUG_WTH_ST_MESSAGE_N(txt, N)
#endif



//====================================================

// Конструктор
readThread::readThread()//:QThread()
{
    isWork=0;
    type_of_protocol=0;
    brate=0;
    portname[0]=0;
    stopbits=0;
    cPortName_remote1[0]=cPortName_remote2[0]=0;
    isSendDataToRemote1=isSendDataToRemote2=iStopBits_remote1=iStopBits_remote2=0;
    iBaudeRate_remote1=iBaudeRate_remote2=0;
    twuFile=twuFile_remote1=twuFile_remote2=NULL;
    lTimeOfLastSendEOCPE=0;
    lTimeOfLastSendEOCPFE1E=0;
    lTimeOfLastSendEOCPFE2E=0;
    whyOtpravlyat_sendDataToRemote=0;

}

//====================================================

// Деструктор
readThread::~readThread()
{
    stop(); //Остановим чтение данных

    // Очищаем очередь данных
    glob_data.reset();

    wait(); //Ждем завершения работы

}

//====================================================

// Уничтожить поток
void readThread::destroy()
{
    stop();

    // Очищаем очередь данных
    glob_data.reset();

    delete this;
}

//====================================================

// Установка параметров для соединения по последовательному порту
void readThread::setOptions(
    int protocol,           // Тип протокола 0 - nmea (iec), 1 - mnp - бинарный
    const char *comport,    // Символьное наиемнование последовательного порта
    int baudrate,           // Скорость чтения в битах
    int stop_bits,          // Число стоповых бит. 0  - 1, 1  - 1.5, 2 - 2
    int period_test_ret_sec, //Период в течении которого допустимо отсутствие данных на вх. последовательном порте
    int _is_remote1,            //Работать ли с первым удаленным узлом
    char *_cPortName_remote1,    //Имя последовательного порта, куда данные будут пересылаться как удаленные
                                        //если значение нулевое или none - то не посылаются данные
    int _iBaudeRate_remote1,  // скорость обмена по порту, бод для обмена с удаленным узлом
    int _iStopBits_remote1,   // 0 - один стоп бит

    int _is_remote2,            //работать ли со вторым удаленным узлом
    char * _cPortName_remote2,    //Имя последовательного порта, куда данные будут пересылаться как удаленные
                                        //если значение нулевое или none - то не посылаются данные
    int _iBaudeRate_remote2,  // скорость обмена по порту, бод для обмена с удаленным узлом
    int _iStopBits_remote2   // 0 - один стоп бит
               )
{
    type_of_protocol=protocol%2;
    if(strlen(comport)>=200)
    {
        strncpy(portname,comport,199);
        portname[199]=0;
    }else{
        strcpy(portname,comport);
    }
    brate=baudrate;
    stopbits=stop_bits%3;

    period_test_ret=period_test_ret_sec;
    if(period_test_ret<1)period_test_ret=1;
    if(period_test_ret>20)period_test_ret=20;

    isSendDataToRemote1=_is_remote1;
    strcpy(cPortName_remote1,_cPortName_remote1);
    iBaudeRate_remote1=_iBaudeRate_remote1;
    iStopBits_remote1=_iStopBits_remote1;

    isSendDataToRemote2=_is_remote2;
    strcpy(cPortName_remote2,_cPortName_remote2);
    iBaudeRate_remote2=_iBaudeRate_remote2;
    iStopBits_remote2=_iStopBits_remote2;
}

//====================================================

// Запуск потока
int readThread::start()
{
    if(isWork) // Если поток уже запущен, выходим из функции
    {

       return 0;
    }

    // Откроем последовательный порт с помощью дескриптора
    isWork=1;
//Запустим сам вторичный поток
    QThread::start();
    return 1;
}

//====================================================

// Остановка потока чтения
int readThread::stop()
{

    // Если поток уже остановлен - выходим из функции
    if(!isWork)
        return 0;
    readMutex.lock();
    isWork=0;
    readMutex.unlock();
    wait(5000);

    isWork = 0;

    return 1;
}

//====================================================




// Основной цикл работы потока чтения данных
void readThread::run()
{
    int i;
    int iRet,n_of_b;
    unsigned char tmp_data[100];
    long c_per_testing;	//  период в мс, когда считается что связь есть - изначально 5 секунд
    long c_testing;                   //Для оценки времени исчерпывания
    long temp_clock;


    error_recive_data=0;




    // проверка на таймауты как в Gettingdata.cpp maorl_tools


    timeoutMutex.lock();

    c_per_testing=period_test_ret*1000; //Оценим период в миллисекундах

    timeoutMutex.unlock();





    error_recive_data=0;			//Нет ошибок с данными
    error_recive_package=0;         //нет ошибок с пакетами
    was_data=true;                  //Байты поступали
    was_package=true;               //Пакеты будьто были распознаны

    n_of_b=1;
    timeoutMutex.lock();
    c_testing=ttGetTimePlusDTime(ttGetMillySecondsCounts(),c_per_testing);

    timeoutMutex.unlock();


    // Цикл чтения данных с порта
    while(isWork == 1)
    {



        readMutex.lock();

        if(!isWork)
            {
            readMutex.unlock();        
            break;
            }

        readMutex.unlock();

        //Проверим открытие файла
        if(testAndOpenComPort(0)<0)
        {
        //Подождем 100 мс и повторим
            twuSLEEP(100);
            continue;
        }


        // число байт доступных для чтения с порта
        n_of_b=twuReadFile(twuFile, tmp_data, 10);

        if(n_of_b>0) // если байты есть
        {
            was_data=true; // - данные были получены
            if(error_recive_data)
            {
                emit againDataEvent();
                error_recive_data=0;
            }

/*25.05.2015 Убрали. Теперь синхронизироваться будет по текущему времени
 Реализация в функции
//Проверим, а может быть послать байты на удаленный узел
            if(testAndOpenComPort(1)>0)
            {
                iRet=twuWriteFile(twuFile_remote1,tmp_data,n_of_b);
                if(iRet<n_of_b)
                {
//Попробуем переоткрыть
                    if(closeAndOpenComPort(1)>0)
                    {
                        iRet=twuWriteFile(twuFile_remote1,tmp_data,n_of_b);
                    }
                }
            }

//А может быть послать на второй
            if(testAndOpenComPort(2)>0)
            {
                iRet=twuWriteFile(twuFile_remote2,tmp_data,n_of_b);

                if(iRet<n_of_b)
                {
//Попробуем пересоздать
                    if(closeAndOpenComPort(2)>0)
                    {
                        iRet=twuWriteFile(twuFile_remote2,tmp_data,n_of_b);
                    }
                }

            }
*/


            if(type_of_protocol) // если бинарный протокол MNP-binary
                {
                for(i=0;i<n_of_b;i++)
                    {
                    iRet=fifo_mnp_data.addByte(tmp_data[i]);
                    if(iRet==3)
                        {
                        ttGetLocalTime(&dt);
                        }
                    else if(iRet==2)
                        {
                        readMutex.lock();

                        //Добавим к глобальным данным

                         iRet=glob_data.addDataMNP(dt,
                                                 fifo_mnp_data.getReceivedDataUnsafe(),
                                                 fifo_mnp_data.getSizeOfReceivedDataUnsafe()
                                                 );

                        readMutex.unlock();

                        if(iRet>0)
                            {
                                was_package=1;
                                if(error_recive_package)
                                {
                                    emit againPackageEvent();
                                    error_recive_package=0;
                                }
                                emit receivedData(iRet);
                            }

                        }
                    }
                }
            else // если протокол NMEA
                {
                for(i=0;i<n_of_b;i++)
                    {
                    iRet=fifo_nmea_data.addByte(tmp_data[i]);
                    if(iRet==3)
                        {
                        ttGetLocalTime(&dt);
                        }
                    else if(iRet==2)
                        {

                        readMutex.lock();


                        iRet=glob_data.addDataIEC(dt,
                                                 fifo_nmea_data.getReceivedDataUnsafe(),
                                                 fifo_nmea_data.getSizeOfReceivedDataUnsafe()
                                                 );

                        readMutex.unlock();
                        if(iRet>0)
                            {
                            was_package=1;
                            if(error_recive_package)
                            {
                                emit againPackageEvent();
                                error_recive_package=0;
                            }

                            emit receivedData(iRet);
                            }
                        }
                    }
                }
            }
        else
            {
            twuSLEEP(50);
            }


        //Проверим, не настала ли время проверки периода
        temp_clock=ttGetMillySecondsCounts();
        if(ttIsTimeCame(c_testing,temp_clock))
        {
                //Проверим дополнительно, что н
            iRet=testTimeOut();
            if(iRet)
            {
                return;
            }
            timeoutMutex.lock();
            c_testing=ttGetTimePlusDTime(c_testing,c_per_testing);
            timeoutMutex.unlock();

        }

    }





        // Закрываем файл дескриптор
        twuCloseFile(twuFile);
        twuFile=NULL;
        if(twuFile_remote1)
        {
            twuCloseFile(twuFile_remote1);
            twuFile_remote1=NULL;
        }

        if(twuFile_remote2)
        {
            twuCloseFile(twuFile_remote2);
            twuFile_remote2=NULL;
        }


    return;
}

// Получить данные RMC-предложения из очереди данных
int readThread::getRMC_DATA(int ns, struct RMC_DATA &rmc_data)
{
    int iRet;
    if(!isWork)return 0;

    readMutex.lock();

        iRet=glob_data.getRMC_DATA(type_of_protocol,ns,rmc_data);

    readMutex.unlock();

    return iRet;
}

// Получить данные GGA-предложения из очереди данных
int readThread::getGGA_DATA(int ns, struct GGA_DATA &gga_data)
{
    int iRet;
    if(!isWork)return 0;

    readMutex.lock();

        iRet=glob_data.getGGA_DATA(type_of_protocol,ns,gga_data);

    readMutex.unlock();

    return iRet;
}

/*!
int readThread::getGSV_DATA(int N_of_sentences)
{
    // сначала необходимо получить из очереди все необходимые предложения
    // для МНП7 это 6 предложений NMEA, 3 - GPS и 3 - Glonass предложений
    // каждое предложение содержит свой номер и инфо о кол-ве сообщений GSV
    // затем извлекаем из любого предложения данные о количестве видимых
    // спутников, затем определяем количество сопровождаемых спутников, те
    // тех, у которых соотношение сигнал шум больше 30 дБ.

    int iRet;
    if(!isWork)return 0;

    struct GSV_DATA *gsv_data;

    readMutex.lock();

        iRet=glob_data.getGSV_DATA(type_of_protocol,ns,gsv_data);

    readMutex.unlock();

    return iRet;


}*/


/*Метод добавлденный 26.05.2015*/
int readThread::sendDataToRemote()
{
    RMC_DATA tmpRMC;
    GGA_DATA tmpGGA;

    int sz;
    int iRet;
    char strkout[300];


    SYSTEMTIME systm;

    if(whyOtpravlyat_sendDataToRemote)
    {

        tmpRMC.ns=1;
        tmpRMC.A='A';
        tmpRMC.sBBBB_BBBB_a.a='S';
        tmpRMC.sBBBB_BBBB_a.gr=0;
        tmpRMC.sBBBB_BBBB_a.m=0;
        tmpRMC.sLLLL_LLLL_a.a='W';
        tmpRMC.sLLLL_LLLL_a.gr=0;
        tmpRMC.sLLLL_LLLL_a.m=0;
        tmpRMC.x_x_a.a='E';
        tmpRMC.x_x_a.x_x=0.1;
        tmpRMC.b='A';
        if(testAndOpenComPort(1)>0)
        {

            GetSystemTime(&systm);

            tmpRMC.sDDMMYY.yy=systm.wYear;
            tmpRMC.sDDMMYY.mm=systm.wMonth;
            tmpRMC.sDDMMYY.dd=systm.wDay;

            tmpRMC.sHHMMSS_SS.h=systm.wHour;
            tmpRMC.sHHMMSS_SS.m=systm.wMinute;
            tmpRMC.sHHMMSS_SS.s=systm.wSecond+
                                systm.wMilliseconds/1000.0;
            sz=rmcMakeRMC(&tmpRMC,strkout,sizeof(strkout));
            if(sz>0)
            {
                iRet=twuWriteFile(twuFile_remote1,strkout,sz);
                if(iRet<sz)
                {
    //Попробуем переоткрыть
                    if(closeAndOpenComPort(1)>0)
                    {
                        iRet=twuWriteFile(twuFile_remote1,strkout,sz);
                    }
                }
            }
        }
    //А может быть послать на второй
        if(testAndOpenComPort(2)>0)
        {
            GetSystemTime(&systm);

            tmpRMC.sDDMMYY.yy=systm.wYear;
            tmpRMC.sDDMMYY.mm=systm.wMonth;
            tmpRMC.sDDMMYY.dd=systm.wDay;

            tmpRMC.sHHMMSS_SS.h=systm.wHour;
            tmpRMC.sHHMMSS_SS.m=systm.wMinute;
            tmpRMC.sHHMMSS_SS.s=systm.wSecond+
                                systm.wMilliseconds/1000.0;
            sz=rmcMakeRMC(&tmpRMC,strkout,sizeof(strkout));
            if(sz>0)
            {
                iRet=twuWriteFile(twuFile_remote2,strkout,sz);
                if(iRet<sz)
                {
    //Попробуем переоткрыть
                    if(closeAndOpenComPort(2)>0)
                    {
                        iRet=twuWriteFile(twuFile_remote2,strkout,sz);
                    }
                }
            }
        }

    }else{

        tmpGGA.ns=1;
        tmpGGA.sBBBB_BBBB_a.a='S';
        tmpGGA.sBBBB_BBBB_a.gr=0;
        tmpGGA.sBBBB_BBBB_a.m=0;
        tmpGGA.sLLLL_LLLL_a.a='W';
        tmpGGA.sLLLL_LLLL_a.gr=0;
        tmpGGA.sLLLL_LLLL_a.m=0;
        tmpGGA.b=1;
        tmpGGA.cc=11;
        tmpGGA.d_d=tmpGGA.e_e=tmpGGA.g_g=0.0;
        tmpGGA.jjjj=123;
        if(testAndOpenComPort(1)>0)
        {

            GetSystemTime(&systm);



            tmpGGA.sHHMMSS_SS.h=systm.wHour;
            tmpGGA.sHHMMSS_SS.m=systm.wMinute;
            tmpGGA.sHHMMSS_SS.s=systm.wSecond+
                                systm.wMilliseconds/1000.0;
            sz=ggaMakeGGA(&tmpGGA,strkout,sizeof(strkout));
            if(sz>0)
            {
                iRet=twuWriteFile(twuFile_remote1,strkout,sz);
                if(iRet<sz)
                {
    //Попробуем переоткрыть
                    if(closeAndOpenComPort(1)>0)
                    {
                        iRet=twuWriteFile(twuFile_remote1,strkout,sz);
                    }
                }
            }
        }
    //А может быть послать на второй
        if(testAndOpenComPort(2)>0)
        {
            GetSystemTime(&systm);

            tmpGGA.sHHMMSS_SS.h=systm.wHour;
            tmpGGA.sHHMMSS_SS.m=systm.wMinute;
            tmpGGA.sHHMMSS_SS.s=systm.wSecond+
                                systm.wMilliseconds/1000.0;
            sz=ggaMakeGGA(&tmpGGA,strkout,sizeof(strkout));
            if(sz>0)
            {
                iRet=twuWriteFile(twuFile_remote2,strkout,sz);
                if(iRet<sz)
                {
    //Попробуем переоткрыть
                    if(closeAndOpenComPort(2)>0)
                    {
                        iRet=twuWriteFile(twuFile_remote2,strkout,sz);
                    }
                }
            }
        }

    }
    whyOtpravlyat_sendDataToRemote=(whyOtpravlyat_sendDataToRemote+1)%5;

    return 1;


}





//! Проверка исчерпалось ли время получения данных
//Возвращает 0 - если всё нормально и можно продолжить работать.
//1 - если была критическая ошибка и надо закрыть программу
int readThread::testTimeOut()
{
    int isRet;
    isRet=1;


    if(was_data)
    {
        was_data=false;


        if(was_package)
        {
            was_package=false;
        }else{
            if(!error_recive_package)
            {
                readMutex.lock();
                glob_data.reset();
                readMutex.unlock();
                error_recive_package=1;
                emit noPackageEvent();
            }
            emit noPackageEventForOther();

        }
    }else{
        was_package=false;
        error_recive_package=1;
        if(!error_recive_data)
        {
            readMutex.lock();
            glob_data.reset();
            readMutex.unlock();
            error_recive_data=1;
            emit noDataEvent();
        }
        emit noDataEventForOther();

//Надо побробывать переоткрыть порт

        closeAndOpenComPort(0);

    }
    return 0;
}

//Проверка открытия порта, и если  он  не открыт, то попытка его открыть
//Параметр type указывает какой порт надо открыть 0 -основной входной, 1 - порт длл связи
//с remote 1, 2 - порт для связи с Remote2.
//Возвращает: 0 -файл не должен быть открытым, согласно, настройкам
int readThread::testAndOpenComPort(int type)
{
    TWUFileDescr *twuFile_temp;
    char *sFN; //Имя файла
    int iBD;    //Скорость обмена
    int iSB;    //Число стопбит

    if(type==1)
    {
        if(!isSendDataToRemote1)return 0;
        if(twuFile_remote1)return 1;
        sFN=cPortName_remote1;
        iBD=iBaudeRate_remote1;
        iSB=iStopBits_remote1;
    }else
    if(type==2)
    {
        if(!isSendDataToRemote2)return 0;
        if(twuFile_remote2)return 1;
        sFN=cPortName_remote2;
        iBD=iBaudeRate_remote2;
        iSB=iStopBits_remote2;
    }else{
        if(twuFile)return 1;
        sFN=portname;
        iBD=brate;
        iSB=stopbits;
    }
    if(type==1||type==2)
    {
        twuFile_temp=twuOpenFileTest(sFN,iBD,iSB);
    }else{
        twuFile_temp=twuOpenFileWork(sFN,iBD,iSB);
    }
    if(!twuFile_temp)
    {
        if(type==1)
        {
            if(lTimeOfLastSendEOCPFE1E==0||
               ttIsTimeCame(lTimeOfLastSendEOCPFE1E,ttGetMillySecondsCounts()))
            {
                emit errorOpenComPortForRemote1Event(0);
                lTimeOfLastSendEOCPFE1E=ttGetTimePlusDTime(ttGetMillySecondsCounts(),
                                                           period_test_ret*1000);
            }
        }else
        if(type==2)
        {
            if(lTimeOfLastSendEOCPFE2E==0||
               ttIsTimeCame(lTimeOfLastSendEOCPFE2E,ttGetMillySecondsCounts()))
            {
                emit errorOpenComPortForRemote2Event(0);
                lTimeOfLastSendEOCPFE2E=ttGetTimePlusDTime(ttGetMillySecondsCounts(),
                                                           period_test_ret*1000);
            }
        }else{

            if(lTimeOfLastSendEOCPE==0||
                    ttIsTimeCame(lTimeOfLastSendEOCPE,ttGetMillySecondsCounts()))
            {
                emit errorOpenComPortEvent(0);
                lTimeOfLastSendEOCPE=ttGetTimePlusDTime(ttGetMillySecondsCounts(),
                                                           period_test_ret*1000);
            }
        }
        return (-1);
    }
    if(twuFile_temp->iError)
    {
        if(type==1)
        {
            if(lTimeOfLastSendEOCPFE1E==0||
               ttIsTimeCame(lTimeOfLastSendEOCPFE1E,ttGetMillySecondsCounts()))
            {
                emit errorOpenComPortForRemote1Event(twuFile_temp->iError);
                lTimeOfLastSendEOCPFE1E=ttGetTimePlusDTime(ttGetMillySecondsCounts(),
                                                           period_test_ret*1000);
            }
        }else
        if(type==2)
        {
            if(lTimeOfLastSendEOCPFE2E==0||
               ttIsTimeCame(lTimeOfLastSendEOCPFE2E,ttGetMillySecondsCounts()))
            {
                emit errorOpenComPortForRemote2Event(twuFile_temp->iError);
                lTimeOfLastSendEOCPFE2E=ttGetTimePlusDTime(ttGetMillySecondsCounts(),
                                                           period_test_ret*1000);
            }
        }else{
            if(lTimeOfLastSendEOCPE==0||
                    ttIsTimeCame(lTimeOfLastSendEOCPE,ttGetMillySecondsCounts()))
            {
                emit errorOpenComPortEvent(twuFile_temp->iError);
                lTimeOfLastSendEOCPE=ttGetTimePlusDTime(ttGetMillySecondsCounts(),
                                                           period_test_ret*1000);
            }
        }
        twuCloseFile(twuFile_temp);
        return (-1);
    }
    twuPurgeFile(twuFile_temp,0xf);
    if(type==1)
    {
        twuFile_remote1=twuFile_temp;
    }else
    if(type==2)
    {
        twuFile_remote2=twuFile_temp;
    }else{
        twuFile=twuFile_temp;
    }
    return 1;
}

//Закрытие и повторное открытие порта
int readThread::closeAndOpenComPort(int type)
{
    if(type==1)
    {
        if(twuFile_remote1)
        {
            twuCloseFile(twuFile_remote1);
            twuFile_remote1=NULL;

        }
    }else
    if(type==2)
    {
        if(twuFile_remote2)
        {
            twuCloseFile(twuFile_remote2);
            twuFile_remote2=NULL;
        }
    }else{
        if(twuFile)
        {
            twuCloseFile(twuFile);
            twuFile=NULL;
        }
    }
    return testAndOpenComPort(type);
}

