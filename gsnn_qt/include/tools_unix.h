﻿#ifndef TOOLS_UNIX_H_INCLUDED
#define TOOLS_UNIX_H_INCLUDED


int kill_process(const char *proc_name);

int matv_pidof(const char *proc_name, int pids[], int max_n_of_pids);
int findProcessesOpenFile(const char *file_name, int *proc_id, int max_n_of_proc_id);
int findProcessesOpenFile(const char *file_name);
#endif // TOOLS_UNIX_H_INCLUDED
