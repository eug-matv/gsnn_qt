﻿#include <stdio.h>
#include "gsv.h"

int gsvRazborGSV(
		   const char *str_data,
		    struct GSV_DATA *gsv_data)
{
    int i=1,k=0,j=0;
    int iRet;
    int n;		  //
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
    
    if(str_data[0]!='$')return 0;
    while(str_data[i])
    {
	summ^=(unsigned char)(str_data[i]);
	if(str_data[i]==','||str_data[i]=='*')
	{
	    if(j==0)
	    {
		if(k!=5)
		{
		      return 0;
		}  
		if(str_data[i-k]!='G'||str_data[i-k+2]!='G'||str_data[i-k+3]!='S'||str_data[i-k+4]!='V')
		{
		      return 0;
		}  
		if(str_data[i-k+1]=='P')gsv_data->ns=0;
		else if(str_data[i-k+1]=='L')gsv_data->ns=1;
		else if(str_data[i-k+1]=='N')gsv_data->ns=2;
		else return 0;
	    }
	    else if(j==1)
	    { 
	      //Считаем число сообщений
		if(k!=0)
		{  
		    iRet=sscanf(str_data+(i-k),"%d",&(gsv_data->n));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		}  
	    }
	    else if(j==2)
	    {
	      //Считаем номер текущего сообщения 
		if(k!=0)
		{  
		    iRet=sscanf(str_data+(i-k),"%d",&(gsv_data->m));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		}  
	    }
	    else if(j==3)
	    {
	      //Считаем общее число НКА в зоне видимости
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%d",&(gsv_data->pp));
		      if(iRet!=1)return 0;
		      if(gsv_data->pp>4||gsv_data->pp<=0)return (-1);
		  }else{
		      return 0;
		  }  
	    }else if(j>=4&&j%4==0)
	    {
		   n=j/4-1;
		   if(n>=gsv_data->pp)return 0;
		   if(k>0)
		   {
		      iRet=sscanf(str_data+(i-k),"%d",&(gsv_data->kk[n]));
		      
		      if(iRet!=1||gsv_data->kk[n]<0)
		      {
			    return 0;
		      }	    
		   }else{
		      return 0;
		   }  
	    }else if(j>=4&&j%4==1)
	    {
		   n=j/4-1;
		   if(n>=gsv_data->pp)return 0;
		   if(k>0)
		   {
		      iRet=sscanf(str_data+(i-k),"%d",&(gsv_data->gg[n]));
		      if(iRet!=1)
		      {
			  return 0; 
		      }	    
		   }else{
		      return 0;
		   }
	    }else if(j>=4&&j%4==2)
	    {
		   n=j/4-1;
		   if(n>=gsv_data->pp)return 0;
		   if(k>0)
		   {
		      iRet=sscanf(str_data+(i-k),"%d",&(gsv_data->yyy[n]));
		      if(iRet!=1)
		      {
			  return 0; 
		      }	    
		   }else{
		      return 0;
		   }  
	    }else if(j>=4&&j%4==3)
	    {
		  n=j/4-1;
		   if(n>=gsv_data->pp)return 0;
		   if(k>0)
		   {
		      iRet=sscanf(str_data+(i-k),"%d",&(gsv_data->xx[n]));
		      if(iRet!=1)
		      {
			  return 0; 
		      }	    
		   }else{
		      return 0;
		   }
	    }  
	    j++;
	    k=0;
	}else{  
	    k++;
	}
	
	if(str_data[i]=='*')
	{
	    iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
	    if(iRet!=1)return 0;
	    if(((unsigned int)summ)!=recv_summ)
	    {
		return 0;
	    }
	    break;
	}  
	
	summ^=(unsigned char)(str_data[i]);
	i++;
    };  
    return 1;  
}
