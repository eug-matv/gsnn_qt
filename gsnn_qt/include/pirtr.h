﻿/*PIRTR - запрос на изменение параметров выдачи координат и времени*/
#ifndef 		__PIRTR_H__
#define 	__PIRTR_H__



struct PIRTR_HHMM
{
    int hh;		//„асы. ≈сли значение <0, то значение отрицательнок, если больше нул¤ то положительно
    unsigned mm;
};

struct PIRTR_DATA
{
  
  //»спользуема¤ система координат от 0  до 4   
  //≈сли <0 - то значение по умолчанию.
    union
    {
	     int a;
	     int system_of_koord;
    };
  
  //–азница UTC и по¤сного времени 
    struct PIRTR_HHMM sHHMM; 
};


#ifdef __cplusplus
extern "C"
{
#endif	
int pirtrMakePIRTR(const struct PIRTR_DATA *pirtr_data, 
		   char *str_data, int size_str_data);
#ifdef __cplusplus
}
#endif


#endif
