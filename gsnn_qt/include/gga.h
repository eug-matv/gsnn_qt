﻿

#ifndef __GGA_H__
#define __GGA_H__


#include "time_tools.h"

struct GGA_TIME_UTC
{
    int h;
    int m;
    double s;
};


struct GGA_LATITUDE
{
  int gr;		//градусы 
  double m;		//минуты
  char a;		//север/юг (N/S)
};


struct GGA_LONGITUDE
{
  int gr;		//градусы 
  double m;		//минуты
  char a;		//восток/запал (E/W)
};
struct GGA_DATA
{
        int ns;	//Тип навигационной системы. 0 - GPS, 1 - ГЛОНАС, 2 GPS+Глонас, 3 - получены из бинарного протокола
	struct GGA_TIME_UTC sHHMMSS_SS;		//1 время обсервации UTC
	struct GGA_LATITUDE sBBBB_BBBB_a;	//2 широта
	struct GGA_LONGITUDE sLLLL_LLLL_a;	//3 долгота
	int b;			//4 Показатель качества обсервации:
				  //0 - определение места не получено
				  //1 - обсервация получена в автономном режиме
				  //2 - обсервация получена в дифференциальном режиме
	int cc;			//5 Число НКА в решении
	double d_d;		//6 Величина горизонтального геометрического фактора
	double e_e;		//7 Высота над средним уровнем моря
    
	double f_f;		//8 превышение геода  над эллипсоидом WGS-84
	
	double g_g;		//9 возраст дифференциальных поправок
	int jjjj;		//10 Идентификатор  дифференциальной станции от 0000 до 1023
	
	
	struct TTDateTime dt[11];
};	

#ifdef __cplusplus
extern "C"
{
#endif	
int ggaRazborGGA(
		   const char *str_data,
		   const struct TTDateTime *dt,
		    struct GGA_DATA *gga_data);
			
int ggaMakeGGA(
			   const struct GGA_DATA *gga_data,
			   char *str_data,
			   int size_str_data
			  );					

int ggaGetDataGGA(
		const struct GGA_DATA *gga_data,   //Указатель 
		int indx,						   //Индекс	
		unsigned char *out_bytes,		   //Выходной байт	
		int max_sz_out_bytes,			   //размер массива	
		int *sz_out_bytes,				   //Размер данных в байтах
		struct TTDateTime *dt			   //Время получения пакета	 
				);				

#ifdef __cplusplus
}
#endif

#endif
