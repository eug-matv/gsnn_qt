﻿#include <stdio.h>
#include <QString>
#include <QDateTime>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "rmc.h"
#include "gga.h"


bool MainWindow::isEndSession=false;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
//0 делаем всё что нужно, для окна - меню и прочее
    ui->setupUi(this);
    createActions();
    createSystemTray();
    noRMCInfoOut();
    ui->lSputniksCount->setText(tr("Satellites: not known"));
    ui->lLastTimeOfSput->setText("");

//1 Создадим объект типа вторичный поток. Реализация класса globalThread в globalthread (.h, .cpp).
    glbl_thrd=new globalThread;

//Проверим состояние создания вторичного потока
    if(glbl_thrd->critical_error)
    {
        switch(glbl_thrd->critical_error)
        {
            case GLBL_THRD_CRT_ERR_NO_CONF:
                criticalErrorOut(tr("The configuration file is not found. It should in the same folder as the program"));
            break;

            case GLBL_THRD_CRT_ERR_NO_ACCESS_CONF:
                criticalErrorOut(tr("Cannot read the configuration file"));
            break;


            case GLBL_THRD_CRT_ERR_STOPBITS_CONF:
                criticalErrorOut(tr("Error in the configuration file: Stopbits !=0, 1, 2"));
            break;

            case GLBL_THRD_CRT_ERR_PROTOCOL_CONF:
                criticalErrorOut(tr("Error in configuration file: Protocol value not equal to 0, 1"));
            break;


            case GLBL_THRD_CRT_ERR_SYNCPERIOD_CONF:
                criticalErrorOut(tr("Error in configuration file: "
                 "(SyncPeriod <= 0) || (SyncPeriod > 600000)"));
            break;

            case GLBL_THRD_CRT_ERR_TIMECONSTANT_CONF:
                criticalErrorOut(tr("Error in configuration file:"
                 "TimeConstant <-7200000 || > 7200000"));
            break;




            case GLBL_THRD_CRT_ERR_TIME_ZONE_ERR:
                /* criticalErrorOut(
                    tr_debug("В системе ошибка работы с TimeZone.\n "
                       "Попробуйте настроить TimeZone или установить\n"
                       "параметры ifNoTimeZoneSetThis=1, а так же  Bias_min и DaylightsBias_min"
                       " в файле конфигурации"));
*/
                criticalErrorOut("TimeZone operation error in the system. Try to set TimeZone or specify ifNoTimeZoneSetThis=1 parameters, as well as Bias_min and DaylightsBias_min in the configuration file");
             break;

        };
        return ;
    }
   // addLineToTextBrowser(tr("Успешно создан основной вторичный поток"));

    hide();


    connect(glbl_thrd,SIGNAL(againDataEvent()),this,SLOT(againDataEventSlot()));
    connect(glbl_thrd, SIGNAL(againPackageEvent()),this,SLOT(againPackageEventSlot()));
    connect(glbl_thrd, SIGNAL(correctSetSystemTime()),this,SLOT(correctSetSystemTimeSlot()));
    connect(glbl_thrd,SIGNAL(curMidnightTime()),this,SLOT(curMidnightTimeSlot()));
    connect(glbl_thrd,SIGNAL(errorSetSystemTime()),this,SLOT(errorSetSystemTimeSlot()));
    connect(glbl_thrd, SIGNAL(noDataEvent(int)), this, SLOT(noDataEventSlot(int)));
    connect(glbl_thrd,SIGNAL(noDataGPSWithDateTime()),this,SLOT(noDataGPSWithDateTimeSlot()));
    connect(glbl_thrd, SIGNAL(noPackageEvent(int)),this,SLOT(noPackageEventSlot(int)));
    connect(glbl_thrd, SIGNAL(receivedData(int)),this,SLOT(receivedDataSlot(int)));
    connect(glbl_thrd,SIGNAL(messageAboutSetTime(QString)),this, SLOT(messageAboutSetTimeSlot(QString)));
    connect(glbl_thrd, SIGNAL(criticalError(QString)),this,SLOT(criticalErrorOut(QString)));
    connect(glbl_thrd, SIGNAL(errorEvent(QString)),this,SLOT(errorEventSlot(QString)));

    glbl_thrd->start(); //Запустим вторичный поток
}

MainWindow::~MainWindow()
{
    if(glbl_thrd)
    {
        glbl_thrd->destroy();
    }
    delete ui;
}

 void MainWindow::createActions()
 {
     aShow=new QAction(tr("Show Window"),this);
     connect(aShow,SIGNAL(triggered()),this,SLOT(show()));

     aHide=new QAction(tr("Hide Window"),this);
     connect(aHide,SIGNAL(triggered()),this,SLOT(hide()));

     aExit=new QAction(tr("Exit"),this);
     connect(aExit,SIGNAL(triggered()),this,SLOT(endSession()));



 }


  void MainWindow::createSystemTray()
  {
      mSystemTrayMenu=new QMenu(this);
      mSystemTrayMenu->addAction(aShow);
      mSystemTrayMenu->addAction(aHide);
      mSystemTrayMenu->addSeparator();
      mSystemTrayMenu->addAction(aExit);


      sti=new QSystemTrayIcon(QIcon(":img/icon_sat.bmp"),this);
      sti->setContextMenu(mSystemTrayMenu);


      connect(sti,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this,SLOT(activateWindowByMouseClick(QSystemTrayIcon::ActivationReason)));

      sti->show();
  }


 void MainWindow::closeEvent(QCloseEvent *event)
 {

     if(!isEndSession)
     {
        event->ignore();
        hide();
     }
 }


 void MainWindow::addLineToTextBrowser(const QString &line)
 {
     QString line1;


     line1=QDateTime::currentDateTime().toString(tr("hh:mm:ss"))+
                             tr(" ")+line;

     ui->textBrowser->setText(line1);

 }


 void MainWindow::activateWindowByMouseClick(QSystemTrayIcon::ActivationReason reason)
 {
     if(reason==QSystemTrayIcon::DoubleClick)
     {
         show();
     }
 }



 void MainWindow::noRMCInfoOut()
 {
     QLabel *tm_lbls[10];
     tm_lbls[0]=ui->lLastTime;  tm_lbls[1]=ui->lLastTime1; tm_lbls[2]=ui->lLastTime2;
     tm_lbls[3]=ui->lLastTime3; tm_lbls[4]=ui->lLastTime4; tm_lbls[5]=ui->lLastTime5;
     tm_lbls[6]=ui->lLastTime6;  tm_lbls[7]=ui->lLastTime7; tm_lbls[8]=ui->lLastTime8;
     tm_lbls[9]=ui->lLastTime9;

          ui->lTypeOfSys->setText(tr("System type: unknown"));
          ui->lTimeUTC->setText(tr("UTC time: unknown"));
          ui->lStatus->setText(tr("Status: unknown"));
          ui->lLatitude->setText(tr("Latitude: unknown"));
          ui->lLongitude->setText(tr("Longitude: unknown"));
          ui->lSpeed->setText(tr("Ground speed: unknown"));
          ui->lCourse->setText(tr("Ground heading: unknown"));
          ui->lDate->setText(tr("Date: unknown"));
          ui->lMagnOtklon->setText(tr("Magnetic deflection: unknown"));
          ui->lRejimMest->setText(tr("Place detection mode: unknown"));

          for(int i=0;i<10;i++)
          {
              tm_lbls[i]->setText(tr(""));
          }

 }



 /*Обработка событий по GPS или ГЛОНАСС*/
 void MainWindow::noDataEventSlot(int iPeriod_sec) //Не было данных в течении определенного периода
 {
     ui->textBrowser->setTextColor(QColor(0,0,0));
     addLineToTextBrowser(tr("The data is not received within ")+
                              QString::number(iPeriod_sec)+tr(" seconds"));

     noRMCInfoOut();
     ui->lSputniksCount->setText(tr("Satellites: not known"));
     ui->lLastTimeOfSput->setText(" ");

     show();
 }


 void MainWindow::againDataEventSlot()             //Данные снова стали поступать
 {
     ui->textBrowser->setTextColor(QColor(0,0,0));
     addLineToTextBrowser(tr("Data began to flow again"));
     show();
 }


 void MainWindow::noPackageEventSlot(int iPeriod_sec)
 {
     ui->textBrowser->setTextColor(QColor(0,0,0));
     addLineToTextBrowser(tr("Was gps/GLONASS data for ")+
                              QString::number(iPeriod_sec)+tr(" seconds"));

     noRMCInfoOut();
     ui->lSputniksCount->setText(tr("Satellites: not known"));
     ui->lLastTimeOfSput->setText(" ");


     show();
 }

 void MainWindow::againPackageEventSlot()
 {
     ui->textBrowser->setTextColor(QColor(0,0,0));
     addLineToTextBrowser(tr("Data gps/GLONASS began to come again"));
     show();
 }

 void MainWindow::receivedDataSlot(int type)            //распознан и получен пакет данных
                                    //В качестве параметра передается.
                                    //Если тип пакета IEC, то значение
                                    //из файла iec61161_1tools.h
                                    //Если кадр 3000, то число 3000
 {
     int iRet,i;
     struct GGA_DATA gga_data;
     struct RMC_DATA rmc_data;
     char tmp_str[200];

     QLabel *tm_lbls[10];
     tm_lbls[0]=ui->lLastTime;  tm_lbls[1]=ui->lLastTime1; tm_lbls[2]=ui->lLastTime2;
     tm_lbls[3]=ui->lLastTime3; tm_lbls[4]=ui->lLastTime4; tm_lbls[5]=ui->lLastTime5;
     tm_lbls[6]=ui->lLastTime6;  tm_lbls[7]=ui->lLastTime7; tm_lbls[8]=ui->lLastTime8;
     tm_lbls[9]=ui->lLastTime9;





    iRet=glbl_thrd->getRMC_DATA(rmc_data);
     if(iRet>0)
     {
//0 тип
         if(rmc_data.ns==0)
         {
             ui->lTypeOfSys->setText(tr("System type: GPS"));
         }else
         if(rmc_data.ns==1)
         {
             ui->lTypeOfSys->setText(tr("System type: GLONASS"));
         }else
         if(rmc_data.ns==2)
         {
             ui->lTypeOfSys->setText(tr("System type: GPS+GLONASS"));
         }else{
             ui->lTypeOfSys->setText(tr("System type: error!"));
         }

//1 время UTC
         sprintf(tmp_str, "UTC time: %02d:%02d:%02d",
                    rmc_data.sHHMMSS_SS.h,
                    rmc_data.sHHMMSS_SS.m,
                 (int)(rmc_data.sHHMMSS_SS.s));
         ui->lTimeUTC->setText(tr(tmp_str));

//2 статус
         if(rmc_data.A=='V')
         {
             ui->lStatus->setText(tr("Status: the solution is not valid"));
         }else
         if(rmc_data.A=='A')
         {
             ui->lStatus->setText(tr("Status: autonomous mode"));
         }else
         if(rmc_data.A=='D')
         {
             ui->lStatus->setText(tr("Status: differential mode"));
         }else{
             ui->lStatus->setText(tr("Status: unknown"));
         }

//3 Широта
         sprintf(tmp_str, "Latitude:  %03d° %03.2lf\' %c",
                rmc_data.sBBBB_BBBB_a.gr,
                rmc_data.sBBBB_BBBB_a.m,
                rmc_data.sBBBB_BBBB_a.a);
         ui->lLatitude->setText(tr(tmp_str));

//4 долгота
         sprintf(tmp_str, "Longitude:  %03d° %03.2lf\' %c",
                rmc_data.sLLLL_LLLL_a.gr,
                rmc_data.sLLLL_LLLL_a.m,
                rmc_data.sLLLL_LLLL_a.a);
         ui->lLongitude->setText(tr(tmp_str));

//5  наземная скорость в узлах
         sprintf(tmp_str,"Ground speed: %4.2lf",
                 rmc_data.v_v);
         ui->lSpeed->setText(tr(tmp_str));

//6 наземный курс в градусах
         sprintf(tmp_str, "Ground heading:  %3.2lf",
                 rmc_data.z_z);
         ui->lCourse->setText(tr(tmp_str));

//7 дата
         sprintf(tmp_str, "Date: %04d.%02d.%02d",
                 rmc_data.sDDMMYY.yy,rmc_data.sDDMMYY.mm, rmc_data.sDDMMYY.dd);
        ui->lDate->setText(tr(tmp_str));

//8 магнитное отклонение в гр
        sprintf(tmp_str, "Magnetic deflection: %3.2lf %c",
                rmc_data.x_x_a.x_x, rmc_data.x_x_a.a);
        ui->lMagnOtklon->setText(tr(tmp_str));


//9 режим местоопределения
        if(rmc_data.b=='A')
        {
            ui->lRejimMest->setText(tr("Place detection mode:  autonomous"));
        }else
        if(rmc_data.b=='D')
        {
            ui->lRejimMest->setText(tr("Place detection mode:  differential"));
        }else
        if(rmc_data.b=='E')
        {
            ui->lRejimMest->setText(tr("Place detection mode:  expected"));
        }else
        if(rmc_data.b=='M')
        {
            ui->lRejimMest->setText(tr("Place detection mode:  manual input"));
        }else
        if(rmc_data.b=='S')
        {
            ui->lRejimMest->setText(tr("Place detection mode:  imitation"));
        }else
        if(rmc_data.b=='N')
        {
            ui->lRejimMest->setText(tr("Place detection mode:   not valid"));
        }else{
            ui->lRejimMest->setText(tr("Place detection mode:  unknown"));
        }

        for(i=0;i<10;i++)
        {
            sprintf(tmp_str, "%04d.%02d.%02d  %02d:%02d:%02d",
                    rmc_data.dt[i].yyyy,  rmc_data.dt[i].mm, rmc_data.dt[i].dd,
                    rmc_data.dt[i].h, rmc_data.dt[i].m,(int)(rmc_data.dt[i].s));
            tm_lbls[i]->setText(tr(tmp_str));
        }
     }else{
         noRMCInfoOut();
     }

     iRet=glbl_thrd->getGGA_DATA(gga_data);
     if(iRet>0)
     {
         ui->lSputniksCount->setText(tr("Satellites: ")+QString::number(gga_data.cc));
         sprintf(tmp_str, "%04d.%02d.%02d  %02d:%02d:%02d",
                 gga_data.dt[5].yyyy, gga_data.dt[5].mm, gga_data.dt[5].dd,
                 gga_data.dt[5].h, gga_data.dt[5].m,(int)(gga_data.dt[5].s));
         ui->lLastTimeOfSput->setText(tmp_str);
     }else{
         ui->lSputniksCount->setText(tr("Satellites: not known"));
         ui->lLastTimeOfSput->setText(" ");
     }

 }



  void MainWindow::noDataGPSWithDateTimeSlot()  //Нет информации со временем и даты
  {
      ui->textBrowser->setTextColor(QColor(0,0,0));
      addLineToTextBrowser(tr("There is no information about time and date"));
      noRMCInfoOut();

  }


  void MainWindow::errorSetSystemTimeSlot()
  {
      ui->textBrowser->setTextColor(QColor(0,0,0));
      addLineToTextBrowser(tr("Error setting system time"));
      show();
  }


  void MainWindow::curMidnightTimeSlot()
  {
      ui->textBrowser->setTextColor(QColor(0,0,0));
      addLineToTextBrowser(tr("Time near midnight - do nothing"));
  }


  void MainWindow::correctSetSystemTimeSlot()
  {
      ui->textBrowser->setTextColor(QColor(0,0,0));
      addLineToTextBrowser(tr("System time is set successfully"));
  }

  void MainWindow::messageAboutSetTimeSlot(const QString &str)
  {
      QString line1;
      ui->textBrowser->setTextColor(QColor(0,0,0));


      line1=QDateTime::currentDateTime().toString(tr("hh:mm:ss"))+
                              tr(" ")+str;

      ui->textBrowser->append(line1);


  }


  void MainWindow::criticalErrorOut(const QString &str)
  {
        ui->textBrowser->clear();
        ui->textBrowser->setTextColor(QColor(255,0,0));
        addLineToTextBrowser(tr("Critical error!"));
        ui->textBrowser->append(tr("Description of the error: "));
        ui->textBrowser->append(str);

//Завершим работу вторичного потока
        glbl_thrd->destroy();
        glbl_thrd=NULL;

//Обязательно покажем окно
        show();

  }


void MainWindow::errorEventSlot(const QString &str)
{  
      ui->textBrowser->setTextColor(QColor(0,0,0));
      addLineToTextBrowser(str);
      show();
}


void MainWindow::endSession()          //Завершение работы сессии
{
    isEndSession=true;
    close();
}
