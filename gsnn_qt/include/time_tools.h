﻿/*Автор Матвеенко Е.А.

Обеспечивает работу со временем.

*/

#ifndef  __TIME_TOOLS_H__
#define __TIME_TOOLS_H__

struct TTDateTime
{
       int yyyy;                 //Год
       int mm;                   //Месяц от 1 до 12
       int dd;                   //День от 1 до 31 (30, 29, 28)
       int h;                    //Часы от 0 до 23
       int m;                    //Минуты от 0 до 59
       double s;                 //Секунды от 0.0000 до 59.99999999
};


#ifdef __cplusplus
extern "C"
{
#endif
/*Получить локальное время в формате TTDateTime.
Возвращает:
1, если всё нормально,
<0 - если ошибка*/
int ttGetLocalTime(struct TTDateTime *dt);


/*Получить по входящему данному в формате struct TTDateTime
значение времени в строковом виде: HH:MM:SS.SSS.
Возвращает:
1, если всё нормально,
0 - если ошибка, например если sz_str<14*/
int ttGetTimeInStr(const struct TTDateTime *dt,   //Время в формате TTDateTime
					char *str, //Строка которая должна заполниться
                                        int sz_str); //Размер строки

/*Получить по входящему данному в формате struct TTDateTime
значение даты в строковом виде: DD.MM.YYYY.
Возвращает:
1, если всё нормально,
0 - если ошибка, например если sz_str<12*/
int ttGetDateInStr(const struct TTDateTime *dt,
                   char *str,
                   int sz_str);

/*Получить по входящему данному в формате struct TTDateTime
значение даты и времени в строковом виде: DD.MM.YYYY   hh:mm:ss.sss.
Возвращает:
1, если всё нормально,
0 - если ошибка, например если sz_str<29*/
int ttGetDateTimeInStr(const struct TTDateTime *dt,
					char *str, int sz_str);


#ifdef __cplusplus
}
#endif


#endif
