﻿#include <stdio.h>
#include <string.h>
#include "pirpr.h"




/*

Если успешно, то возвращает полное число байт включая стартовый и два конечных <CR>  и <LF>.
0 - если ошибка в данных и пакет не получен,
и -1 -- если не хватило по длине size_data<, чем реальная длина пакетов
*/
int pirprMakePIRPR(const struct PIRPR_DATA *pirpr_data, 
		   char *str_data, int size_str_data)
{
    int nc=0;
    int i;
    unsigned int u_mask=0;
    unsigned char ctr_summ=0;
    if(nc+2>size_str_data)return (-1);
    str_data[nc]='$';
    nc+=1;
//Вывести PIRPR 
    if(nc+6>size_str_data)return (-1);
    memcpy(str_data+nc,"PIRPR,", 6);
    nc+=6;
    
//Вывести номер порта
    if(pirpr_data->x<0)
    {
	if(nc+1>size_str_data)return (-1);
	str_data[nc]=',';
	nc++;
    }else{
	if(nc+2>size_str_data)return (-1);
	sprintf(str_data+nc, "%d", (pirpr_data->x)%2);
	str_data[nc+1]=',';
	nc+=2;
    }  
    
 //Вывести скорость обмена
    if(pirpr_data->b<=0)
    {
	 if(nc+1>size_str_data)return (-1);
	 str_data[nc]=',';
	 nc++;
    }else{
	 if(nc+7>size_str_data)return (-1);
	 sprintf(str_data+nc, "%d", pirpr_data->b);
	 nc+=strlen(str_data+nc);
	 str_data[nc]=',';
	 nc++;
    }  
    
  //Вывести тип протокола
    if(pirpr_data->n<0||pirpr_data->n>4)return 0;
    if(nc+2>size_str_data)return (-1);
    sprintf(str_data+nc,"%d,", pirpr_data->n);
    nc+=2;
  
 //Подготовить и вывести битовую маску разрешенных  протоколов
    if(nc+9>size_str_data)return (-1);
    for(i=0;i<12;i++)
    {
	if(pirpr_data->mmmm[i])u_mask|=1<<i;
    }  
    sprintf(str_data+nc,"%04X*",u_mask);
    nc+=4;
    
 //Теперь расчитаем контрольную сумму   
    for(i=1;i<nc;i++)
    {
	ctr_summ^=(unsigned char)(str_data[i]);
    }
    nc++;
    sprintf(str_data+nc, "%02X",ctr_summ);
    nc+=2;
    str_data[nc]=0x0D;
    str_data[nc+1]=0x0A;
    nc+=2;
    return nc;
}
