﻿#include <QtCore>
#include <QEvent>
#include <QTextStream>
#include <QApplication>

// API для установки времени
#ifdef _WIN32
#include <windows.h>
#include <winbase.h>
#include <TCHAR.H>
#include <conio.h>
#else
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif

#include <string.h>
#include <stdio.h>

#include "globalthread.h"


// работа с программным ядром NMEA/MNP-b [Матвеенко Е.А.]

#include "rmc.h"
#include "time_tools.h"
#include "iec_lat_long.h"

// работа с лог-файлами [Рацебуржинский С.Л.]

  #include "protocol.h"

// работа с Юлианской датой [Меньшенин С.С.]
#include "defs.h"
#include "TimeMath.h"

#include "iec61161_1tools.h"
#include "time_count_tools.h"

#include "trans_file.h"
//====================================================
// Конструктор потока управления
globalThread::globalThread()//:QThread()
{

    QString sLogDir, sTmpLogDir, sLogFileName, sTmpLogFileName;

    int nom_log_protocol=0;
    ReadThread=NULL;
    tcp_socket=NULL;
//Сделаем владельца объектом globalThread сам себя. Это для того, чтобы обрабатывать
//ловушки сигнлов в себе
    QObject::moveToThread(this);
    //---------------------------------------------
    // Функциональная часть программы
    //---------------------------------------------

    /* Log file */ //! WinAPI

    char CurrentDirectory[1024];

    char strg[1000],str_protocol[100];


    QString configfile = QCoreApplication::applicationDirPath() + QString("/gsnn_config.ini");

    QFile config(configfile);



    // рабочий каталог
    memset(CurrentDirectory,0,sizeof(CurrentDirectory));

#ifdef _WIN32
    GetCurrentDirectory(sizeof(CurrentDirectory)-1, CurrentDirectory);
#else
    getcwd(CurrentDirectory, sizeof(CurrentDirectory)-1);
#endif
    if (!(config.exists())) //Проверим существование конфигурационного файла
    {
//Если файла не сушествует надо в текущую программу внести данные
        sLogDir=tr(CurrentDirectory);
        sTmpLogDir=tr(CurrentDirectory);
        sLogFileName=tr("gsnn_qt.log");
        sTmpLogFileName=tr("$gsnn_qt$.log");
    }else{
        QSettings m_settings(configfile, QSettings::IniFormat);
        //m_settings.setIniCodec(QTextCodec::codecForName("windows-1251"));


            m_settings.beginGroup("/LOG");
                sLogDir = m_settings.value("/dirname", tr(CurrentDirectory)).toString();
                sTmpLogDir = m_settings.value("/tmpdirname", tr(CurrentDirectory)).toString();
                nom_log_protocol=m_settings.value("/nomprotocol",tr("0")).toInt(0);
                sLogFileName=m_settings.value("/protocolname", tr("gsnn_qt.log")).toString();
                sTmpLogFileName=
                        m_settings.value("/tmpprotocolname",tr("$gsnn_qt$.log")).toString();
           m_settings.endGroup();
           if(sLogDir==tr("."))
           {
               sLogDir=tr(CurrentDirectory);
           }
           if(sTmpLogDir==tr("."))
           {
               sTmpLogDir=tr(CurrentDirectory);
           }

    }




//Создадим файл протокола
    logProtocol = new TProtocol();
    logProtocol->Init(  sLogDir.toLatin1().data(),
                        sTmpLogDir.toLatin1().data(),
                        nom_log_protocol,
                        sLogFileName.toLatin1().data(),
                        sTmpLogFileName.toLatin1().data(),
                        (prtcl_char *)(tr("Getting started program gsnn_qt").toUtf8().data()),
                        (prtcl_char *)(tr("The end of the program gsnn_qt").toUtf8().data()));



    //! необходимость временной константы обуславливается отставанием пакета данных
    //! от секундной метки, выдаваемой приемником
    //! Приемник МНП7 выдает пакеты данных, конец которых привязан к временной метке.

    //JD_RMCtimemark = 42.0; // это чтоб не вылетело в первом цикле
    //nodata_counter = 0;    // при проверке ошибок



    //--------------------------------------------------------------------------

    //! Здесь считываем файл конфигурации
    //! QSettings может использовать ини файл для хранения настроек


    //! Если файл не существует - то не надо его создавать --- надо ругануться и выйти
    if (!(config.exists()))
        {        
        logProtocol->WriteInfo((prtcl_char *)(tr("The working directory is not detected settings file for the program. ").toUtf8().data())
                               );
        config.close();
        critical_error=GLBL_THRD_CRT_ERR_NO_CONF;
        return;
    }else{
        readSettings();
        if(critical_error<0)
        {
            return;
        }
    }





//Созданим объект вторичного потока чтения данных с порта
    ReadThread = new readThread;


#ifdef _WIN32
        Sleep(2000);
#else
        sleep(2);
#endif

        connect(ReadThread, SIGNAL(noDataEvent()),this,SLOT(noDataEventSlot()));
        connect(ReadThread, SIGNAL(againDataEvent()),this,SLOT(againDataEventSlot()));
        connect(ReadThread, SIGNAL(noPackageEvent()),this,SLOT(noPackageEventSlot()));
        connect(ReadThread, SIGNAL(againPackageEvent()),this,SLOT(againPackageEventSlot()));
        connect(ReadThread, SIGNAL(receivedData(int)),this,SLOT(receivedDataSlot(int)));
        connect(ReadThread, SIGNAL(noDataEventForOther()),this,SLOT(noDataEventForOtherSlot()));
        connect(ReadThread, SIGNAL(noPackageEventForOther()),this,SLOT(noPackageEventForOtherSlot()));
        connect(ReadThread, SIGNAL(errorOpenComPortEvent(int)),this,SLOT(errorOpenComPortSlot(int)));
        connect(ReadThread, SIGNAL(errorOpenComPortForRemote1Event(int)),this, SLOT(errorOpenComPortForRemote1Slot(int)));
        connect(ReadThread, SIGNAL(errorOpenComPortForRemote2Event(int)),this, SLOT(errorOpenComPortForRemote2Slot(int)));

//Установка опций
        logProtocol->WriteInfo((prtcl_char *)(tr("Setting options for reading GPS data from serial port\n").toUtf8().data()));

        ReadThread->setOptions(App_set.Protocol, App_set.PortName,
                              App_set.BaudeRate,
                                  App_set.StopBits,
                                  App_set.iTimeForTestNoData,
                                  App_set.is_remote1,
                                  App_set.cPortName_remote1,
                                  App_set.iBaudeRate_remote1,
                                  App_set.iStopBits_remote1,
                                  App_set.is_remote2,
                                  App_set.cPortName_remote2,
                                  App_set.iBaudeRate_remote2,
                                  App_set.iStopBits_remote2);




         logProtocol->WriteInfo((prtcl_char *)(tr("Setting options for reading the GPS data from the serial port is successful\n").toUtf8().data()));



    //---------------------------------------------

         if(App_set.Protocol)
         {
             strcpy(str_protocol,"MNP-binary");
         }else{
             strcpy(str_protocol,"IEC 61162-1 (NMEA-0183)");
         }



     // Выдаем информацию, что приложение успешно запущенно - перечисляем настройки
         logProtocol->WriteInfo((prtcl_char *)"\n");
         logProtocol->WriteInfo((prtcl_char *)tr("The application runs successfully with the settings:\n").toUtf8().data());
#ifdef QT_RUS
         sprintf(strg, "- Параметры порта:\n"
                        "    PortName:   %s\n"
                        "    BaudeRate:  %d\n"
                        "    StopBits:   %d\n"
                        "    Protocol:   %s\n"
                        "- Параметры синхронизации:\n"
                        "    SyncPeriod:    %d секунд\n"
                        "    TimeConstant:  %d миллисекунд\n"
                        "    TimeForTestNoData_sec:  %d секунд\n"
                        " Параметры для связи с удаленным узлом 1: \n"
                         " IsSendingData:   %d\n"
                         "PortName_remote: %s\n"
                          "BaudeRate_remote:    %d\n"
                         "StopBits_remote:  %d\n"
                         " Параметры для связи с удаленным узлом 2: \n"
                         " IsSendingData:   %d\n"
                         "PortName_remote: %s\n"
                         "BaudeRate_remote:    %d\n"
                         "StopBits_remote:  %d\n",
                        App_set.PortName, App_set.BaudeRate,
                        App_set.StopBits, str_protocol,
                        App_set.SyncPeriod, App_set.TimeConstant,
                        App_set.iTimeForTestNoData,
                        App_set.is_remote1,
                        App_set.cPortName_remote1,
                        App_set.iBaudeRate_remote1,
                        App_set.iStopBits_remote1,
                        App_set.is_remote2,
                        App_set.cPortName_remote2,
                        App_set.iBaudeRate_remote2,
                        App_set.iStopBits_remote2
                 );
#else
         sprintf(strg, "the port settings:\n"
                        "    PortName:   %s\n"
                        "    BaudeRate:  %d\n"
                        "    StopBits:   %d\n"
                        "    Protocol:   %s\n"
                        "the sync options:\n"
                        "    SyncPeriod:    %d seconds\n"
                        "    TimeConstant:  %d milliseconds\n"
                        "    TimeForTestNoData_sec:  %d seconds\n"
                        " The parameters for communication with the remote host № 1: \n"
                         " IsSendingData:   %d\n"
                         "PortName_remote: %s\n"
                          "BaudeRate_remote:    %d\n"
                         "StopBits_remote:  %d\n"
                         " The parameters for communication with the remote host № 2: \n"
                         " IsSendingData:   %d\n"
                         "PortName_remote: %s\n"
                         "BaudeRate_remote:    %d\n"
                         "StopBits_remote:  %d\n",
                        App_set.PortName, App_set.BaudeRate,
                        App_set.StopBits, str_protocol,
                        App_set.SyncPeriod, App_set.TimeConstant,
                        App_set.iTimeForTestNoData,
                        App_set.is_remote1,
                        App_set.cPortName_remote1,
                        App_set.iBaudeRate_remote1,
                        App_set.iStopBits_remote1,
                        App_set.is_remote2,
                        App_set.cPortName_remote2,
                        App_set.iBaudeRate_remote2,
                        App_set.iStopBits_remote2
                 );
#endif
         logProtocol->WriteInfo((prtcl_char *)strg);



        //iTimer->start();

        critical_error = 0;


    isWasRMC_DATA=0;
    memset(&rmc_data,0,sizeof(rmc_data));
    isWasGGA_DATA=0;
    memset(&gga_data,0,sizeof(gga_data));


    isWasCorrectRMC_DATA=0;
    memset(&correct_rmc_data,0,sizeof(correct_rmc_data));

    iWasErrorTCPConnect=0;


} // конец конструктора

//====================================================
// деструктор
globalThread::~globalThread()
{


    stop();

    delete logProtocol; // удаляем объект/поток протокола, поскольку он не Qt-шный

    if(ReadThread)
    {
        delete ReadThread;      // удаляем объект/поток чтения
    }

    if(tcp_socket)
    {

        delete tcp_socket;
        tcp_socket=0;
    }

}
//====================================================
// Уничтожить поток
void globalThread::destroy()
{
    if(ReadThread)
    {
       ReadThread->stop();
    }
    exit();
    wait();
    delete this;
}

//====================================================
// остановка работы потока
void globalThread::stop()
{
    logProtocol->WriteInfo((prtcl_char *)tr("The main thread is stopped \n").toUtf8().data());

}

//====================================================
// основной цикл работы потока
void globalThread::run()
{
    int iRet;

    char strg[1000],str_protocol[100];

    if(App_set.Protocol)
    {
        strcpy(str_protocol,"MNP-binary");
    }else{
        strcpy(str_protocol,"IEC 61162-1 (NMEA-0183)");
    }


    if(App_set.local_port_event>=1&&App_set.local_port_event<(1<<16))
    {
        tcp_socket=new QTcpSocket;
        connect(tcp_socket,SIGNAL(readyRead()),this,SLOT(readDataFromTCP()));
    }else{
        tcp_socket=NULL;
    }


// Выдаем информацию, что приложение успешно запущенно - перечисляем настройки
    logProtocol->WriteInfo((prtcl_char *)"\n");
    logProtocol->WriteInfo((prtcl_char *)tr("The application runs successfully with the settings:\n").toUtf8().data());
#ifdef QT_RUS
    sprintf(strg, "- Параметры порта:\n"
                     "    PortName:   %s\n"
                     "    BaudeRate:  %d\n"
                     "    StopBits:   %d\n"
                     "    Protocol:   %s\n"
                     "- Параметры синхронизации:\n"
                     "    SyncPeriod:    %d секунд\n"
                     "    TimeConstant:  %d миллисекунд\n"
                     "    TimeForTestNoData_sec:  %d секунд\n"
                     " Параметры для связи с удаленным узлом 1: \n"
                      " IsSendingData:   %d\n"
                      "PortName_remote: %s\n"
                       "BaudeRate_remote:    %d\n"
                      "StopBits_remote:  %d\n"
                      " Параметры для связи с удаленным узлом 2: \n"
                      " IsSendingData:   %d\n"
                      "PortName_remote: %s\n"
                      "BaudeRate_remote:    %d\n"
                      "StopBits_remote:  %d\n",
                     App_set.PortName, App_set.BaudeRate,
                     App_set.StopBits, str_protocol,
                     App_set.SyncPeriod, App_set.TimeConstant,
                      App_set.iTimeForTestNoData,
                     App_set.is_remote1,
                     App_set.cPortName_remote1,
                     App_set.iBaudeRate_remote1,
                     App_set.iStopBits_remote1,
                     App_set.is_remote2,
                     App_set.cPortName_remote2,
                     App_set.iBaudeRate_remote2,
                     App_set.iStopBits_remote2
              );
#else
    sprintf(strg, "the port settings:\n"
                   "    PortName:   %s\n"
                   "    BaudeRate:  %d\n"
                   "    StopBits:   %d\n"
                   "    Protocol:   %s\n"
                   "the sync options:\n"
                   "    SyncPeriod:    %d seconds\n"
                   "    TimeConstant:  %d milliseconds\n"
                   "    TimeForTestNoData_sec:  %d seconds\n"
                   " The parameters for communication with the remote host № 1: \n"
                    " IsSendingData:   %d\n"
                    "PortName_remote: %s\n"
                     "BaudeRate_remote:    %d\n"
                    "StopBits_remote:  %d\n"
                    " The parameters for communication with the remote host № 2: \n"
                    " IsSendingData:   %d\n"
                    "PortName_remote: %s\n"
                    "BaudeRate_remote:    %d\n"
                    "StopBits_remote:  %d\n",
                   App_set.PortName, App_set.BaudeRate,
                   App_set.StopBits, str_protocol,
                   App_set.SyncPeriod, App_set.TimeConstant,
                   App_set.iTimeForTestNoData,
                   App_set.is_remote1,
                   App_set.cPortName_remote1,
                   App_set.iBaudeRate_remote1,
                   App_set.iStopBits_remote1,
                   App_set.is_remote2,
                   App_set.cPortName_remote2,
                   App_set.iBaudeRate_remote2,
                   App_set.iStopBits_remote2
            );
#endif
    logProtocol->WriteInfo((prtcl_char *)strg);



   /* запускаем вторичный поток чтения данных из порта*/
    logProtocol->WriteInfo((prtcl_char *)tr("Launching a secondary thread to read information from the serial port\n").toUtf8().data());
    iRet = ReadThread->start();
    if (iRet < 1)
    {
             /* Сообщение о критической ошибке */
//Занести надо в лог файл и вывалиться с треском
              logProtocol->WriteInfo((prtcl_char *)tr("Error starting the secondary thread read the data: \n").toUtf8().data());
              if(iRet==0)
              {
                  logProtocol->WriteInfo((prtcl_char *)tr("Secondary thread to read data from the port is already running\n").toUtf8().data());
                  emit criticalError(tr("Secondary thread to read data from the port is already running"));

              }
//Надо выйти
              return;
    }else{
        logProtocol->WriteInfo((prtcl_char *)tr("Launching a secondary thread to read information has been completed successfully\n").toUtf8().data());
    }

     syncTimer = new QTimer;       // таймер синхронизации времени

     syncTimer->setInterval(App_set.SyncPeriod*1000);

     QObject::connect(syncTimer, SIGNAL(timeout()), this, SLOT(syncTimeout()));

     syncTimer->start();


     syncTimerForSend = new QTimer;   //Для периодической поссылки информации на удаленные узлы

     syncTimerForSend->setInterval(500);

     QObject::connect(syncTimerForSend, SIGNAL(timeout()),this, SLOT(syncTimeoutForSendingToRemote()));

     syncTimerForSend->start();
//Переходим в режим опроса событий
     exec();


     syncTimerForSend->stop();
     syncTimer->stop();
     ReadThread->stop();


     delete syncTimerForSend;
     delete syncTimer;
}


//====================================================
// действия по срабатыванию таймера синхронизации
void globalThread::syncTimeout()
{
    // Алгоритм под Юл. дату
    /*
    1. Берем последние данные структуры rmc_data
       - Преобразуем в TTDateTime поля sDDMMYY и sHHMMSS_SS
         т.о. получаем последне зафиксированное время UTC
       - Преобразуем в TTDateTime время и дату фиксации полей sDDMMYY и sHHMMSS_SS
         соответственно  rmc_data.dt[7] и rmc_data.dt[1]
         т.о. получаем время фиксации времени UTC по текущему локальному времени машины
    2. Вычитаем из локального времени данные о часовом поясе и переход на сезонное время
    Для этого:
       - Преобразуем и то и другое в полную Юлианскую дату и вычитаем
    3. Считаем ошибку разсинхронизации полученного системного времени фиксации пакета
    4. Узнаем текущее системное время и вычитаем из него ошибку
    5. Устанавливаем системное время с учетом ошибки

    Примечание: все математические операции над структурами времени/даты п
    проводить только переведя в формат Юлианской даты
    */

    long double JD_UTC_rmc      = 0;
    long double JD_LocalFix_rmc = 0;
    long double JD_TimeZone     = 0;
    long double JD_TimeError    = 0;
    long double JD_SystemTime   = 0;
    long double JD_TimeConstant = 0;


    emit messageAboutSetTime(tr("the input to the function SyncTimeout()"));

    ReadThread->stop(); //Остановим поток чтения данных

    syncMutex.lock();

    if(!isWasCorrectRMC_DATA)
    {
//Не было корректного времени за последний момент времени
        syncMutex.unlock();
        emit noDataGPSWithDateTime();
        emit messageAboutSetTime(tr("There is no accurate data on time"));
        ReadThread->start();
        return;
    }



    TTDateTime *UTC_rmc      = new TTDateTime;  //Время UTC полученное от приемника GPS/ГЛОНАСС в последнем пакете типа RMC
    TTDateTime *LocalFix_rmc = new TTDateTime;  //Локальное получения последнего пакета RMC
    TTDateTime *CurentUTC    = new TTDateTime;  //Текущее время в формате UTC

    UTC_rmc->dd     = correct_rmc_data.sDDMMYY.dd;
    UTC_rmc->mm     = correct_rmc_data.sDDMMYY.mm;
    UTC_rmc->yyyy   = correct_rmc_data.sDDMMYY.yy;
    UTC_rmc->h      = correct_rmc_data.sHHMMSS_SS.h;
    UTC_rmc->m      = correct_rmc_data.sHHMMSS_SS.m;
    UTC_rmc->s      = correct_rmc_data.sHHMMSS_SS.s;

    LocalFix_rmc->dd    = correct_rmc_data.dt[7].dd; // ибо 7-ое поле данных поступает позже первого
    LocalFix_rmc->mm    = correct_rmc_data.dt[7].mm;
    LocalFix_rmc->yyyy  = correct_rmc_data.dt[7].yyyy;
    LocalFix_rmc->h     = correct_rmc_data.dt[7].h;
    LocalFix_rmc->m     = correct_rmc_data.dt[7].m;
    LocalFix_rmc->s     = correct_rmc_data.dt[7].s;
    isWasCorrectRMC_DATA=0; //Сбросим параметр

    if(!GetJulianDateFromUTC(*UTC_rmc, &JD_UTC_rmc)) // из полей 'UTC' и 'Date' RMC строки
        {

        delete UTC_rmc;
        delete LocalFix_rmc;
        delete CurentUTC;
        syncMutex.unlock();
        emit messageAboutSetTime(tr("Runtime error: GetJulianDateFromUTC(*UTC_rmc, &JD_UTC_rmc)"));
        ReadThread->start();
        return;
        }
    emit messageAboutSetTime(tr("Successful execution of the function GetJulianDateFromUTC(*UTC_rmc, &JD_UTC_rmc)"));


    if(!GetJulianDateFromUTC(*LocalFix_rmc, &JD_LocalFix_rmc)) // из полей времени фиксации данных
        {

        delete UTC_rmc;
        delete LocalFix_rmc;
        delete CurentUTC;

        syncMutex.unlock();
        emit messageAboutSetTime(tr("Runtime error: GetJulianDateFromUTC(*LocalFix_rmc, &JD_LocalFix_rmc)"));
        ReadThread->start();
        return;
        }
    emit messageAboutSetTime(tr("Successful execution of the function GetJulianDateFromUTC(*LocalFix_rmc, &JD_LocalFix_rmc)"));

    // Считаем рассинхронизацию (с учетом часового пояса и сезонного времени)
    // локального времени, по которому фиксируется фиксация приема данных
    if(!GetJulianDateFromTimezoneInfo(&JD_TimeZone))
        {

            JD_TimeZone=((double)(App_set.iBias_min + App_set.iDaylightsBias_min))/1440.0;
        }
    emit messageAboutSetTime(tr("Successful execution of the function GetJulianDateFromTimezoneInfo(&JD_TimeZone)"));

    JD_TimeError = (JD_LocalFix_rmc + JD_TimeZone) - JD_UTC_rmc;

    // Узнаем текущее системное время в формате Юл. даты
    if(!GetJulianDateFromCurentSystemTime(&JD_SystemTime))
        {

        delete UTC_rmc;
        delete LocalFix_rmc;
        delete CurentUTC;
        syncMutex.unlock();
        emit messageAboutSetTime(tr("Runtime error: GetJulianDateFromCurentSystemTime(&JD_SystemTime)"));
        ReadThread->start();
        return;
        }
    emit messageAboutSetTime(tr("Successful execution of the function GetJulianDateFromCurentSystemTime(&JD_SystemTime)"));

    // преобразуем временную постоянную из параметров программы [sec] в Юлианскую дату
    JD_TimeConstant = 0.001 * App_set.TimeConstant / 86400.0;

    // Вычитаем из него ошибку и добавляем временную апостоянную
    JD_SystemTime = JD_SystemTime - JD_TimeError + JD_TimeConstant;

    // Устанавливаем новое системное время с учетом ошибки
    if(!GetUTCfromJulianDate (JD_SystemTime, CurentUTC))
        {
        delete UTC_rmc;
        delete LocalFix_rmc;
        delete CurentUTC;
        syncMutex.unlock();
        emit messageAboutSetTime(tr("Runtime error: GetUTCfromJulianDate (JD_SystemTime, CurentUTC)"));
        ReadThread->start();
        return;
        }
emit messageAboutSetTime(tr("Successful execution of the function GetUTCfromJulianDate (JD_SystemTime, CurentUTC)"));

    // если близко полночь, то синхронизацию не выполняется, т.к. это может сбить лог.протоколы
    // также за счет этого, корректирующие високосные секунды не будут ни на что влиять

    if (CurentUTC->h == 23 && CurentUTC->mm >= 45 &&
        CurentUTC->h == 0  && CurentUTC->mm <=15 )
    {
            emit curMidnightTime();
    }
    else
    {
         if(!SetSystemTimeFromUTC(*CurentUTC))
         {
             delete UTC_rmc;
             delete LocalFix_rmc;
             delete CurentUTC;
             syncMutex.unlock();
             logProtocol->WriteInfo((prtcl_char *)tr("Error setting system time\n").toUtf8().data());
             emit errorSetSystemTime();
             ReadThread->start();
             return;
         }

         syncTimer->stop();

         emit messageAboutSetTime(tr("Successful execution of the function SetSystemTimeFromUTC"));
         emit correctSetSystemTime();
         ReadThread->start();
         syncTimer->start();
    }

    delete UTC_rmc;
    delete LocalFix_rmc;
    delete CurentUTC;
    syncMutex.unlock();
    ReadThread->start();
    return;
}


//Просто считать с порта, если что пришло

void globalThread::readDataFromTCP()
{
    unsigned int tmp;
    int nb;
    while(tcp_socket->bytesAvailable())
    {
            nb=tcp_socket->bytesAvailable()%sizeof(tmp);
            if(nb==0)nb=sizeof(tmp);
            tcp_socket->read((char*)(&tmp),nb);
    }
}



//27.05.2015
void globalThread::syncTimeoutForSendingToRemote()
{
    ReadThread->sendDataToRemote();
}





//====================================================
// считать параметры программы из ини-файла
void globalThread::readSettings()
{
    // ini-файл должен находится в одной папке с программой


        QString str; // ето чтоб считать имя порта в char*
        //char *strg;

        QString inifile = QCoreApplication::applicationDirPath() + QString("/gsnn_config.ini");
        QSettings m_settings(inifile, QSettings::IniFormat);

        critical_error=0;

//        m_settings.setIniCodec(QTextCodec::codecForName("windows-1251"));

        // если ошибка доступа, то сообщаем пользователю
        if (m_settings.status() != QSettings::NoError) // 0 - QSettings::NoError
        {

            critical_error = GLBL_THRD_CRT_ERR_NO_ACCESS_CONF;
            //! Тут отправить сигнал в главный поток об ошибке
            logProtocol->WriteInfo((prtcl_char *)tr("There is a problem with reading the file with settings for the program.").toUtf8().data());
            return ;
            //qApp->quit();
        }

        // Параметры основного порта
        m_settings.beginGroup("/PortSettings");

            // преобразование имени порта в char*
            str = m_settings.value("/PortName", "").toString();

            strcpy(App_set.PortName, str.toLatin1().data()); // работает!
            //qDebug(DefaultPortName);

            App_set.BaudeRate   = m_settings.value("/BaudRate", "9600").toInt();
            App_set.StopBits    = m_settings.value("/StopBits", "0").toInt();
            App_set.Protocol    = m_settings.value("/protocol", "0").toInt();
        m_settings.endGroup();

        // Параметры программы
        m_settings.beginGroup("/AppSettings");
            App_set.SyncPeriod     = m_settings.value("/SyncPeriod_sec", "").toInt();     // периодичность синхронизации
            App_set.TimeConstant   = m_settings.value("/TimeConstant_msec", "350").toInt();  // добавочное время мсек
            App_set.iTimeForTestNoData =
                    m_settings.value("/TimeForTestNoData_sec","5").toInt();
            if(App_set.iTimeForTestNoData<1)App_set.iTimeForTestNoData=1;
            if(App_set.iTimeForTestNoData>20)App_set.iTimeForTestNoData=20;
        m_settings.endGroup();


        //Параметры для удаленной машины
        m_settings.beginGroup("/RemoteNode1");
            App_set.is_remote1=m_settings.value("/IsSendingData","0").toInt();
            str=m_settings.value("/PortName_remote","").toString();
            strcpy(App_set.cPortName_remote1, str.toLatin1()); // работает!
            App_set.iBaudeRate_remote1=m_settings.value("/BaudRate_remote", "9600").toInt();
            App_set.iStopBits_remote1=m_settings.value("/StopBits_remote","0").toInt();
        m_settings.endGroup();


        m_settings.beginGroup("/RemoteNode2");
            App_set.is_remote2=m_settings.value("/IsSendingData","0").toInt();
            str=m_settings.value("/PortName_remote","").toString();
            strcpy(App_set.cPortName_remote2, str.toLatin1()); // работает!
            App_set.iBaudeRate_remote2=m_settings.value("/BaudRate_remote", "9600").toInt();
            App_set.iStopBits_remote2=m_settings.value("/StopBits_remote","0").toInt();
        m_settings.endGroup();


        m_settings.beginGroup("/WindowsObjects");         
            App_set.local_port_event=m_settings.value("/LocalPortForEvents","0").toInt();
        m_settings.endGroup();

//Считаем параметры тайм зоны
        m_settings.beginGroup("/TimeZone");
            App_set.iIfNoTimeZoneSetThis=m_settings.value("/ifNoTimeZoneSetThis","0").toInt();
            App_set.iBias_min=m_settings.value("/Bias_min","-360").toInt();
            App_set.iDaylightsBias_min=m_settings.value("/DaylightsBias","0").toInt();
        m_settings.endGroup();
    //! проверка того, что считали

        //наиболее часто используемык скорости для приемников
        //спутниковых навигационных систем


        // проверка стоповых битов
        if (App_set.StopBits != 0 && //
            App_set.StopBits != 1 && //
            App_set.StopBits != 2)   //
            {
                logProtocol->WriteInfo((prtcl_char *)tr("Error in configuration file: StopBits is not 0, 1 and 2.").toUtf8().data());
                critical_error=GLBL_THRD_CRT_ERR_STOPBITS_CONF;
                return ;
            }

        // проверка протокола
        if (App_set.Protocol != 0 &&
            App_set.Protocol != 1)
            {
                logProtocol->WriteInfo((prtcl_char *)tr("Error in configuration file: "
                                       "Protocol is not 0 and 1.").toUtf8().data());
                critical_error=GLBL_THRD_CRT_ERR_PROTOCOL_CONF;
                return;
            }


        // проверка выбора навигационной системы

        //! Проверка параметров синхронизации

        // не более половины суток
        if (App_set.SyncPeriod <= 0 ||
            App_set.SyncPeriod > 600000)
            {
                logProtocol->WriteInfo((prtcl_char *)tr("Error in configuration file: "
                                       "SyncPeriod <=0 or > 600000").toUtf8().data());
                critical_error=GLBL_THRD_CRT_ERR_SYNCPERIOD_CONF;
            }


        // не более +- два часа для поправки времени в связи с отиеной перехода на летнее время
        if (App_set.TimeConstant < -7200000 ||
            App_set.TimeConstant > 7200000)
            {
                logProtocol->WriteInfo((prtcl_char *)tr("Error in configuration file: "
                                       "TimeConstant <-7200000 or > 7200000").toUtf8().data());
                critical_error=GLBL_THRD_CRT_ERR_TIMECONSTANT_CONF;
                return;
            }



//Проверим наличия в системе TimeZone
            if(App_set.iIfNoTimeZoneSetThis==0)
            {
                if(!GetJulianDateFromTimezoneInfo(NULL))
                {
                    logProtocol->WriteInfo((prtcl_char *)tr("The system failed to work with the TimeZone.\n "
                                           "Try to set the TimeZone or set the options ifNoTimeZoneSetThis=1\n"
                                           "and Bias_min and DaylightsBias_min in the file gsnn_config.ini").toUtf8().data());
                    critical_error=GLBL_THRD_CRT_ERR_TIME_ZONE_ERR;
                    return;

                }
            }




}


//==================================================
int globalThread::getRMC_DATA(struct RMC_DATA &rmc)
{
    readMutex.lock();
    if(!isWasRMC_DATA)
    {
        readMutex.unlock();
        return 0;
    }
    memcpy(&rmc,&rmc_data, sizeof(rmc));
    readMutex.unlock();
   return 1;

}

int globalThread::getGGA_DATA(struct GGA_DATA &gga)
{
    readMutex.lock();
    if(!isWasGGA_DATA)
    {
        readMutex.unlock();
        return 0;
    }
    memcpy(&gga,&gga_data,sizeof(gga));
    readMutex.unlock();
    return 1;
}






//====================================================
 void globalThread::receivedDataSlot(int type)
{
    struct RMC_DATA rmc_data1;
    struct GGA_DATA gga_data1;
    int iRet;
    unsigned int tcp_data=3;
    int n_of_b;


    if(tcp_socket&&iWasErrorTCPConnect)
    {
        if(ttIsTimeCame(ttNextTimeTestForError,ttGetMillySecondsCounts()))
        {
            iWasErrorTCPConnect=0;
        }
    }


//Проверим состояние порта
    if(tcp_socket&&!iWasErrorTCPConnect)
    {

            tcp_socket->connectToHost(tr("127.0.0.1"),App_set.local_port_event);
            if(tcp_socket->waitForConnected(1000))
            {
                n_of_b=tcp_socket->write((char*)(&tcp_data),sizeof(tcp_data));
                if(!tcp_socket->waitForBytesWritten(500))
                {
                    char strka[200];
                    sprintf(strka,tr("In the method globalThread::receivedDataSlot error sending data to 127.0.0.1: %d").toUtf8().data(),
                            App_set.local_port_event);
                    logProtocol->WriteInfo(strka);
                    emit errorEvent(tr(strka));
                    iWasErrorTCPConnect=1;
                    ttNextTimeTestForError=ttGetTimePlusDTime(ttGetMillySecondsCounts(),10000);

                }
                tcp_socket->disconnectFromHost();
                tcp_socket->waitForDisconnected();

            }else{
                char strka[200];
                sprintf(strka,tr("In the method globalThread::receivedDataSlot connection error 127.0.0.1:%d").toUtf8().data(),
                        App_set.local_port_event);
                logProtocol->WriteInfo(strka);
                emit errorEvent(tr(strka));
                iWasErrorTCPConnect=1;
                ttNextTimeTestForError=ttGetTimePlusDTime(ttGetMillySecondsCounts(),10000);
            }
    }



    if(type==NMEA_GPRMC||type==NMEA_GLRMC||
       type==NMEA_GNRMC||type==3000)
    {
           iRet=ReadThread->getRMC_DATA(type/10,rmc_data1);
           if(iRet>0)
           {
               isWasRMC_DATA=1;
               readMutex.lock();
               memcpy(&rmc_data,&rmc_data1,sizeof(rmc_data));
               readMutex.unlock();

               syncMutex.lock();
               if(rmc_data.A=='A'||rmc_data.A=='D')
               {
                   isWasCorrectRMC_DATA=1;
                   memcpy(&correct_rmc_data,&rmc_data,sizeof(rmc_data));

               }

               syncMutex.unlock();


               emit receivedData(type);
            }
        }


        if(type==NMEA_GPGGA||
           type==NMEA_GLGGA||
           type==NMEA_GNGGA||
           type==3000)
        {
            iRet=ReadThread->getGGA_DATA(type/10,gga_data1);
            if(iRet>0)
            {
                isWasGGA_DATA=1;
                readMutex.lock();
                memcpy(&gga_data,&gga_data1,sizeof(gga_data));
                readMutex.unlock();
                emit receivedData(type);
            }
        }

}

void globalThread::noDataEventSlot()
{
    char txt[200];
    QTextStream out(stdout);
    sprintf(txt, tr("The data is not received within %d seconds \n").toUtf8().data(),App_set.iTimeForTestNoData);
    logProtocol->WriteInfo(txt);
    isWasRMC_DATA=0;
    isWasGGA_DATA=0;
    isWasCorrectRMC_DATA=0;
    memset(&rmc_data,0,sizeof(rmc_data));
    memset(&gga_data,0,sizeof(gga_data));
    emit noDataEvent(App_set.iTimeForTestNoData);
}

void globalThread::againDataEventSlot() // Данные появились снова
{
    logProtocol->WriteInfo((prtcl_char *)tr("The data come again \n").toUtf8().data());
    emit againDataEvent();
}

void globalThread::noPackageEventSlot()
{
    char txt[200];
    sprintf(txt,tr("No gps data was within %d seconds  \n").toUtf8().data(), App_set.iTimeForTestNoData);
    logProtocol->WriteInfo(txt);
    isWasRMC_DATA=0;
    isWasGGA_DATA=0;
    isWasCorrectRMC_DATA=0;
    memset(&rmc_data,0,sizeof(rmc_data));
    memset(&gga_data,0,sizeof(gga_data));

    emit noPackageEvent(App_set.iTimeForTestNoData);
}

void globalThread::againPackageEventSlot() // Данные появились снова
{
    logProtocol->WriteInfo((prtcl_char *)tr("Gps data began to come again \n").toUtf8().data());
    emit againPackageEvent();
}





void globalThread::noDataEventForOtherSlot() //Нет длительное время данных с порта
{

    unsigned int tcp_data=1;
    if(tcp_socket&&iWasErrorTCPConnect)
    {
        if(ttIsTimeCame(ttNextTimeTestForError,ttGetMillySecondsCounts()))
        {
            iWasErrorTCPConnect=0;
        }
    }


    if(tcp_socket&&!iWasErrorTCPConnect)
    {

            tcp_socket->connectToHost(tr("127.0.0.1"),App_set.local_port_event);
            if(tcp_socket->waitForConnected(1000))
            {
                tcp_socket->write((char*)(&tcp_data),sizeof(tcp_data));
                if(!tcp_socket->waitForBytesWritten(1000))
                {
                    char strka[200];
                    sprintf(strka,tr("In the method globalThread::noDataEventForOtherSlot error sending data to 127.0.0.1:%d").toUtf8().data(),
                            App_set.local_port_event);
                    logProtocol->WriteInfo(strka);
                    emit errorEvent(strka);
                    iWasErrorTCPConnect=1;
                    ttNextTimeTestForError=ttGetTimePlusDTime(ttGetMillySecondsCounts(),10000);
                }
                tcp_socket->disconnectFromHost();
                tcp_socket->waitForDisconnected();

            }else{
                char strka[200];
                sprintf(strka,tr("In the method globalThread::noDataEventForOtherSlot error connection 127.0.0.1:%d").toUtf8().data(),
                        App_set.local_port_event);
                logProtocol->WriteInfo(strka);
                emit errorEvent(strka);
                iWasErrorTCPConnect=1;
                ttNextTimeTestForError=ttGetTimePlusDTime(ttGetMillySecondsCounts(),10000);
            }
    }


}


void globalThread::noPackageEventForOtherSlot() //Нет длительное время данных с порта
{
    unsigned int tcp_data=2;
    if(tcp_socket&&iWasErrorTCPConnect)
    {
        if(ttIsTimeCame(ttNextTimeTestForError,ttGetMillySecondsCounts()))
        {
            iWasErrorTCPConnect=0;
        }
    }

    if(tcp_socket&&!iWasErrorTCPConnect)
    {

            tcp_socket->connectToHost(tr("127.0.0.1"),App_set.local_port_event);
            if(tcp_socket->waitForConnected(1000))
            {
                tcp_socket->write((char*)(&tcp_data),sizeof(tcp_data));
                if(!tcp_socket->waitForBytesWritten(1000))
                {
                    char strka[200];
                    sprintf(strka,tr("In the method globalThread::noPackageEventForOtherSlot error sending data to 127.0.0.1:%d").toUtf8().data(),
                            App_set.local_port_event);
                    logProtocol->WriteInfo(strka);
                    emit errorEvent(strka);
                    iWasErrorTCPConnect=1;
                    ttNextTimeTestForError=ttGetTimePlusDTime(ttGetMillySecondsCounts(),10000);
                }
                tcp_socket->disconnectFromHost();
                tcp_socket->waitForDisconnected();

            }else{
                char strka[200];
                sprintf(strka,tr("In the method globalThread::noPackageEventForOtherSlot error connection 127.0.0.1::%d").toUtf8().data(),
                        App_set.local_port_event);
                logProtocol->WriteInfo(strka);
                emit errorEvent(strka);;
                iWasErrorTCPConnect=1;
                ttNextTimeTestForError=ttGetTimePlusDTime(ttGetMillySecondsCounts(),10000);
            }
    }

}



//Ошибка октрытия выходного последовательного порта 1
void globalThread::errorOpenComPortSlot(int code_err)       //Ошибка открытия порта
{
    QString str;
    if(code_err==0)
    {
        str=tr("Error allocating memory for device  ")+tr(App_set.PortName);
    }else
    if(code_err==-1)
    {
        str=tr("Error opening  ")+tr(App_set.PortName)+
                tr(" port to receive data from GPS/GLONASS receiver");
    }else
    if(code_err==-2)
    {
        str=tr("Error setting port settings ")+tr(App_set.PortName)+
               tr(" port: Baudrate: ")+QString::number(App_set.BaudeRate)+
                QString(" Stop bit: ")+QString::number(App_set.StopBits*0.5+1.0)+
                tr(" to receive data from GPS/GLONASS receiver");
    }else
    {
        str=tr("Error associated with the serial port emulation via file ")+tr(App_set.PortName);
    }
    emit errorEvent(str);
    logProtocol->WriteInfo(str.toLatin1().data());

}

void globalThread::errorOpenComPortForRemote1Slot(int code_err)       //Ошибка открытия порта
{
    QString str;
    if(code_err==0)
    {
        str=tr("Error allocating memory for device  ")+tr(App_set.cPortName_remote1);
    }else
    if(code_err==-1)
    {
        str=tr("Error opening ")+tr(App_set.cPortName_remote1)+
                tr(" port to transfer data to remote host 1");
    }else
    if(code_err==-2)
    {
        str=tr("Error setting port settings ")+tr(App_set.cPortName_remote1)+
               tr(" port: Baudrate: ")+QString::number(App_set.iBaudeRate_remote1)+
                tr(" stop bit: ")+QString::number(App_set.iStopBits_remote1*0.5+1.0)+
                tr(" to transfer data to remote host 1");
    }else
    {
        str=tr("Error associated with the serial port emulation via file ")+tr(App_set.cPortName_remote1);
    }
    emit errorEvent(str);

    logProtocol->WriteInfo(str.toLatin1().data());
}

void globalThread::errorOpenComPortForRemote2Slot(int code_err)       //Ошибка открытия порта
{
    QString str;
    if(code_err==0)
    {
        str=tr("Error allocating memory for device  ")+tr(App_set.cPortName_remote2);
    }else
    if(code_err==-1)
    {
        str=tr("Error opening ")+tr(App_set.cPortName_remote2)+
                tr(" port to transfer data to remote host 2");
    }else
    if(code_err==-2)
    {
        str=tr("Error setting port settings ")+tr(App_set.cPortName_remote2)+
               tr(" port: Baudrate: ")+QString::number(App_set.iBaudeRate_remote2)+
                tr(" stop bit: ")+QString::number(App_set.iStopBits_remote2*0.5+1.0)+
                tr(" to transfer data to remote host 2");
    }else
    {
        str=tr("Error associated with the serial port emulation via file ")+tr(App_set.cPortName_remote2);
    }
    emit errorEvent(str);
    logProtocol->WriteInfo(str.toLatin1().data());
}






