﻿#include <stdio.h>
#include "zda.h"
#include "iec61161_1tools.h"



int zdaRazborZDA( const char *str_data,
		    struct ZDA_DATA *zda_data)
{
    int i=1,k=0,j=0;
    int iRet;
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
    
    if(str_data[0]!='$')return 0;
    while(str_data[i])
    {
	summ^=(unsigned char)(str_data[i]);
	if(str_data[i]==','||str_data[i]=='*')
	{
	    if(j==0)
	    {
		if(k!=5)
		{
		      return 0;
		}  
		if(str_data[i-k]!='G'||str_data[i-k+2]!='Z'||str_data[i-k+3]!='D'||str_data[i-k+4]!='A')
		{
		      return 0;
		}  
		if(str_data[i-k+1]=='P')zda_data->ns=0;
		else if(str_data[i-k+1]=='L')zda_data->ns=1;
		else if(str_data[i-k+1]=='N')zda_data->ns=2;
		else return 0;
	    }
	    else if(j==1)
	    { 
	      
	//Считаем время UTC
		if(k!=0)
		{  
		  int hh,mm;
		  double ssss;
		  iRet=iecToolsGetTime(str_data+(i-k),&hh,&mm,&ssss);
		  if(iRet<=0)return 0;
		  zda_data->sHHMMSS_SS.h=hh;
		  zda_data->sHHMMSS_SS.m=mm;
		  zda_data->sHHMMSS_SS.s=ssss;
		  
		}else{
		    return 0;
		}  
	    }
	    else if(j==2)
	    {
	//Считаем день	      
		if(k>0)
		{  
		    iRet=sscanf(str_data+(i-k),"%d", &(zda_data->dd));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		}
	    }
	    else if(j==3)
	    {
	//Считаем месяц
		if(k>0)
		{  
		    iRet=sscanf(str_data+(i-k),"%d", &(zda_data->bb));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		}
	    }else if(j==4)
	    {
	//Считаем год
		if(k>0)
		{  
		    iRet=sscanf(str_data+(i-k),"%d", &(zda_data->yyyy));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		}
	    }
	    else if(j==5)
	    {
	//Считаем часы локальной временной зоны
		if(k>0)
		{  
		    iRet=sscanf(str_data+(i-k),"%d", &(zda_data->xx));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		}

	    }else if(j==6)
	    {
      //Считаем минуты локальной временной зоны
		if(k>0)
		{  
		    iRet=sscanf(str_data+(i-k),"%d", &(zda_data->mm));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		}
	    }
	}else{  
	    k++;
	}
	
	if(str_data[i]=='*')
	{
	    iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
	    if(iRet!=1)return 0;
	    if(((unsigned int)summ)!=recv_summ)
	    {
		return 0;
	    }
	    break;
	}  
	
	summ^=(unsigned char)(str_data[i]);
	i++;
    };  
    return 1;  
}
