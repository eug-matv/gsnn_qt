﻿/*Данные местоопределения в проекции Гауса-Крюгера*/

#ifndef 		__PIRGK_H__
#define		__PIRGK_H__

#include "time_tools.h"


struct PIRGK_TIME_UTC
{
    int h;
    int m;
    double s;
};


struct	 PIRGK_DATE
{
      int dd;
      int mm;	
      int yy;
};

struct PIRGK_DATA
{
      struct PIRGK_TIME_UTC sHHMMSS_SS;		//Время UTC
      int A;					//Индикатор качества GNSS: 0 - определение местоположения невозможно.
							  //1 - GNSS режим обычной точности, местоположение достоверно.
      double x_x;				//Координата x, м
      double y_y;				//Координата y в метрах увеличинная на 500000 плюс номер зоны, умноженный на 1000000
      double z_z;				//высота в м
      double v_v;				//скорость м/с
      double k_k;				//Курс в градусах
      struct PIRGK_DATE sDDMMYY;		//дата (день|месяц|год)
      double f_f;				//геометрический фактор ухудшения точности в плане (HDOP)
      double g_g;				//геометрический фактор ухудшения точности по высоте (VDOP)
      int n;					//Количество НКА в решении
      
};

#ifdef __cplusplus
extern "C"
{
#endif	
int pirgkRazborPIRGK( const char *str_data,
		    struct PIRGK_DATA *pirgk_data);
#ifdef __cplusplus
}
#endif

#endif
