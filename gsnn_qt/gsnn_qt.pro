#-------------------------------------------------
#
# Project created by QtCreator 2011-08-31T10:43:20
#
#-------------------------------------------------

# Directories for build and executive files
#QT       += core gui
DESTDIR += bin
OBJECTS_DIR += build
MOC_DIR += build
UI_DIR += build

# Project config

CONFIG += thread
CONFIG += app_bundle
# Project global defines
DEFINES -= QT_DLL
DEFINES -= UNICODE \
          _UNICODE

QT       += core
QT       += gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gsnn_qt

TEMPLATE = app

SOURCES += main.cpp \
    TimeMath.cpp \
    include/zda.c \
    include/vtg.c \
    include/tools_win_unix.cpp \
    include/time_tools.cpp \
    include/rmc.c \
    include/reading_nmea_data.cpp \
    include/reading_mnp_data.cpp \
    include/pirtr.c \
    include/pirsr.c \
    include/pirra.c \
    include/pirpr.c \
    include/pirgk.c \
    include/pirfv.c \
    include/pirea.c \
    include/mnp_kadr.cpp \
    include/iec_lat_long.c \
    include/iec.c \
    include/iec61161_1tools.c \
    include/gsv.c \
    include/gsa.c \
    include/glob_data.cpp \
    include/gll.c \
    include/gga.c \
    globalthread.cpp \
    readthread.cpp \
    sync_work/sync_proc_mutex.cpp \
    sync_work/sync_proc_event.cpp \
    mainwindow/mainwindow.cpp \
    include/time_count_tools.cpp


 win32:SOURCES += logfile/protocol.cpp


 unix:SOURCES += include/tools_unix.cpp

HEADERS += \
    TimeMath.h \
    defs.h \
    include/zda.h \
    include/vtg.h \
    include/tools_win_unix.h \
    include/time_tools.h \
    include/stdafx.h \
    include/rmc.h \
    include/reading_nmea_data.h \
    include/reading_mnp_data.h \
    include/pirtr.h \
    include/pirsr.h \
    include/pirra.h \
    include/pirpr.h \
    include/pirgk.h \
    include/pirfv.h \
    include/pirea.h \
    include/mnp_kadr.h \
    include/kadr3000cls.h \
    include/iec_lat_long.h \
    include/iec.h \
    include/iec61161_1tools.h \
    include/gsv.h \
    include/gsa.h \
    include/glob_data.h \
    include/gll.h \
    include/gga.h \
    globalthread.h \
    readthread.h \
    app_settings.h \
    sync_work/sync_proc_mutex.h \
    sync_work/sync_proc_event.h \
    mainwindow/mainwindow.h \
    include/time_count_tools.h \
    trans_file.h


 win32{
    HEADERS += logfile/protocol.h
 }else{
    HEADERS += logfile_unix/protocol.h  \
    tools_unix.h
 }

RESOURCES += \
    icons.qrc

INCLUDEPATH += ./include  \
          ./sync_work   \
            ./mainwindow    \
            .

win32{
    INCLUDEPATH += ./logfile
}else{
    INCLUDEPATH += ./logfile_unix
}

FORMS += \
    mainwindow/mainwindow.ui

DISTFILES +=




