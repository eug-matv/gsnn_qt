﻿#ifndef __TIME_COUNT_TOOLS_H__
#define __TIME_COUNT_TOOLS_H__

/*ѕолучение значени¤ характеризующее число микросекунд, прошедшее с какого-то периода*/
long ttGetMillySecondsCounts(void);

/*–азница между двума паказател¤ми времени*/
long ttGetDTimeMS(long ms1, long ms2);

/*ѕрирост времени*/
long ttGetTimePlusDTime(long tm_ms, long d_tm_ms);

/*наступило ли данное врем¤*/
int ttIsTimeCame(long wait_tm_ms, long cur_tm_ms);
#endif
