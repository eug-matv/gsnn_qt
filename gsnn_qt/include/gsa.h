﻿#ifndef __GSA_H__
#define __GSA_H__

#include "time_tools.h"


struct GSA_DATA
{
      int ns;	//Тип навигационной системы. 0 - GPS, 1 - ГЛОНАС, 2 GPS+Глонас
      char a;			//1 Тип управления,'M' - ручное, 'A' - автоматическое
      int  b;		//2 Режим работы 2 - 2D, 3 - 3D, иначе обсервация невозможна
      int xx[12];	//3-14 Номера НКА
      double c_c;		//15 пространственный геометрический фактор ухудшения точности
      double d_d;		//16 геометрический фактор ухудшения точности в плане
      double f_f;		//17 геометрический фактор ухудшения точности по высоте
	  struct TTDateTime dt[18];  //Время каждого элемента
};

#ifdef __cplusplus
extern "C"
{
#endif	
int gsaRazborGSA(
		   const char *str_data,
		    struct GSA_DATA *gsa_data);
			
			


#ifdef __cplusplus
}
#endif

#endif
