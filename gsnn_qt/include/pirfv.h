﻿/*Номер версии встроенного приемника*/
#ifndef 		__PIRFV_H__
#define 	__PIRFV_H__

#include "time_tools.h"

struct PIRFV_DATA
{
      int xx_;		//Мажорная часть номера версии приемника
      int _xx;		//Минорная часть н
};

#ifdef __cplusplus
extern "C"
{
#endif	
int pirfvRazborPIRFV( const char *str_data,
		    struct PIRFV_DATA *pirfv_data);
#ifdef __cplusplus
}
#endif

#endif
