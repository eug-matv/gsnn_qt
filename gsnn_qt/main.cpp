//#include <QtGui/QApplication>
#include <QApplication>
#include <QtCore>
#include <QtCore/QtGlobal>
#include <QtCore/QTimer>
#include <QMenu>
#include <QSystemTrayIcon>
#include <QMainWindow>
#include <QTranslator>

#include "mainwindow.h"
#include "globalthread.h"
#include "defs.h"
#include "trans_file.h"


#ifdef _WIN32
#include <windows.h>
#endif


#include <stdio.h>



class MyApplication : public QApplication
{
   public:
    MyApplication(int argc, char **argv) :
        QApplication (argc, argv)
    {

    };

    void setWindow(MainWindow *_mw)
    {
        mw=_mw;
    }


#ifdef _WIN32
    bool winEventFilter(MSG *mes, long *retVal)
    {
        if(mes->message == WM_QUERYENDSESSION)
        {
            mw->endSession();
            *retVal=true;
            return true;
        }
        return false;
    };
#endif
private:
   MainWindow *mw;

};


/*Главная функция программы*/
int main(int argc, char *argv[])
{
    MyApplication app(argc, argv);

#ifdef QT_RUS
    QTranslator translator;
    translator.load(QString(":/gsnn_qt_rus.qm"),".");
    app.installTranslator(&translator);
#endif


//1)	Устанавливает кодировки и языки для отображения данных.
    /*
    QTextCodec *russian = QTextCodec::codecForName("windows-1251");
    QTextCodec::setCodecForCStrings( russian );
    QTextCodec::setCodecForLocale( russian );
    QTextCodec::setCodecForTr( russian );
*/
//2) Создаем главное окно программы - реализовано в mainwindow.h, mainwindow.cpp, mainwindow.ui
    MainWindow mw;

//3) Передать параметр mw в app
    app.setWindow(&mw);

//4) Запускаем главный обработчик сообщений

    app.exec();
    return 0;
}

