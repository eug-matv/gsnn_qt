﻿#include <stdio.h>
#include "pirra.h"


int pirraRazborPIRRA( const char *str_data,
		    struct PIRRA_DATA *pirra_data)
{
    int i=1,k=0,j=0;
    int iRet;   
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
    
    if(str_data[0]!='$')return 0;
    pirra_data->n_of_x=0;
    while(str_data[i])
    {
	summ^=(unsigned char)(str_data[i]);
	if(str_data[i]==','||str_data[i]=='*')
	{
	    if(j==0)
	    {
		if(k!=5)
		{
		      return 0;
		}  
		if(str_data[i-k]!='P'||str_data[i-k+1]!='I'||str_data[i-k+2]!='R'||str_data[i-k+3]!='R'||str_data[i-k+4]!='A')
		{
		      return 0;
		}  
	    }
	    else if(j<1000)
	    { 
		
	      //Считаем результат теста
		if(k!=0)
		{  
		    iRet=sscanf(str_data+(i-k),"%d",&(pirra_data->x[pirra_data->n_of_x]));
		    if(iRet!=1)return 0;
		    (pirra_data->n_of_x)++;
		}else{
		    return 0;
		}  
	    }
	}else{  
	    k++;
	}
	
	if(str_data[i]=='*')
	{
	    iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
	    if(iRet!=1)return 0;
	    if(((unsigned int)summ)!=recv_summ)
	    {
		return 0;
	    }
	    break;
	}  
	
	summ^=(unsigned char)(str_data[i]);
	i++;
    };  
    return 1;        
}
