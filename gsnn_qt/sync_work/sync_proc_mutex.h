﻿#ifndef __SYNC_PROC_H__
#define __SYNC_PROC_H__




#ifdef _WIN32
#include <windows.h>

struct TSPMutex
{
	HANDLE handle;
	int isCreate;	
};
#else
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
struct TSPMutex
{
	sem_t *sem;				
	int isCreate;
	char *name;			
};

#endif	





struct TSPMutex* spOpenMutex(const char *name, int isCreate);

int spCloseMutex(struct TSPMutex* mtx);

/*Функция возвращает 1, если успешно. 0 - если время timeout_ms вышло, -1 -- прочие ошибки*/
int spWaitForMutex(struct TSPMutex* mtx, long timeout_ms);


/*Освобождение мьютекса*/
int spReleaseForMutex(struct TSPMutex* mtx);







#endif
