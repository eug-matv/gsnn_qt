﻿#include <stdio.h>
#include "vtg.h"



int vtgRazborVTG( const char *str_data,
		    struct VTG_DATA *vtg_data)
{
    int i=1,k=0,j=0;
    int iRet;
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
    
    if(str_data[0]!='$')return 0;
    while(str_data[i])
    {
	summ^=(unsigned char)(str_data[i]);
	if(str_data[i]==','||str_data[i]=='*')
	{
	    if(j==0)
	    {
		if(k!=5)
		{
		      return 0;
		}  
		if(str_data[i-k]!='G'||str_data[i-k+2]!='V'||str_data[i-k+3]!='T'||str_data[i-k+4]!='G')
		{
		      return 0;
		}  
		if(str_data[i-k+1]=='P')vtg_data->ns=0;
		else if(str_data[i-k+1]=='L')vtg_data->ns=1;
		else if(str_data[i-k+1]=='N')vtg_data->ns=2;
		else return 0;
	    }
	    else if(j==1)
	    { 
		
	      //Считаем истинный наземный курс
		if(k!=0)
		{  
            iRet=sscanf(str_data+(i-k),"%lf",&(vtg_data->a_a_T));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		}  
	    }
	    else if(j==2)
	    {  
	    }
	    else if(j==3)
	    {
	      //Считаем магнитный наземный курс
		 if(k!=0)
		 {  
            iRet=sscanf(str_data+(i-k),"%lf",&(vtg_data->f_f_M));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		} 
		    
	    }else if(j==4)
	    { 
	    }
	    else if(j==5)
	    {
	      //Считаем наземную скорость в узлах
		 if(k!=0)
		 {  
            iRet=sscanf(str_data+(i-k),"%lf",&(vtg_data->c_c_N));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		} 
	    }else if(j==6)
	    {
		 
	    }else if(j==7)
	    {
	      //Считаем наземную скорость в км/ч
		 if(k!=0)
		 {  
            iRet=sscanf(str_data+(i-k),"%lf",&(vtg_data->d_d_K));
		    if(iRet!=1)return 0;
		}else{
		    return 0;
		} 

	    }else if(j==8)
	    {
		
	    }else if(j==9)
	    {
		int ii;
		vtg_data->b=0;
		for(ii=i-k;ii<i;ii++)
		{
		    if(str_data[ii]=='A'||str_data[ii]=='D'||
			   str_data[ii]=='E'||str_data[ii]=='M'||
			   str_data[ii]=='S'||str_data[ii]=='N')
		    {
			    vtg_data->b=str_data[ii];
		    }
		}
		if(vtg_data->b==0)return 0;  
	    }
	}else{  
	    k++;
	}
	
	if(str_data[i]=='*')
	{
	    iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
	    if(iRet!=1)return 0;
	    if(((unsigned int)summ)!=recv_summ)
	    {
		return 0;
	    }
	    break;
	}  
	
	summ^=(unsigned char)(str_data[i]);
	i++;
    };  
    return 1;  
}
