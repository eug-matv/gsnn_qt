﻿#ifndef GLOBALTHREAD_H
#define GLOBALTHREAD_H

#include <QMutex>
#include <QThread>
#include <QTimer>
#include <QEventLoop>
#include <QTcpSocket>


#include "app_settings.h"
#include "readthread.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/mman.h>
#endif


//include\rmc.h
#include "rmc.h"

//include\gsv.h
#include "gsv.h"

//include\gga.h
#include "gga.h"

//logfile\protocol.h
#include "protocol.h"


#include "sync_proc_event.h"


//Критические ошибки создания  - параметр класса critical_error
//Ошибок нет - всё нормально
#define GLBL_THRD_CRT_ERR_OK 0

//Не найден файл конфигурации
#define GLBL_THRD_CRT_ERR_NO_CONF (-1)


//Ошибка доступа к файлу конфигурации
#define GLBL_THRD_CRT_ERR_NO_ACCESS_CONF (-2)


//ошибка --- не использовать эту ситуацию
#define GLBL_THRD_CRT_ERR_BAUDRATE_CONF (-3)

//Неправильное значени числа битов в ini-файле
#define GLBL_THRD_CRT_ERR_STOPBITS_CONF (-4)


//Неправильное значение номера протокола. Должно быть либо 0 либо 1
#define GLBL_THRD_CRT_ERR_PROTOCOL_CONF (-5)


//Неправильное значение периода синхронизации
#define GLBL_THRD_CRT_ERR_SYNCPERIOD_CONF (-7)

//Неправильное значение временной константы
#define GLBL_THRD_CRT_ERR_TIMECONSTANT_CONF (-8)

//Неправильное значение числа минимальных спутников
#define GLBL_THRD_CRT_ERR_MIN_SAT_COUNT_CONF (-9)

//Ошибка установки опций
#define GLBL_THRD_CRT_ERR_SET_OPT (-10)


//Нельзя запустить поток, если он уже запущен
#define GLBL_THRD_CRT_ERR_START_READ_RUN (-12)


//Ошибка открытия порта для чтения данных
#define GLBL_THRD_CRT_ERR_RUN_READ_PORT_IN (-13)

//Ошибка открытия порта для передачи данных на удаленный узел 1
#define GLBL_THRD_CRT_ERR_RUN_READ_PORT_OUT1 (-14)


//Ошибка открытия порта для передачи данных на удаленный узел 1
#define GLBL_THRD_CRT_ERR_RUN_READ_PORT_OUT2 (-15)

//Ошибка не настроена TimeZone
#define GLBL_THRD_CRT_ERR_TIME_ZONE_ERR (-20)



class globalThread : public QThread
{
    Q_OBJECT

public slots:

    //void stopThread();
    //void restartThread();

public:

    globalThread();
    ~globalThread();

    void destroy(); // уничтожить поток

    // флаги состояния объекта (если класс будет использоваться не в Qt)
    int summ_error; // интегральная ошибка
    int critical_error; // критическая ошибка
    int ini_file_error;
    int nodata_error;
    int wrongdata_error;

//Тестирование программы на выход

    int getRMC_DATA(struct RMC_DATA &rmc);
    int getGGA_DATA(struct GGA_DATA &gga);



protected:

    void run(); // основной цикл работы потока
    void stop();
    void readSettings();    // считать параметры программы из ини-файла




/*
    void enumerateSerialPorts(); // создает список последовательных портов
    bool NMEAdeviceAutoSearch(int baude_rate);  // поиск NMEA-совместимого

    QList<QString> SerialPortList; // перечень доступных COM-портов системы
    Пока убрано
*/

private:

    QEventLoop loop; // Обработка событий таймеров в потоке

    readThread *ReadThread; // Поток чтения

    TProtocol *logProtocol;


    AppSettings App_set;   // структура с параметрами пррограммы

    int isWasRMC_DATA;
    struct RMC_DATA rmc_data; // данные последней RMC-строки
    int isWasGGA_DATA;
    struct GGA_DATA gga_data; // данные последней GGA-строки

    int isWasCorrectRMC_DATA;
    struct RMC_DATA correct_rmc_data; // данные последней RMC-строки которая корректна

    QMutex readMutex;       // мютекс чтения данных
    QMutex syncMutex;       // мютекс синхронизации

   QTimer *syncTimer;       //Таймер

   QTimer *syncTimerForSend;   //Таймер для поссылки информации

   QTcpSocket *tcp_socket;  //Сокет TCP-IP

   int iWasErrorTCPConnect;   //Была ошибка соединения
   long ttNextTimeTestForError;

signals:
    // сигналы об ошибках
    void criticalError(const QString &);   //Передается сообщение об ошибке
    void errorEvent(const QString &);

    void noDataEvent(int);             // Не было получено данных в течении длительного периода
    void againDataEvent();          // Данные появились снова
    void noPackageEvent(int);             // Не было получено данных в течении 5 секунд
    void againPackageEvent();          // Данные появились снова


    void receivedData(int);            //распознан и получен пакет данных
                                       //В качестве параметра передается.
                                       //Если тип пакета IEC, то значение
                                       //из файла iec61161_1tools.h
                                       //Если кадр 3000, то число 3000


    void noDataGPSWithDateTime();  //Нет информации со временем и даты

    void errorSetSystemTime();      //Ошибка установки системного времени

    void curMidnightTime();         //Текущее времея полночь - это не ошибку, но корректировку не
                                    //не делаем

    void correctSetSystemTime();    //Успешная установка нового системного времени


    void messageAboutSetTime(const QString&);   //Сообщение об установки времени

protected slots:

    void syncTimeout();     // действия по срабатыванию таймера синхронизации времени


    void readDataFromTCP(); //Чтение данных с порта, если что пришло



    //27.05.2015
    void syncTimeoutForSendingToRemote();



public slots:

    void noDataEventSlot();             // Не было получено данных в течении 5 секунд
    void againDataEventSlot();          // Данные появились снова
    void noPackageEventSlot();             // Не было получено данных в течении 5 секунд
    void againPackageEventSlot();          // Данные появились снова
    void receivedDataSlot(int type);            //появились новые данные - надо добавить
    void noDataEventForOtherSlot(); //Нет длительное время данных с порта
    void noPackageEventForOtherSlot(); //Нет длительное время данных с порта
    void errorOpenComPortSlot(int code_err);       //Ошибка открытия порта
    void errorOpenComPortForRemote1Slot(int code_err); //Ошибка открытия порта для связи
                                                //с удаленным узлом 1

    void errorOpenComPortForRemote2Slot(int code_err); //Ошибка открытия порта для связи
                                                //с удаленным узлом 2


};


#endif // GLOBALTHREAD_H
