﻿/*PIRSR  -  выбор спутников, используемых в решении навигационной задачи*/

#ifndef 		__PIRSR_H__
#define 	__PIRSR_H__


struct PIRSR_GPS_MASK
{
    int isNoEmpty;	//Ќе пустое ли поле
    int m[32];		//Ќе нулевое значение использовани¤ спутника, нулевое -- не использовани¤
};



struct PIRSR_GLONASS_MASK
{
    int isNoEmpty;	//Ќе пустое ли поле
    int m[24];		//Ќе нулевое значение использовани¤ спутника, нулевое -- не использовани¤
};



struct PIRSR_DATA
{
    struct PIRSR_GPS_MASK pppppppp;
    struct PIRSR_GLONASS_MASK xxxxxx;
};


/*≈сли успешно, то возвращает полное число байт включа¤ стартовый и два конечных <CR>  и <LF>.
0 - если ошибка в данных и пакет не получен,
и -1 -- если не хватило по длине size_data<, чем реальна¤ длина пакетов
*/
#ifdef __cplusplus
extern "C"
{
#endif	
int pirsrMakePIRSR(const struct PIRSR_DATA *pirsr_data, 
		   char *str_data, int size_str_data);
#ifdef __cplusplus
}
#endif


#endif
