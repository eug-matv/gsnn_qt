﻿#include <stdio.h>
#include "gsa.h"

int gsaRazborGSA(
		   const char *str_data,
		    struct GSA_DATA *gsa_data)
{
    int i=1,k=0,j=0;
    int iRet;
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
    
    if(str_data[0]!='$')return 0;
    while(str_data[i])
    {
	summ^=(unsigned char)(str_data[i]);
	if(str_data[i]==','||str_data[i]=='*')
	{
	    if(j==0)
	    {
    		if(k!=5)
	    	{
		       return 0;
		    }  
		    if(str_data[i-k]!='G'||str_data[i-k+2]!='G'||str_data[i-k+3]!='S'||str_data[i-k+4]!='A')
	        {
		      return 0;
     		}  
		    if(str_data[i-k+1]=='P')gsa_data->ns=0;
		    else  if(str_data[i-k+1]=='L')gsa_data->ns=1;
		    else if(str_data[i-k+1]=='N')gsa_data->ns=2;
		    else return 0;
	    }
	    else if(j==1)
	    { 
	      //Считаем тип устройства
		if(k!=0)
		{  
		    gsa_data->a=(int)str_data[i-k]; 
		}else{
		    gsa_data->a=0;
		}  
	    }
	    else if(j==2)
	    {
	      //Считаем режим работы
		if(k>0)
		{  
		      gsa_data->b=(int)(str_data[i-k])-0x30;
		}else{
		      gsa_data->b=-1;
		}  
	    }
	    else if(j>=3&&j<15)
	    {
	      //Считаем номера НКА 
		  if(k>0)
		  {
		      iRet=sscanf(str_data+(i-k),"%d",&(gsa_data->xx[j-3]));
		      if(iRet!=1)gsa_data->xx[j-3]=-1;
		      
		  }else{
		      gsa_data->xx[j-3]=-1;
		  }  
	    }else if(j==15)
	    {
		   if(k>0)
		   {
		      iRet=sscanf(str_data+(i-k),"%lf",&(gsa_data->c_c));
		      if(iRet!=1)
		      {
			    gsa_data->c_c=-1.0e20;
		      }	    
		   }else{
		      gsa_data->c_c=-1.0e20;
		   }  
	    }else if(j==16)
	    {
		   if(k>0)
		   {
		      iRet=sscanf(str_data+(i-k),"%lf",&(gsa_data->d_d));
		      if(iRet!=1)
		      {
			    gsa_data->d_d=-1.0e20;
		      }	    
		   }else{
		      gsa_data->d_d=-1.0e20;
		   }
	    }else if(j==17)
	    {
		   if(k>0)
		   {
		      iRet=sscanf(str_data+(i-k),"%lf",&(gsa_data->f_f));
		      if(iRet!=1)
		      {
			    gsa_data->f_f=-1.0e20;
		      }	    
		   }else{
		      gsa_data->f_f=-1.0e20;
		   }  
	    }
	    j++;
	    k=0;
	}else{  
	    k++;
	}
	
	if(str_data[i]=='*')
	{
	    iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
	    if(iRet!=1)return 0;
	    if(((unsigned int)summ)!=recv_summ)
	    {
		return 0;
	    }
	    break;
	}  
	
	summ^=(unsigned char)(str_data[i]);
	i++;
    };  
    return 1;  
}
