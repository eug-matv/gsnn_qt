
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


#include "tools_unix.h"


int kill_process(const char *proc_name)
{
   DIR *pDir;
   int iRet,szProcName,idProc;
   int n_of_kill=0;
   char fname[2000],realfile[2000];
   struct dirent *dr;

   pDir=opendir("/proc");
   if(!pDir)return (-1);
   szProcName=strlen(proc_name);
   do
   {
        dr=readdir(pDir);
        if(dr&&strcmp(dr->d_name,"")&&dr->d_type==DT_DIR&&dr->d_name[0]>=0x30&&dr->d_name[0]<=0x39)
        {
            sprintf(fname,"/proc/%s/exe",dr->d_name);
            iRet=readlink(fname,realfile,1999);
            if(iRet>0&&iRet>szProcName)
            {
                realfile[iRet]=0;
                if(strcmp(realfile+(iRet-szProcName),proc_name)==0&&realfile[iRet-szProcName-1]=='/')
                {
                    idProc=atoi(dr->d_name);
                    if(idProc!=getpid())
                    {
                        kill(idProc,9);
                        n_of_kill++;
                    }
                }
            }
        }
   }while(dr);
   closedir(pDir);
   return n_of_kill;
}


int matv_pidof(const char *proc_name, int pids[], int max_n_of_pids)
{
    DIR *pDir;
    int iRet,szProcName;
    int n_of_proc=0;
    char fname[2000],realfile[2000];
    struct dirent *dr;

    pDir=opendir("/proc");
    if(!pDir)return (-1);
    szProcName=strlen(proc_name);
    do
    {
         dr=readdir(pDir);
         if(dr&&strcmp(dr->d_name,"")&&dr->d_type==DT_DIR&&dr->d_name[0]>=0x30&&dr->d_name[0]<=0x39)
         {
             sprintf(fname,"/proc/%s/exe",dr->d_name);
             iRet=readlink(fname,realfile,1999);
             if(iRet>0&&iRet>szProcName)
             {
                 realfile[iRet]=0;
                 if(strcmp(realfile+(iRet-szProcName),proc_name)==0&&realfile[iRet-szProcName-1]=='/')
                 {
                     if(n_of_proc<max_n_of_pids)
                     {
                        pids[n_of_proc]=atoi(dr->d_name);
                        n_of_proc++;
                     }
                 }
             }
         }
    }while(dr);
    closedir(pDir);
    return n_of_proc;
}


/**/
int findProcessesOpenFile(const char *file_name, int *proc_id, int max_n_of_proc_id)
{

    DIR *pDir1, *pDir2;
    int n_of_files=0;
    int iRet;
    char dname[2000],fname[2000],realfile[2000];
    struct dirent *dr1, *dr2;
    pDir1=opendir("/proc");
    if(!pDir1)return (-1);
    do
    {
         dr1=readdir(pDir1);
         if(dr1&&strcmp(dr1->d_name,"")&&dr1->d_type==DT_DIR&&dr1->d_name[0]>=0x30&&dr1->d_name[0]<=0x39)
         {
             sprintf(dname,"/proc/%s/fd",dr1->d_name);
             pDir2=opendir(dname);
             if(!pDir2)continue;
             do
             {
                 dr2=readdir(pDir2);
                 if(dr2&&dr2->d_name[0]>=0x30&&dr2->d_name[0]<=0x39)
                 {
                     sprintf(fname,"%s/%s",dname,dr2->d_name);
                     iRet=readlink(fname,realfile,1999);
                     if(iRet>0)
                     {
                        realfile[iRet]=0;
                        if(strcmp(realfile,file_name)==0)
                        {
                            if(n_of_files<max_n_of_proc_id)
                            {
                                if(proc_id)
                                {
                                    proc_id[n_of_files]=atoi(dr2->d_name);
                                }
                            }
                            n_of_files++;
                        }
                    }

                 }

             }while(dr2);
             closedir(pDir2);
         }
    }while(dr1);
    closedir(pDir1);
    return n_of_files;
}


int findProcessesOpenFile(const char *file_name)
{
    return findProcessesOpenFile(file_name,NULL,0);

}
