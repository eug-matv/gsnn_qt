﻿/*Автор Матвеенко Е.А.
Данный класс обеспечивает хранение всех пакетов по типам.
*/

#ifndef __GLOB_DATA_H__
#define __GLOB_DATA_H__

#ifdef linux
#include <pthread.h>
#else
#include <windows.h>
#endif

#include <stdlib.h>
#include "time_tools.h"
#include "iec61161_1tools.h"
#include "kadr3000cls.h"



class TDATA_OF_ALL_PACKAGES
{

private:       

/*Тут определены пакеты получаемые в формате iec. Получены только достоверные даты.
Пока это проверяется только для RMC
*/
    struct TTDateTime nmea_dt[30];  //Первые тридцать элементов - номера которых начинаются с 1
    char *nmea_str[30];
	void *nmea_data[30];
    int nmea_maxsz[30];
    int nmea_sz[30];
    struct TTDateTime iec_dt[30];
    void *iec_data[30];
	char *iec_str[30];
    int iec_maxsz[30];
    int iec_sz[30];








/*Данные в формате mnp-binary*/
	unsigned short k3000[80+1+5];  //Кадр 3000
	int sz_k3000;
	TKadr3000cls k3000obj;			//Объект типа кадр 3000
	
	
	unsigned short k3001[416+1+5]; //Кадр 3001
	int sz_k3001;
//Пока класс не реализован	
	
	
	unsigned short k3011[624+1+5]; //Кадр 3011
	int sz_k3011;
//Пока класс не реализован

	unsigned short k3002[448+1+5]; //Кадр 3002	
	int sz_k3002;
//Пока класс не реализован

	unsigned short k3003[2*24+1+5]; //Кадр 3003
	int sz_k3003;
//Пока класс не реализован	

	int isDestroying;
	int last_type_data;		//Последний тип данных
	
	
	
#ifdef linux       
       pthread_mutex_t mutex;      //Мьютекс для Линукса
#else
       CRITICAL_SECTION csCS;      //Критическая секция для Винды
#endif           
    
public:    
    TDATA_OF_ALL_PACKAGES()
    {
        int i;
        isDestroying=0;
        for(i=0;i<30;i++)
        {
           nmea_str[i]=0;
           iec_str[i]=0;
           nmea_maxsz[i]=nmea_sz[i]=0;
           iec_maxsz[i]=iec_sz[i]=0; 
		   iec_data[i]=nmea_data[i]=0;
        }      
		sz_k3000=sz_k3001=sz_k3002=sz_k3003=sz_k3011=0;
		
#ifdef linux
        pthread_mutex_init(&mutex,NULL);    
#else
        InitializeCriticalSection(&csCS);
#endif                  
		last_type_data=-1;
    };       
    
    ~TDATA_OF_ALL_PACKAGES()
    {
       int i;                   
#ifdef linux
       pthread_mutex_lock(&mutex);
#else
       EnterCriticalSection(&csCS);
#endif           
       for(i=0;i<30;i++)
       {
          if((nmea_maxsz[i])>0)delete[](nmea_str[i]);
          if(iec_maxsz[i]>0)delete[](iec_str[i]);    
		  if(iec_data[i])free(iec_data[i]);
		  if(nmea_data[i])free(nmea_data[i]);
          
       }
       isDestroying=1;
#ifdef linux
       pthread_mutex_unlock(&mutex);
#else
       LeaveCriticalSection(&csCS);
#endif           

       
#ifdef linux
       pthread_mutex_destroy(&mutex); 
#else
       DeleteCriticalSection(&csCS);     
#endif                                 
    };
	

//Сброс данных
	int reset();
    
    int addDataIEC(const TTDateTime &dt, 
                   const void *ucData, 
                   int szData);

	int addDataMNP(const TTDateTime &dt,
				   const void *ucData, 
				   int szData);
	
	
    int getDataIEC(int type,
                   TTDateTime &dt,
                   void *ucData,
                   int maxszData,
                   int &szData);                              
    
    int getDataStr(int type,
                   char *ucData,
                   int maxszData,
                   int &szData     
                        );          
                        
    int addDataStr(const char *cData,
                   int szData);                                                      

    int getNotStringData(int type,
               void *notString);          
			   
    int getDataByTypeAndIndex(int type,
	                          int indx,
							  unsigned char *out_bytes,		   //Выходной байт	
							  int max_sz_out_bytes,			   //размер массива	
							  int &sz_out_bytes,				   //Размер данных в байтах
							  struct TTDateTime &dt			   //Время получения пакета	 
							  );		
							  
//Получение рекомендованного минимума данных
	int getRMC_DATA(int type_of_protocol,   //Тип протокола 0 - NMEA, 1 - MNP
			   int ns,				//ns=0 -- GPS, ns==1 -- ГЛОНАСС, ns==2 -- GPS - ГЛОНАСС
			    struct RMC_DATA &rmc_data	//Заполненные данные
				);

        //! Прототип функции определения количества сопровождаемых спутников
        int getGSV_DATA( int ns, struct GSV_DATA &gsv_data);
        int getGGA_DATA( int type_of_protocol,   //Тип протокола 0 - NMEA, 1 - MNP
                         int ns,				//ns=0 -- GPS, ns==1 -- ГЛОНАСС, ns==2 -- GPS - ГЛОНАСС
                          struct GGA_DATA &gga_data	//Заполненные данные
                         );
	
							  
};




#endif
