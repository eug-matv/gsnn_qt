﻿#ifdef linux
#include <sys/timeb.h>
#include <time.h>
#else
#include <windows.h>
#endif
#include <stdio.h>

#include "time_tools.h"


int ttGetLocalTime(struct TTDateTime *dt)
{

#ifdef linux
    int iRet;
   struct tm tm_t;
   struct timeb tmb;
   iRet=ftime(&tmb);
   if(iRet)return -1;    
   if(!localtime_r(&(tmb.time),&tm_t))return (-2);
   dt->yyyy=1900+tm_t.tm_year; //! а не 1970 ли год ?
   dt->mm=1+tm_t.tm_mon;
   dt->dd=tm_t.tm_mday;
   dt->h=tm_t.tm_hour;
   dt->m=tm_t.tm_min;
   dt->s=tm_t.tm_sec+0.001*tmb.millitm;
#else
   SYSTEMTIME systm;
   GetLocalTime(&systm);
   dt->yyyy=systm.wYear;
   dt->mm=systm.wMonth;
   dt->dd=systm.wDay;
   dt->h=systm.wHour;
   dt->m=systm.wMinute;
   dt->s=systm.wSecond+0.001*systm.wMilliseconds;
#endif   
   return 1;
}


int ttGetTimeInStr(const struct TTDateTime *dt,
					char *str, int sz_str)
{
		if(!str||!dt||sz_str<14)return 0;
		sprintf(str,"%02d:%02d:%06.03lf",dt->h,dt->m,dt->s);
		return 1;
}					

int ttGetDateInStr(const struct TTDateTime *dt,
					char *str, int sz_str)
{
		if(!str||!dt||sz_str<12)return 0;
		sprintf(str,"%02d.%02d.%04d",dt->dd,dt->mm,dt->yyyy);
		return 1;
}					


int ttGetDateTimeInStr(const struct TTDateTime *dt,
					char *str, int sz_str)
{
		if(!str||!dt||sz_str<(12+14+3))return 0;
		sprintf(str,"%02d.%02d.%04d   %02d:%02d:%06.03lf",
		            dt->dd,dt->mm,dt->yyyy,dt->h,dt->m,dt->s);
		return 1;
}					
