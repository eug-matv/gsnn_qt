﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QAction>
#include <QIcon>
#include <QCloseEvent>
#include "globalthread.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:


    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


    
private:
static    bool isEndSession;

    Ui::MainWindow *ui;
    QSystemTrayIcon *sti;   //Трей для работы программы
    QAction *aShow;
    QAction *aHide;
    QAction *aExit;
    QMenu *mSystemTrayMenu;

    globalThread *glbl_thrd;
    int count_line;

protected:
    void createActions();
    void createSystemTray();

//Промежуточная процедура - добавление записи в textBrowser
    void addLineToTextBrowser(const QString &line);


//Событие закрытия окна
    virtual void closeEvent(QCloseEvent *event);

//Дополнительная процедура. Устанавливает, что нет информации о RMC
    void noRMCInfoOut();

protected slots:
    void activateWindowByMouseClick(QSystemTrayIcon::ActivationReason reason);


public slots:
    void errorEventSlot(const QString &str);       //Ошибка открытия порта

    void noDataEventSlot(int iPeriod_sec); //Не было данных в течении определенного периода
    void againDataEventSlot();             //Данные снова стали поступать
    void noPackageEventSlot(int iPeriod_sec);
    void againPackageEventSlot();          // Данные появились снова
    void receivedDataSlot(int);            //распознан и получен пакет данных
                                       //В качестве параметра передается.
                                       //Если тип пакета IEC, то значение
                                       //из файла iec61161_1tools.h
                                       //Если кадр 3000, то число 3000
    void noDataGPSWithDateTimeSlot();  //Нет информации со временем и даты
    void errorSetSystemTimeSlot();      //Ошибка установки системного времени
    void curMidnightTimeSlot();         //Текущее времея полночь - это не ошибку, но корректировку не
                                        //не делаем
    void correctSetSystemTimeSlot();    //Успешная установка нового системного времени
    void messageAboutSetTimeSlot(const QString &str);

    void criticalErrorOut(const QString &str);

    void endSession();          //Завершение работы сессии
};

#endif // MAINWINDOW_H
