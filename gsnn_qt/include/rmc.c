﻿#include <stdio.h>
#include <string.h>
#include "rmc.h"
#include "iec61161_1tools.h"
#include "iec_lat_long.h"


int rmcRazborRMC( const char *str_data,
		    const struct TTDateTime *dt,
			struct RMC_DATA *rmc_data)
{
    int i=1,k=0,j=0;
    int iRet;		  //
    unsigned char summ=0;  //Контрольная сумма, которая расчитана
    unsigned int recv_summ;	//Полученная контрольная сумма
	int gr;
	double m;
	
    if(str_data[0]!='$')return 0;
    while(str_data[i])
    {
		
		if(str_data[i]==','||str_data[i]=='*')
	   {
	       if(j==0)
	       {
		      if(k!=5)
    		  {
	    	      return 0;
		      }  
		      if(str_data[i-k]!='G'||str_data[i-k+2]!='R'||str_data[i-k+3]!='M'||str_data[i-k+4]!='C')
		      {
		         return 0;
		      }  
		      if(str_data[i-k+1]=='P')rmc_data->ns=0;
		       else if(str_data[i-k+1]=='L')rmc_data->ns=1;
		       else if(str_data[i-k+1]=='N')rmc_data->ns=2;
		       else return 0;
	       }
	       else if(j==1)
	       { 
	      //Считаем время UTC
		      if(k!=0)
		      {  
		        int hh,mm;
		        double ssss;
		        iRet=iecToolsGetTime(str_data+(i-k),&hh,&mm,&ssss);
		        if(iRet>0)
		        {
			      rmc_data->sHHMMSS_SS.h=hh;
			      rmc_data->sHHMMSS_SS.m=mm;
			      rmc_data->sHHMMSS_SS.s=ssss;
			      memcpy(rmc_data->dt+j,dt,sizeof(struct TTDateTime));
		        }
		      }
	       }
	       else if(j==2)
	       {
	      //Счмиаем статус
		      if(k!=0)
		      {  
		         int ii;
		         for(ii=i-k;ii<i;ii++)
			     {
			        if(str_data[ii]=='V'||str_data[ii]=='A'||str_data[ii]=='D')
			        {
			          rmc_data->A=str_data[ii];
				      memcpy(rmc_data->dt+j,dt,sizeof(struct TTDateTime));
				      break;
                                }
                             }
		       }  
	       }
	       else if(j==3)
	       {
	      //Считаем широту 
		  
		      if(k>0)
		      {
		          iRet=iecToolsGetLatitudeGrMin(str_data+(i-k),&gr,&m);
		          if(iRet<=0)
			      {
				     gr=-1;
				     m=-1.0;
			      }	  
		      }else{
			      gr=-1;
			      m=-1.0;
		      }  
	        }else if(j==4)
	        {
		 //Счмиаем север это или юг
		      if(k!=0&&gr>=0)
		      {  
		        int ii;
		        for(ii=i-k;ii<i;ii++)
		        {
	    	 	    if(str_data[ii]=='N'||str_data[ii]=='S')
		    	    {
				       rmc_data->sBBBB_BBBB_a.gr=gr;	
				       rmc_data->sBBBB_BBBB_a.m=m;
			           rmc_data->sBBBB_BBBB_a.a=str_data[ii];
				       memcpy(rmc_data->dt+(j-1),dt,sizeof(struct TTDateTime));
				       break;
			       }
		        }
		      } 
	       }
	       else if(j==5)
	       {
		      //Считаем долготу  
		      if(k>0)
		      {
		         iRet=iecToolsGetLatitudeGrMin(str_data+(i-k),&gr,&m);
		         if(iRet<=0)
			     {
				    gr=-1; 
				    m=-1.0;
			     }	  
		      }else{
				    gr=-1; 
				    m=-1.0;
		      }   
	        }else if(j==6)
	        {
		 //Считаем запад это или восток
		      if(k!=0&&gr>=0)
		      {  
		        int ii;
		        for(ii=i-k;ii<i;ii++)
		        {
		    	   if(str_data[ii]=='E'||str_data[ii]=='W')
    		       {
					   rmc_data->sLLLL_LLLL_a.gr=gr;	
				       rmc_data->sLLLL_LLLL_a.m=m;
					   rmc_data->sLLLL_LLLL_a.a=str_data[ii];
					   memcpy(rmc_data->dt+(j-2),dt,sizeof(struct TTDateTime));
				       break;
				   }
		        }
		      }
		   }else 
		   if(j==7)
	       {
		     double dfV;
//Считать наземную скорость в узлах
		     if(k>0)
	    	 {
		        iRet=sscanf(str_data+(i-k),"%lf",&dfV);
			    if(iRet==1)
			    {	
		           rmc_data->v_v=dfV;
			       memcpy(rmc_data->dt+(j-2),dt,sizeof(struct TTDateTime));
			    }
		    }

	       }else if(j==8)
	       {
		      double dfV;
//Считать наземный курс в градусах
		      if(k>0)
		      {
		         iRet=sscanf(str_data+(i-k),"%lf",&dfV);
		         if(iRet==1)
			     {	
		            rmc_data->z_z=dfV;
			        memcpy(rmc_data->dt+(j-2),dt,sizeof(struct TTDateTime));
			     }   
		      }
	       }else if(j==9)
	       {
		      int dd,mm,yy; 	
		      if(k>0)
		      {
		         iRet=iecToolsGetDate(str_data+(i-k),&dd,&mm,&yy);
		         if(iRet>0)
			     {
				    rmc_data->sDDMMYY.dd=dd;
				    rmc_data->sDDMMYY.mm=mm;
				    rmc_data->sDDMMYY.yy=yy;
			        memcpy(rmc_data->dt+(j-2),dt,sizeof(struct TTDateTime));				 
			    }	
		      }
		   }else if(j==10)
	       {
		
		       if(k>0)
		       {
		           iRet=sscanf(str_data+(i-k),"%lf",&m); 
		           if(iRet!=1)
			       {	
				      m=-10000.0;
			       }
		       }else{
		           m=-10000.0;   
		       }  
	       }else if(j==11)
	       {
		 //Считаем запад это или восток
		        if(k!=0&&m>-9999.0)
		        {  
		           int ii;
		   
		           for(ii=i-k;ii<i;ii++)
		           {
			          if(str_data[ii]=='E'||str_data[ii]=='W')
			          {
			             rmc_data->x_x_a.a=str_data[ii];
				         rmc_data->x_x_a.x_x=m;
					     memcpy(rmc_data->dt+(j-3),dt,sizeof(struct TTDateTime));				 
				         break;
			          }
		           }
		   
		        }
	       }else if(j==12)
	       {
		 //Считаем режим местоопределения
		        if(k!=0)
		        {  
		          int ii;
		          for(ii=i-k;ii<i;ii++)
		          {
			          if(str_data[ii]=='A'||str_data[ii]=='D'||
			             str_data[ii]=='E'||str_data[ii]=='M'||
			             str_data[ii]=='S'||str_data[ii]=='N')
			          {
			             rmc_data->b=str_data[ii];
				         memcpy(rmc_data->dt+(j-3),dt,sizeof(struct TTDateTime));				 
			             break;				
			          }
		          }
		        }
		   }
	       j++;
	       k=0;
	   }else{  
	       k++;
	   }
	
	   if(str_data[i]=='*')
	   {
	       iRet=sscanf(str_data+(i+1),"%X",&recv_summ);
	       if(iRet!=1)return 0;
	       if(((unsigned int)summ)!=recv_summ)
	       {
		    return 0;
	       }
	       break;
	   }else{
		   summ^=(unsigned char)(str_data[i]);	
	   }  
	   i++;
    };
	
	memcpy(rmc_data->dt, dt,sizeof(struct TTDateTime));
	

//Теперь надо проверить, а достоверно ли данное
    if(rmc_data->A=='D')
    {
        return 10;
    }

    if(rmc_data->A=='V')
    {
        return 20;
    }
	
    return 1;  
}





int rmcMakeRMC(
			   const struct RMC_DATA *rmc_data,
			   char *str_data,
			   int size_str_data
			  )
{
    int nc=0;
    int i;
    unsigned char ctr_summ=0;
    if(nc+2>size_str_data)return (-1);
    str_data[nc]='$';
    nc+=1;
//Вывести RMC 
    if(nc+6>size_str_data)return (-1);
	if(rmc_data->ns==0)
	{	
		memcpy(str_data+nc,"GPRMC,", 6);
	}
	else if(rmc_data->ns==1)
	{
		memcpy(str_data+nc,"GLRMC,", 6);
	}else{
		memcpy(str_data+nc,"GNRMC,", 6);
	}	
    nc+=6;
    
//Вывести время
    if(rmc_data->sHHMMSS_SS.h<0||rmc_data->sHHMMSS_SS.h>23||
	rmc_data->sHHMMSS_SS.m<0||rmc_data->sHHMMSS_SS.m>=60||
	rmc_data->sHHMMSS_SS.s<0||rmc_data->sHHMMSS_SS.s>59.99)
    {
	   if(nc+1>size_str_data)return (-1);
	   str_data[nc]=',';
	   nc++;
    }else{
	   if(nc+10>size_str_data)return (-1);
	   sprintf(str_data+nc, "%02d%02d%05.02lf", 
	   rmc_data->sHHMMSS_SS.h,rmc_data->sHHMMSS_SS.m,rmc_data->sHHMMSS_SS.s);
	   str_data[nc+9]=',';
	   nc+=10;
    }  
    
 //Вывести Режим
    if(rmc_data->A=='V'||rmc_data->A=='A'||rmc_data->A=='D')
    {
    	 if(nc+2>size_str_data)return (-1);
	     sprintf(str_data+nc, "%c", rmc_data->A);
	     str_data[nc+1]=',';
    	 nc+=2;
    }else{
    	 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
    }  
    
  //Вывести широту
    if(rmc_data->sBBBB_BBBB_a.gr<0||rmc_data->sBBBB_BBBB_a.gr>90||
	   rmc_data->sBBBB_BBBB_a.m<0||rmc_data->sBBBB_BBBB_a.m>59.99999)
    {
	   if(nc+1>size_str_data)return (-1);
	   str_data[nc]=',';
	   nc++;
    }else{
	   if(nc+10>size_str_data)return (-1);
	   sprintf(str_data+nc, "%02d%05.02lf", 
	   rmc_data->sBBBB_BBBB_a.gr,rmc_data->sBBBB_BBBB_a.m);
	   str_data[nc+7]=',';
	   nc+=8;
    }  
	
//Выведем север или юг	
    if(rmc_data->sBBBB_BBBB_a.a=='N'||rmc_data->sBBBB_BBBB_a.a=='S')
    {
    	 if(nc+2>size_str_data)return (-1);
	     sprintf(str_data+nc, "%c", rmc_data->sBBBB_BBBB_a.a);
	     str_data[nc+1]=',';
    	 nc+=2;
    }else{
    	 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
    }  

  //Вывести долготу
    if(rmc_data->sLLLL_LLLL_a.gr<0||rmc_data->sLLLL_LLLL_a.gr>=180||
	   rmc_data->sLLLL_LLLL_a.m<0||rmc_data->sLLLL_LLLL_a.m>59.99999)
    {
	   if(nc+1>size_str_data)return (-1);
	   str_data[nc]=',';
	   nc++;
    }else{
	   if(nc+11>size_str_data)return (-1);
	   sprintf(str_data+nc, "%03d%05.02lf", 
	   rmc_data->sLLLL_LLLL_a.gr,rmc_data->sLLLL_LLLL_a.m);
	   str_data[nc+8]=',';
	   nc+=9;
    }  
	
//Выведем Запад или восток	
    if(rmc_data->sLLLL_LLLL_a.a=='W'||rmc_data->sLLLL_LLLL_a.a=='E')
    {
    	 if(nc+2>size_str_data)return (-1);
	     sprintf(str_data+nc, "%c", rmc_data->sLLLL_LLLL_a.a);
	     str_data[nc+1]=',';
    	 nc+=2;
    }else{
    	 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
    }  

//Выведе скорость в узлах
    if(rmc_data->v_v<0)
	{
		 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
	}else{
		 if(nc+10>size_str_data)return (-1);
		 sprintf(str_data+nc, "%1.3lf,", rmc_data->v_v);
		 nc=strlen(str_data);
    }	
	
//Выведем наземный курс в градусах
    if(rmc_data->z_z<0)
	{
		 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
	}else{
		 if(nc+10>size_str_data)return (-1);
		 sprintf(str_data+nc, "%1.3lf,", rmc_data->z_z);
		 nc=strlen(str_data);
    }	
	
//Выведем дату
	if(rmc_data->sDDMMYY.dd<1||rmc_data->sDDMMYY.dd>31||
	   rmc_data->sDDMMYY.mm<1||rmc_data->sDDMMYY.mm>12||
	   rmc_data->sDDMMYY.yy<0)
    {
		 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;			
	}else{
		 if(nc+7>size_str_data)return (-1);
         sprintf(str_data+nc,"%02d%02d%02d",rmc_data->sDDMMYY.dd,rmc_data->sDDMMYY.mm,rmc_data->sDDMMYY.yy%100);
		 str_data[nc+6]=',';
		 nc+=7;
	}	

//Выведем магнитное склонение в градусах
	if(rmc_data->x_x_a.x_x<0)
	{
		 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;			
	}else{
		 if(nc+10>size_str_data)return (-1);
		 sprintf(str_data+nc, "%1.3lf,", rmc_data->x_x_a.x_x);
		 nc=strlen(str_data);
	}	
	
//Выведем восток/запад
    if(rmc_data->x_x_a.a=='W'||rmc_data->x_x_a.a=='E')
    {
    	 if(nc+2>size_str_data)return (-1);
	     sprintf(str_data+nc, "%c", rmc_data->x_x_a.a);
	     str_data[nc+1]=',';
    	 nc+=2;
    }else{
    	 if(nc+1>size_str_data)return (-1);
	     str_data[nc]=',';
	     nc++;
    }  
	
//Выведем режим местоопределения
	if(rmc_data->b=='A'||rmc_data->b=='D'||rmc_data->b=='E'||rmc_data->b=='M'||
	   rmc_data->b=='S')
	{
		 if(nc+2>size_str_data)return (-1);
	     sprintf(str_data+nc, "%c", rmc_data->b);
	     str_data[nc+1]='*';
    	 nc+=1;
	}else{
    	 if(nc+1>size_str_data)return (-1);
	     str_data[nc]='*';
	}	
	

 //Подготовить и вывести битовую маску разрешенных  протоколов
    
    
 //Теперь расчитаем контрольную сумму   
    for(i=1;i<nc;i++)
    {
	  ctr_summ^=(unsigned char)(str_data[i]);
    }
    nc++;
    sprintf(str_data+nc, "%02X",ctr_summ);
    nc+=2;
    str_data[nc]=0x0D;
    str_data[nc+1]=0x0A;
    nc+=2;
    return nc;
					
}



int rmcGetDataRMC(
		const struct RMC_DATA *rmc_data,   //Указатель 
		int indx,						   //Индекс	
		unsigned char *out_bytes,		   //Выходной байт	
		int max_sz_out_bytes,			   //размер массива	
		int *sz_out_bytes,				   //Размер данных в байтах
		struct TTDateTime *dt			   //Время получения пакета	 
				)				
{
	if(rmc_data==0||max_sz_out_bytes<4)return 0;
	if(indx<1||indx>9)return -1;
	
	switch(indx)
	{
		case 1:
            if((int)sizeof(rmc_data->sHHMMSS_SS)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(rmc_data->sHHMMSS_SS),sizeof(rmc_data->sHHMMSS_SS));
			*sz_out_bytes=sizeof(rmc_data->sHHMMSS_SS);
		break;
		
		case 2:
		    ((char*)out_bytes)[0]=rmc_data->A;
			*sz_out_bytes=1;
		break;
		
		case 3:
            if((int)sizeof(rmc_data->sBBBB_BBBB_a)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(rmc_data->sBBBB_BBBB_a),sizeof(rmc_data->sBBBB_BBBB_a));
			*sz_out_bytes=sizeof(rmc_data->sBBBB_BBBB_a);
		break;
		
		case 4:
            if((int)sizeof(rmc_data->sLLLL_LLLL_a)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(rmc_data->sLLLL_LLLL_a),sizeof(rmc_data->sLLLL_LLLL_a));
			*sz_out_bytes=sizeof(rmc_data->sLLLL_LLLL_a);
		break;
		
		case 5:
            if((int)sizeof(rmc_data->v_v)>max_sz_out_bytes)return (-2);
			*((double*)(out_bytes))=rmc_data->v_v;
			*sz_out_bytes=sizeof(rmc_data->v_v);
		break;	
		
		case 6:
            if((int)sizeof(rmc_data->z_z)>max_sz_out_bytes)return (-2);
			*((double*)(out_bytes))=rmc_data->z_z;
			*sz_out_bytes=sizeof(rmc_data->z_z);
		break;	
			
		case 7:
            if((int)sizeof(rmc_data->sDDMMYY)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(rmc_data->sDDMMYY),sizeof(rmc_data->sDDMMYY));
			*sz_out_bytes=sizeof(rmc_data->sDDMMYY);
		break;
		
		case 8:
            if((int)sizeof(rmc_data->x_x_a)>max_sz_out_bytes)return (-2);
			memcpy(out_bytes,&(rmc_data->x_x_a),sizeof(rmc_data->x_x_a));
			*sz_out_bytes=sizeof(rmc_data->x_x_a);
		break;
		
		case 9:
			((char*)out_bytes)[0]=rmc_data->b;
			*sz_out_bytes=1;
		break;
		
		default:
			return (-3);
		
	};
		
	memcpy(dt,rmc_data+indx,sizeof(struct TTDateTime));



	return 1;
}				


int rmcPrintfRMC(void *fp, 		//Дескриптор файл
			const struct RMC_DATA *rmc_data)
{
	if(rmc_data->dt[0].mm<1||rmc_data->dt[0].dd<1)
	{
		fprintf((FILE*)fp,"\nNo data\n");
		return 1;
	}	
		
	if(rmc_data->ns==0)
	{
		fprintf((FILE*)fp,"\nGPS\n");
	}else 
	if(rmc_data->ns==1)
	{
		fprintf((FILE*)fp,"\nGLONASS\n");
	}else
	if(rmc_data->ns==2){
		fprintf((FILE*)fp,"\nGPS+GLONASS\n");
	}else{
		fprintf((FILE*)fp,"\nNot IEC protocol\n");
	}	
	if(rmc_data->dt[1].mm<1||rmc_data->dt[1].dd<1)
	{
		fprintf((FILE*)fp,"Time: No info \n");
	}else{
		fprintf((FILE*)fp, "Time: %02d:%02d:%05.02lf\n",
		    rmc_data->sHHMMSS_SS.h, rmc_data->sHHMMSS_SS.m,rmc_data->sHHMMSS_SS.s);
	}	
	
	if(rmc_data->dt[2].mm<1||rmc_data->dt[2].dd<1)
	{
		fprintf((FILE*)fp,"Status: No info \n");
	}else{
		fprintf((FILE*)fp,"Status: %c\n", rmc_data->A);
	}
	
	if(rmc_data->dt[3].mm<1||rmc_data->dt[3].dd<1)
	{
		fprintf((FILE*)fp,"Latitude: No info \n");
	}else{
		char str[30];
		iecLLMakeStringFromLatOrLong((void*)(&(rmc_data->sBBBB_BBBB_a)),str,30);
		fprintf((FILE*)fp,"Latitude: %s\n",str);
	}
	
	if(rmc_data->dt[4].mm<1||rmc_data->dt[4].dd<1)
	{
		fprintf((FILE*)fp,"Longitude: No info \n");
	}else{
		char str[30];
		iecLLMakeStringFromLatOrLong((void*)(&(rmc_data->sLLLL_LLLL_a)),str,30);
		fprintf((FILE*)fp,"Longitude: %s\n",str);
	}
	
//Выведем наземную скорость
	if(rmc_data->dt[5].mm<1||rmc_data->dt[5].dd<1)
	{
		fprintf((FILE*)fp,"V: No info \n");
	}else{
		fprintf((FILE*)fp,"V: %g\n",rmc_data->v_v);
	}

//Наземный курс в градусах
	if(rmc_data->dt[6].mm<1||rmc_data->dt[6].dd<1)
	{
		fprintf((FILE*)fp,"Z: No info \n");
	}else{
		fprintf((FILE*)fp,"Z: %g\n",rmc_data->z_z);
	}

//Дата 
	if(rmc_data->dt[7].mm<1||rmc_data->dt[7].dd<1)
	{
		fprintf((FILE*)fp,"Date: No info \n");
	}else{
		fprintf((FILE*)fp,"Date: %02d.%02d.%04d\n",
		   rmc_data->sDDMMYY.dd,rmc_data->sDDMMYY.mm,rmc_data->sDDMMYY.yy);		
	}	
	
//Магнитное склонение в градусах
	if(rmc_data->dt[8].mm<1||rmc_data->dt[8].dd<1)
	{
		fprintf((FILE*)fp,"Magnetic declination: No info \n");
	}else{
		fprintf((FILE*)fp,"Magnetic declination: %lf %c\n",
		   rmc_data->x_x_a.x_x, rmc_data->x_x_a.a);		
	}
	
//Режим местоопределения
	if(rmc_data->dt[9].mm<1||rmc_data->dt[9].dd<1)
	{
		fprintf((FILE*)fp,"Regim: No info \n");
	}else{
		fprintf((FILE*)fp,"Regim: %c\n", rmc_data->b);
	}
	return 1;
}			
