﻿/*Описание сообщения типа видимые спутники*/


#ifndef __GSV_H__
#define __GSV_H__

#include "time_tools.h"

struct GSV_DATA
{
      int ns;		//Тип навигационной системы. 0 - GPS, 1 - ГЛОНАС, 2 GPS+Глонас
      int n;		//Общее число сообщений
      int m;		//Номер текущего сообщения
      int pp	;	//Общее число НКА в зоне видимости
      int kk[4];		//Номер НКА
      int gg[4];		//Возвышение над горизонтом (от 0 до 90), градусы
      int yyy[4];		//Азимут, градусы (от 0 до 359)
      int xx[4];		//сигнал/шум (от 00 до 99), дБГц; ноль, если НКА не сопровождается
      
      
};

#ifdef __cplusplus
extern "C"
{
#endif	
int gsvRazborGSV(
		   const char *str_data,
		    struct GSV_DATA *gsv_data);
#ifdef __cplusplus
}
#endif


#endif
