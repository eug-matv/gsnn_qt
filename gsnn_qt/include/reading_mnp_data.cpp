﻿#include <string.h>
#include "reading_mnp_data.h"

int TFIFO_MNP_DATA::addByte(unsigned char b)
{
    unsigned short *usArray;
	unsigned int sum;
    int i;
    unsigned char *temp;
    
    if(szNewData<10)
    {  
      
    	if(szNewData==0)
	    {
	        if(b==0xFF)
    	    {	
        		ucNewData[0]=0xFF;
	     	    szNewData=1;
                return 3;
			}
	        return 0;
	    }
    
	    if(szNewData==1)
	    {
            if(b==0x81)
	        {
		       ucNewData[1]=0x81;
		       szNewData=2;
		       return 1;
             }
	         szNewData=0;
	         return 0;
		}
	    if(szNewData==5)
	    {
	       ucNewData[5]=b;
	       szNewData++;
	       szWaittingDataWords=(int)(*((unsigned short*)(ucNewData+4)));
	       if(szWaittingDataWords>MNP_KADR_MAXSIZE)
	       {
		       szNewData=0;
		       return 0;
	       }
	       iGotDataWords=0;
	       return 1;
	     }  
    
    
		if(szNewData==9)
		{
//Пришел второй байт контрольной суммы
			ucNewData[9]=b;
			szNewData++;
//Проверим контрольную сумму
			usArray=(unsigned short*)ucNewData;
			sum=(unsigned int)(usArray[0]);
			for(i=1;i<5;i++)
			{
				sum+=(unsigned int)(usArray[i]);
			}
			if((sum&0xFFFF)==0)
			{ 	
				if(szWaittingDataWords==0)
				{
					szReceivedData=szNewData;
					temp=ucReceivedData;
					ucReceivedData=ucNewData;
					ucNewData=temp;
					szNewData=0;
					return 2;
				}  
				return 1;
			}
//А тут надо попробывать найти символ 0xFF
			for(i=0;i<10;i++)
			{
				if(ucNewData[i]==0xFF)
				{
					if(i==9)
					{
						ucNewData[0]=0xFF;
						szNewData=1;
						return 3;
					}else{
						if(ucNewData[i+1]==0x81)
						{
							memmove(ucNewData, ucNewData+i,10-i);
							szNewData=10-i;
							return 3;
						}
					}	
				}
			}
			szNewData=0;
			return 0;
		}
		ucNewData[szNewData++]=b;	//Добавим новый символ
		return 1;
	}	
      
    ucNewData[szNewData++]=b;
    
    
    if(szNewData%2==0&&(szNewData-10)/2>szWaittingDataWords)
    {
 //Явно получена контрольная сумма
	   usArray=(unsigned short*)(ucNewData+10);
	   sum=(unsigned int)usArray[0];
	   for(i=1;i<=szWaittingDataWords;i++)
	   {
	       sum+=(unsigned int)(usArray[i]);
	   }
	   if((sum&0xFFFF)!=0)
	   {
			
		   for(i=2;i<szNewData;i++)
		   { 	   
			   if(ucNewData[i]==0xFF&&
			         (i==(szNewData-1)||ucNewData[i+1]==0x81))
			   {		 
					unsigned char *temp_uchar;
					int sz_temp_uchar=szNewData-i;
                    int temp_iRet=0,temp_i;
					temp_uchar=new unsigned char [sz_temp_uchar];
					memcpy(temp_uchar,ucNewData+i,sz_temp_uchar);  
					szNewData=0;
			
					for(temp_i=0;temp_i<sz_temp_uchar;temp_i++)
					{
						temp_iRet=this->addByte(temp_uchar[temp_i]);
					}	
					delete []temp_uchar; //Очистим память
					return temp_iRet;
			   }	
		   }
	   }
     
	   szReceivedData=szNewData;
       temp=ucReceivedData;
	   ucReceivedData=ucNewData;
	   ucNewData=temp;
	   szNewData=0;
	   return 2;
    }
    return 1;
}



 int TFIFO_MNP_DATA::
	getReceievedData(int &type_of_kadr,  //Тип кадра
			 int &size_of_data,  //Размер данных в словах (по 2 байта)
			 unsigned short &mask2200, //Маска для кадра 2200,
			 unsigned short *data	  //Данные, которые были получены	
			  )
{
    if(szReceivedData>=10)
    {
    	type_of_kadr=(int)(*((unsigned short*)(ucReceivedData+2)));
     	size_of_data=(int)(*((unsigned short*)(ucReceivedData+4)));
      	mask2200=*((unsigned short*)(ucReceivedData+6));
       	if(size_of_data>0)
	    {
	         memcpy(data, ucReceivedData, size_of_data*2);
        }
	    return 1;
    }
    return 0;
}
