﻿#include <stdio.h>
#include <string.h>
#include "pirtr.h"


/*Если успешно, то возвращает полное число байт включая стартовый и два конечных <CR>  и <LF>.
0 - если ошибка в данных и пакет не получен,
и -1 -- если не хватило по длине size_data<, чем реальная длина пакетов
*/
int pirtrMakePIRTR(const struct PIRTR_DATA *pirtr_data, 
		   char *str_data, int size_str_data)
{
	
    int nc=0;
    int i;

    unsigned char ctr_summ=0;
    if(nc+2>size_str_data)return (-1);
    str_data[nc]='$';
    nc+=1;
//Вывести PIRTR 
    if(nc+6>size_str_data)return (-1);
    memcpy(str_data+nc,"PIRTR,", 6);
    nc+=6;
    
//Вывести формат сообшения
    if(pirtr_data->a<0||pirtr_data->a>4)
    {
    	if(nc+1>size_str_data)return (-1);
     	str_data[nc]=',';
      	nc++;
    }else{
    	if(nc+2>size_str_data)return (-1);
     	sprintf(str_data+nc, "%d", (pirtr_data->a)%2);
      	str_data[nc+1]=',';
       	nc+=2;
    }  
    
 //Вывести разницу во времени
    if(pirtr_data->sHHMM.hh<0)
    {
    	if(nc+10>size_str_data)return (-1);
     	sprintf(str_data+nc, "%03d%02d*", pirtr_data->sHHMM.hh, pirtr_data->sHHMM.mm);
      	nc+=5;
    }else{
    	if(nc+9>size_str_data)return (-1);
     	sprintf(str_data+nc, "%02d%02d*",pirtr_data->sHHMM.hh, pirtr_data->sHHMM.mm); 
      	nc+=4;
    }
    
    
 //Теперь расчитаем контрольную сумму   
    for(i=1;i<nc;i++)
    {
	ctr_summ^=(unsigned char)(str_data[i]);
    }
    nc++;
    sprintf(str_data+nc, "%02X",ctr_summ);
    nc+=2;
    str_data[nc]=0x0D;
    str_data[nc+1]=0x0A;
    nc+=2;
    return nc;
}
