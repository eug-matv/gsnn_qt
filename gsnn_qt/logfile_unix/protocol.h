﻿//---------------------------------------------------------------------------
//#pragma once
#ifndef _protocol_h_
#define _protocol_h_

#define _CRT_SECURE_NO_WARNINGS
#define prtcl_char char
class TProtocol
{
private:
    bool noflush;
//	struct
//	{
//		unsigned int line;
//		prtcl_char text[10][2560];
//	} noflush;
public:
    int NUM;
/*
    HANDLE HND;
    DWORD thID;
    prtcl_char ProtokolFile[64];
    prtcl_char StartMes[128];
    prtcl_char StopMes[128];
    SYSTEMTIME Local;
    SYSTEMTIME LastSave;
    SYSTEMTIME Start;
    prtcl_char SaveFile[1024];
    prtcl_char tpmFile[1024];
    prtcl_char DirName[512];
    bool newdata;
    bool filebusy;
    bool PrtThreadWork;
    bool stop;
    FILE *fp_tmp;
    //TProtocol(prtcl_char*, prtcl_char*, int, prtcl_char*, prtcl_char*, prtcl_char*, prtcl_char*);
*/
    TProtocol(){};
    ~TProtocol(){};
    void ProtocolRoutine(){};
    void sWriteInfo(prtcl_char * dt, ...){};
    void WriteInfo(prtcl_char * dt){};
    void WriteInfoWithoutData(prtcl_char * dt){};
    void Init(prtcl_char*, prtcl_char*, int, prtcl_char*, prtcl_char*, prtcl_char*, prtcl_char*){};
    void Free(){};
  //  static DWORD WINAPI PrtThread(LPVOID);
};
extern TProtocol *Protocol;
extern TProtocol *lpStateProtocol;
extern TProtocol *Log;

int WriteProtocol(prtcl_char *);

struct _msg
{
    prtcl_char Device[64];
    prtcl_char Text[256];
};

#define MessageNum 20
/*
class _Message
{
public:
    int count;
    _msg msg[MessageNum];
    _msg* p_wr;
    _msg* p_rd;
    _Message();
    TProtocol *Protocol;
    void Write(prtcl_char*, va_list);
    void Read(prtcl_char*);
};
*/
#endif

