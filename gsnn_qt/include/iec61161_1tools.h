﻿
#ifndef   __IEC61161_1TOOLS_H__
#define  __IEC61161_1TOOLS_H__

#include "time_tools.h"

#define MAX_NMEA_SIZE  2512        


#define NMEA_GPGGA       1
#define NMEA_GPGSA       2
#define NMEA_GPGSV       3
#define NMEA_GPRMC       4
#define NMEA_GPVTG       5
#define NMEA_GPGLL       6
#define NMEA_GPZDA       7
#define NMEA_GLGGA       11
#define NMEA_GLGSA       12
#define NMEA_GLGSV       13
#define NMEA_GLRMC       14
#define NMEA_GLVTG       15
#define NMEA_GLGLL       16
#define NMEA_GLZDA       17
#define NMEA_GNGGA       21
#define NMEA_GNGSA       22
#define NMEA_GNGSV       23
#define NMEA_GNRMC       24
#define NMEA_GNVTG       25
#define NMEA_GNGLL       26
#define NMEA_GNZDA       27
#define IEC_PIRPR      101
#define IEC_PIRTR      102
#define IEC_PIRSR      103
#define IEC_PIREA      104
#define IEC_PIRFV      105
#define IEC_PIRGK      106
#define IEC_PIRRA      107
#define IEC_FAULT   -1


struct TIEC_String_Data
{
	int type;				//Тип данных. Определен выше
	char *str_data;			//Строковые данные
	int sz_str_data;		//Число байт
	int max_sz_str_data;  //Максимальный размер данных
	struct TTDateTime *dt;	//Время получения каждого из элементов. Время с индексом
	int sz_dt;				//Число элементов sz_dt
}; 


#ifdef __cplusplus
extern "C" 
{
#endif	


int iecToolsGetTime(const char *strka, 
		    int *hh, int *mm,double *ssss);

		    
		    

int iecToolsGetLatitudeGrMin(const char *strka, 
		    int *graduses, double *minutes);
		    
		    
//Получение даты
int iecToolsGetDate(const char *strka, 
		    int *dd, int *mm,int *yy);
		    

/*Получение типа данных в зависимости от входной строки. Согласно определению NMEA_* и IEC_* */
int iecToolsTypeOfPackage(const unsigned char *data);

		    
/*Процедуры работы с данными типа TIEC_String_Data. - пока под вопросом*/
 struct TIEC_String_Data* iecInitIEC_String_Data(int type);

 int iecDestroyIEC_String_Data(struct TIEC_String_Data *iec);
 
 int iecRenewIEC_String_Data(struct TIEC_String_Data *iec,  //Данные структуры 
                             const char *new_data_str,      //Новая добавляемая строка
							 int sz_new_data_str,		    //Размер добавляемой строки
							 const struct TTDateTime *dt    //Добавляемое время
							 );
 


			
#ifdef __cplusplus
}
#endif

		

#endif
