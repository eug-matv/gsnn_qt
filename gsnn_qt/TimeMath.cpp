﻿
#include <time.h>
#include <math.h>

#include "time_tools.h"
#include "TimeMath.h"

#undef UNICODE
#undef _UNICODE

#include <QtCore>

//------------------------------------------------------------
/* Проверка правильного заполнения структуры времени TTDateTime */
bool IsUTCTimeValid(TTDateTime utc)
{
    int daysInMonth;

    if( utc.mm == 0 || utc.mm > 12 )
        {
        qDebug( "Error: if( utc_month == 0 || utc_month > 12 )" );
        return FALSE;
        }

    daysInMonth = GetNumberOfDaysInMonth(utc.mm, utc.yyyy);

    if (daysInMonth<=0) // проверка возвращаемого значения
        {
        qDebug("Error: day in mounth return -1");
        return FALSE;
        }

    if( utc.dd == 0 || utc.dd > daysInMonth ) // дней в месяце [0...daysInMonth]
        {
        qDebug( "Error: if( utc_day == 0 || utc_day > daysInMonth )" );
        return FALSE;
        }
    if( utc.h > 23 )
        {
        qDebug( "Error: if( utc_hour > 23 )" ); // часов в сутках [0...23]
        return FALSE;
        }
    if( utc.m > 59 )
        {
        qDebug( "Error: if( utc_minute > 59 )" ); // минут в часе [0...59]
        return FALSE;
        }
    if( utc.s > 60 )
        {
        qDebug( "Error: if( utc_seconds > 60 )" ); // секунд в минуте[0...60],
        // 60 - високосная секунда, добавляется по указанию службы наблюдения
        // за вращением земли 30 июня или 31 декабря, так как Земля замедляет
        // вращение, високосная секунда может также отниматься, такого еще не было
        return FALSE;
        }

  return TRUE;
}

//-------------------------------------------------------------------
/* Функция возвращает количество дней по указанному месяцу и году */
int GetNumberOfDaysInMonth(int month, int year)
{
    bool is_a_leapyear;
    int days = 0;

    is_a_leapyear = IsLeapYear(year); // високосный ли год

    switch(month)
        {
        case  1: days = DAYS_IN_JAN; break;
        case  2: if( is_a_leapyear ) { days = 29; } else { days = 28; }break;
        case  3: days = DAYS_IN_MAR; break;
        case  4: days = DAYS_IN_APR; break;
        case  5: days = DAYS_IN_MAY; break;
        case  6: days = DAYS_IN_JUN; break;
        case  7: days = DAYS_IN_JUL; break;
        case  8: days = DAYS_IN_AUG; break;
        case  9: days = DAYS_IN_SEP; break;
        case 10: days = DAYS_IN_OCT; break;
        case 11: days = DAYS_IN_NOV; break;
        case 12: days = DAYS_IN_DEC; break;
        default:
            {
            qDebug( "Unexpected default month case." );
            return -1;
            break;
            }
        }
    return days;
}

//-------------------------------------------------------------------
/* Функция проверяет, является ли указанный год високосным */
bool IsLeapYear(int year)
{
    int leap_year;
    leap_year = ((year%4==0) && (year%100!=0)) || (year%400==0);

    if(leap_year == 1)
        {
        return TRUE;
        }
    else
        {
        return FALSE;
        }
}

//-------------------------------------------------------------------
/* Функция определяет какой по счету день в году */
int GetDayOfYear(int day, int month, int year)
{
    int dayofyear = 0;

    // определим количество дней в феврале текущего года
    int days_in_feb = 0;
    days_in_feb = GetNumberOfDaysInMonth(year, 2);

    if(days_in_feb <= 0) // ошибка определения количества дней
        {
        qDebug("Error: day in FEBRARY returns -1");
        return -1;
        }

    switch(month)
        {
        case  1: dayofyear = day; break;
        case  2: dayofyear = DAYS_IN_JAN + day; break;
        case  3: dayofyear = DAYS_IN_JAN + days_in_feb + day; break;
        case  4: dayofyear = 62  + days_in_feb + day; break;
        case  5: dayofyear = 92  + days_in_feb + day; break;
        case  6: dayofyear = 123 + days_in_feb + day; break;
        case  7: dayofyear = 153 + days_in_feb + day; break;
        case  8: dayofyear = 184 + days_in_feb + day; break;
        case  9: dayofyear = 215 + days_in_feb + day; break;
        case 10: dayofyear = 245 + days_in_feb + day; break;
        case 11: dayofyear = 276 + days_in_feb + day; break;
        case 12: dayofyear = 306 + days_in_feb + day; break;
        default:
            {
            qDebug( "unexpected default month case." );
            return -1;
            break;
            }
        }
    return dayofyear;
}

//-----------------------------------------------------------------
/* Получение Юлианской даты из структуры TTDateTime */
bool GetJulianDateFromUTC(TTDateTime utc, long double *julian_date)
{

    double y; // temp for year
    double m; // temp for month
    bool result;

    // Check the input.
    result = IsUTCTimeValid(utc);
    if( result == FALSE )
        {
        qDebug("TTDateTimeValid returned FALSE.");
        return FALSE;
        }

    /*
    Алгоритм перевода в Юлианскую дату и обратно взят из книги:
    Hofmann-Wellenhof, B., H. Lichtenegger, and J. Collins (1994). GPS Theory and
    Practice, Third, revised edition. Springer-Verlag, Wien New York. pp. 38-42

    Примечание:
    GPS и Glonass имеют схожие алгоритмы формирования совственного времени,
    отличия есть только в годе начала отсчета,
    В данной программе не ведется учет собственного времени навигационных систем
    ибо не нужно, гравитационные возмущения луны и солнца не считаем
    */

    if( utc.mm <= 2 )
        {
        y = utc.yyyy - 1;
        m = utc.mm + 12;
        }
    else
        {
        y = utc.yyyy;
        m = utc.mm;
        }

    *julian_date = (int)(365.25*y) + (int)(30.6001*(m+1.0)) + utc.dd +
                   utc.h/24.0 + utc.m/1440.0 + utc.s/86400.0 + 1720981.5;
    return TRUE;
}

//-------------------------------------------------------------------
/* Преобразование Юлианской даты  в структуру TTDateTime */
bool GetUTCfromJulianDate (long double julian_date, TTDateTime *utc)
{
    int a, b, c, d, e; // промежуточные коэффициенты

    int year;
    int month;
    int day;
    int hour;
    int minute;
    int days_in_month = 0;
    double td; // double буфер для расчета
    double seconds;

    // Проверка формата данных
    if( julian_date < 0.0 )
        {
        qDebug( "Error: julian_date < 0.0 !" );
        return FALSE;
        }

    /*
    Алгоритм перевода в Юлианскую дату и обратно взят из книги:
    Hofmann-Wellenhof, B., H. Lichtenegger, and J. Collins (1994). GPS Theory and
    Practice, Third, revised edition. Springer-Verlag, Wien New York. pp. 38-42

    Примечание:
    GPS и Glonass имеют схожие алгоритмы формирования совственного времени,
    отличия есть только в годе начала отсчета,
    В данной программе не ведется учет собственного времени навигационных систем
    ибо не нужно, гравитационные возмущения луны и солнца не считаем
    */

    // Считаем промежуточные коэффициенты
    a = (int)(julian_date+0.5);
    b = a + 1537.0;
    c = (int)( ((double)b-122.1)/365.25 );
    d = (int)(365.25*c);
    e = (int)( ((double)(b-d))/30.6001 );

    td      = b - d - (int)(30.6001*e) + fmod( (double)(julian_date)+0.5, (double)(1.0) );   // [days]
    day     = (int)td;
    td     -= day;
    td     *= 24.0;        // [hours]
    hour    = (int)td;
    td     -= hour;
    td     *= 60.0;        // [minutes]
    minute  = (int)td;
    td     -= minute;
    td     *= 60.0;        // [s]
    seconds = td;
    month   = (int)(e - 1.0 - 12.0*(int)(e/14.0));
    year    = (int)(c - 4715.0 - (int)( (7.0+(double)month) / 10.0 ));

    // Проверка на переполнение числовых полей
    if( seconds >= 60.0 )
        {
        seconds -= 60.0;
        minute++;
        if( minute >= 60 )
            {
            minute -= 60;
            hour++;
            if( hour >= 24 )
                {
                hour -= 24;
                day++;

                days_in_month = GetNumberOfDaysInMonth(month, year);

                if (days_in_month <= 0)
                    {
                    qDebug("Error: day in mounth return -1");
                    return FALSE;
                    }

                if( day > days_in_month )
                    {
                    day = 1;
                    month++;
                    if( month > 12 )
                        {
                        month = 1;
                        year++;
                        }
                    }
                }
            }
        }

    // Присвоение
    utc->yyyy   = year;
    utc->mm     = month;
    utc->dd     = day;
    utc->h      = hour;
    utc->m      = minute;
    utc->s      = seconds;

    return TRUE;
}

//-------------------------------------------------------------------
/* Установить системное время UTC для виндовс */

#ifdef _WIN32
bool SetSystemTimeFromUTC(TTDateTime utc) //! WinAPI
{
    bool result;
    SYSTEMTIME t;

    if(!IsUTCTimeValid(utc))
        {
        return FALSE;
        }

    t.wYear     = utc.yyyy;
    t.wMonth    = utc.mm;
    t.wDay      = utc.dd;
    t.wHour     = utc.h;
    t.wMinute   = utc.m;
    t.wSecond   = (WORD)(floor(utc.s));
    t.wMilliseconds = (WORD)((utc.s - t.wSecond)*1000);

    // Set the PC system time.
    result = SetSystemTime( &t );

    return result;

    // POSIX BSD: settimeofday
}
#else
bool SetSystemTimeFromUTC(TTDateTime utc) //! Linux
{
    return false;
}
#endif

//-------------------------------------------------------------------
/* Преобразование структуры времени TTDateTime в формат времени виндовс */


bool ConvertUTCToSystemTime(TTDateTime utc, SYSTEMTIME *t) //! WinAPI
{
    if(IsUTCTimeValid(utc))
        {
        t->wYear    = utc.yyyy;
        t->wMonth   = utc.mm;
        t->wDay     = utc.dd;
        t->wHour    = utc.h;
        t->wMinute  = utc.m;
#ifdef _WIN32
        t->wSecond  = (WORD)(floor(utc.s));
        t->wMilliseconds = (WORD)((utc.s - t->wSecond)*1000);
#else
        t->wSecond = (long)(floor(utc.s));
        t->wMilliseconds = (long)((utc.s - t->wSecond)*1000);
#endif

        return TRUE;
        }
    return FALSE;
}


//--------------------------------------------------------------------
/* Преобразование структуры времени виндовс в TTDateTime */

bool ConvertSystemTimeToUTC(SYSTEMTIME t, TTDateTime *utc) //! WinAPI
{
    utc->yyyy   = t.wYear;
    utc->mm     = t.wMonth;
    utc->dd     = t.wDay;
    utc->h      = t.wHour;
    utc->m      = t.wMinute;
    utc->s      = t.wSecond + 0.001 * t.wMilliseconds;
    return true;
}



//--------------------------------------------------------------------
/* Функция добывает данные о названии временной зоны */
//! непроверенно
#ifdef _WIN32
bool GetTimeZoneName(char *zone_name) //! WinAPI
{
    TIME_ZONE_INFORMATION TimeZoneInfo; // данные о часовом поясе
    LPTIME_ZONE_INFORMATION TimeZone = &TimeZoneInfo; // Указатель

    ZeroMemory(TimeZone, sizeof(TimeZoneInfo)); // Очищаем память

    if(GetTimeZoneInformation(TimeZone) == TIME_ZONE_ID_UNKNOWN)
        {
        qDebug("Error: Can't get TimeZoneInformation");
        return FALSE;
        }

    int i = 0;

    while(TimeZone->StandardName[i] != '\0' || (i > 32))
    {
        zone_name[i] = (char)TimeZone->StandardName[i];
        if(i > 32)
            {
            zone_name[i] = '\0';
            }
        ++i;
    }

    return TRUE;
}
#endif

//--------------------------------------------------------------------
/* Получение Юлианской даты из данных о часовом поясе и переходе
на сезонное время в ОС Виндовс*/
#ifdef _WIN32
bool GetJulianDateFromTimezoneInfo(long double *julian_date) //! WinAPI
{
    TIME_ZONE_INFORMATION TimeZoneInfo; // данные о часовом поясе
    LPTIME_ZONE_INFORMATION TimeZone = &TimeZoneInfo; // Указатель

    ZeroMemory(TimeZone, sizeof(TimeZoneInfo)); // Очищаем память

    if(GetTimeZoneInformation(TimeZone) == TIME_ZONE_ID_UNKNOWN)
        {

        return FALSE;
        }

    //int TimeZoneH = 0; // - разница в часах
    int TimeZoneM = 0; // и митутах

    //! UTC = local time + bias
    //! разница в TimeZoneInformation хранится со знаком минус (-300 минут для Ебурга)
    //! daylightBias - данные о переходе на летнее время в минутах (-60 минут, если летнее время)

    //! После отмены перехода на зимнее время будет
    //! TimeZoneInformation = -360 для Ебурга (-360 в Виндовс XP и 2K соответсявует сейчас Омску!)
    //! daylightBias = 0 (не выполяется переход)

    //TimeZoneH = (TimeZone->Bias + TimeZone->DaylightBias)/60.0;
    //TimeZoneM = (TimeZone->Bias + TimeZone->DaylightBias) - TimeZoneH*60.0;

    //*julian_date = (TimeZoneH/24.0 + TimeZoneM/1440.0);
    TimeZoneM = (double)(TimeZone->Bias + TimeZone->DaylightBias);
    if(julian_date)
    {
        *julian_date = TimeZoneM/1440.0;
    }
    //delete TimeZoneInfo;
    return TRUE;
}
#else
bool GetJulianDateFromTimezoneInfo(long double *julian_date) //! Linux
{
    int TimeZoneM = 0; // и митутах
//Установим глобальные переменные timezone и daylight. Для этого используется функция tzset из time.h
//Значения timezone и daylight в секундах
    tzset();
    TimeZoneM=(double)((timezone+daylight)/60);
    *julian_date = TimeZoneM/1440.0;

    //delete TimeZoneInfo;
    return TRUE;
}
#endif

//--------------------------------------------------------------------
// получить Юлианскую дату из структуры tm библиотеки "time.h"
bool GetJulianDateFromTM(tm datatime, long double *julian_date) //! недоделана
{
    double y; // temp for year
    double m; // temp for month
    double sec; // temp for sec


    /*
    Алгоритм перевода в Юлианскую дату и обратно взят из книги:
    Hofmann-Wellenhof, B., H. Lichtenegger, and J. Collins (1994). GPS Theory and
    Practice, Third, revised edition. Springer-Verlag, Wien New York. pp. 38-42

    Примечание:
    GPS и Glonass имеют схожие алгоритмы формирования совственного времени,
    отличия есть только в годе начала отсчета,
    В данной программе не ведется учет собственного времени навигационных систем
    ибо не нужно, гравитационные возмущения луны и солнца не считаем
    */

    sec = datatime.tm_sec + 0.001;

    if( datatime.tm_mon <= 2 )
        {
        y = datatime.tm_year - 1.0;
        m = datatime.tm_mon + 12.0;
        }
    else
        {
        y = datatime.tm_year;
        m = datatime.tm_mon;
        }

    *julian_date = (int)(365.25*(y+1970.0)) + (int)(30.6001*(m+1.0)) + datatime.tm_mday +
                   datatime.tm_hour/24.0 + datatime.tm_min/1440.0 + sec/86400.0 + 1720981.5;
    return TRUE;
}

//--------------------------------------------------------------------
#ifdef _WIN32
bool GetJulianDateFromCurentSystemTime(long double *julian_date)
{
    SYSTEMTIME t;

    GetSystemTime(&t);

    double y; // temp for year
    double m; // temp for month

    double sec; // temp for sec



    /*
    Алгоритм перевода в Юлианскую дату и обратно взят из книги:
    Hofmann-Wellenhof, B., H. Lichtenegger, and J. Collins (1994). GPS Theory and
    Practice, Third, revised edition. Springer-Verlag, Wien New York. pp. 38-42

    Примечание:
    GPS и Glonass имеют схожие алгоритмы формирования совственного времени,
    отличия есть только в годе начала отсчета,
    В данной программе не ведется учет собственного времени навигационных систем
    ибо не нужно, гравитационные возмущения луны и солнца не считаем
    */

    if( t.wMonth <= 2 )
        {
        y = (double)(t.wYear - 1.0);
        m = (double)(t.wMonth + 12.0);
        }
    else
        {
        y = t.wYear;
        m = t.wMonth;
        }

    sec = t.wSecond + 0.001*t.wMilliseconds;

    *julian_date = (int)(365.25*y) + (int)(30.6001*(m+1.0)) + t.wDay +
                   t.wHour/24.0 + t.wMinute/1440.0 + sec/86400.0 + 1720981.5;
    return TRUE;
}
#else
//Матвеенко Е.А.: для совместимости с Linux
bool GetJulianDateFromCurentSystemTime(long double *julian_date)
{
    struct TTDateTime utc;
//1 - получим время utc
    if(ttGetLocalTime(&utc)<=0)
    {
           qDebug("Error: Can't get ttGetLocalTime");
           return FALSE;
    }
//2 - получение юлианского времени
    return GetJulianDateFromUTC(utc,julian_date);
}
#endif
