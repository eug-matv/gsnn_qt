﻿#ifndef __MNP_KADR_H__
#define __MNP_KADR_H__

#define MNP_KADR_MAXSIZE 	2000

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "time_tools.h"




class TMNPKadr
{
    uint16_t data[MNP_KADR_MAXSIZE];
    int data_size;			//Размер данных в словах (по два байта)
	
protected:
	struct TTDateTime dt;   //Время получения последнего пакета данных

	
public:
    TMNPKadr()
    {
                int i;
                unsigned char *uc=(unsigned char*)(&dt);
		data_size=0;
                //В Buildere memset не работает
                //memset(&dt,0,sizeof(struct TTDateTime));
                for(i=0;i<(int)(sizeof(TTDateTime));i++)
                {
                        uc[0]=0;
                }
    };

    int setData(const void *d, int size_in_words, const struct TTDateTime &dt1);
    int16_t getIData(int n_word)
    {
	        if(n_word<0||n_word>=data_size)
	        {
                return -(0x7FFF);
            }
	        return *((int16_t*)(data+n_word));    
    };
    uint16_t getUIData(int n_word)
    {
	         if(n_word<0||n_word>=data_size)
	         {
                  return 0xFFFF;
             }
	         return (data[n_word]);    
    };  

    int32_t getDIData(int n_word)
    {
	    if(n_word<0||n_word>=data_size-1)
	    {
	       return -(0x7FFFFFF);
	    }
	    return *((int32_t*)(data+n_word));
    };
    
    uint32_t getUDIData(int n_word)
    {
	    if(n_word<0||n_word>=data_size-1)
	    {
	       return 0xFFFFFFF;
	    }
	    return *((uint32_t*)(data+n_word));
    };

    float getFData(int n_word)
    {
	   if(n_word<0||n_word>=data_size-1)
	   {
	      return -1e32;
	   }
	   return *((float*)(data+n_word));
    };
    
    double getDFData(int n_word)
    {  
	   union
	   {
		   unsigned short us[4];
		   double dbl;
	   } tmp_union;
		
		
	   if(n_word<0||n_word>=data_size-3)
	   {
	       return -1e32;
	   }
	   tmp_union.us[2]=data[n_word];
	   tmp_union.us[3]=data[n_word+1];
	   tmp_union.us[0]=data[n_word+2];
	   tmp_union.us[1]=data[n_word+3];
	   
	 
	   
	   return tmp_union.dbl;
    };

    uint16_t getBits(	int n_word, 
			int f_bit,
			int n_bits)
    {
	   uint16_t m,i;
	   if(n_word<0||n_word>=data_size||f_bit<0||f_bit>=16||n_bits<=0||n_bits>16-f_bit)
	   {
	       return 0xFFFF;
	   } 
	   if(n_bits==1)return ((data[n_word]>>f_bit)&1);
	   m=1;
	   for(i=1;i<n_bits;i++)
	   {
	       m|=1<<i;
	   }
	   return ((data[n_word]>>f_bit)&1);
    };
};


#endif
