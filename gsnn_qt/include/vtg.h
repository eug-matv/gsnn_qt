﻿ /*—ообщение типа VTG -- наземные курс и скорость*/
 
 #ifndef __VTG_H__
 #define __VTG_H__
 
 #include "time_tools.h"
 
 struct VTG_DATA
 {
      int ns;				//“ип навигационной системы. 0 - GPS, 1 - √ЋќЌј—, 2 GPS+√лонас
      double a_a_T;	//истинный наземный курс в градусах
      double f_f_M;	//магнитный наземный курс в градусах
      double c_c_N;	//наземна¤ скорость в узлах
      double d_d_K;	//наземна¤ скорость в км/ч
      char b;		//режим местоопределени¤: A - автономный, D - дифференциальный, 
			//E - ожидаемый (сопровождение при недостаточном количество спутников),
			//M - ручной ввод, S -  режим иммитации, N - данные не годны
 };
 
#ifdef __cplusplus
extern "C"
{
#endif	 
 int vtgRazborVTG( const char *str_data,
		    struct VTG_DATA *vtg_data);
 
#ifdef __cplusplus
}
#endif
 
 
#endif
